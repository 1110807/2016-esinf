package java_tutorial_7;

import java_tutorial_7.Java_Tutorial_7.Monster;

public class main {
    
    public static void main(String[] args)
    {
        Monster Frank = new Monster();
        
        Frank.name = "Frank";
        
        System.out.println(Frank.name + " has an attack of " + Frank.getAttack());
    }
}
