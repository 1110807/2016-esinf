package java_tutorial_7;

public class Java_Tutorial_7 {

    public static class Monster {
        
        public final String  TOMBSTONE = "Here Lies a Dead monster";
        
        private int health = 500;
        private int attack = 20;
        private int movement = 2;
        private final int xPosition = 0;
        private final int yPosition = 0;
        private boolean alive = true;
        
        public String name = "Big Monster";
        
        public int getAttack()
        {
            return attack;
        }
        
        public int getMovement()
        {
            return movement;
        }
        
        public int getHealth()
        {
            return health;
        }
        
        public void setHealth( int decreaseHealth)
        {
            health = health - decreaseHealth;
            if (health < 0)
            {
                alive = false;
            }
        }
        
        public void setHealth(double decrease)
        {
            int intDecreaseHealth = (int)decreaseHealth;
            health = health - decreaseHealth;
            if (health < 0)
            {
                alive = false;
            }
        }
        
        public Monster(int health, int attack, int movement)
        {
            this.health = health;
            this.attack = attack;
            this.movement = movement;
        }
        
        // Default Constructor
        public Monster()
        {
            
        }
        
    }
    
    public static void main(String[] args) {
        
        Monster Frank = new Monster();
        
        System.out.println(Frank.attack);
        
    }
    
}
