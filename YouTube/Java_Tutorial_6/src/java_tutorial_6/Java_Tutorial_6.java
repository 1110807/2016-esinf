package java_tutorial_6;

import java.util.*;
import java.io.FileNotFoundException;
import java.io.*;

public class Java_Tutorial_6 {
    
    static Scanner userInput = new Scanner(System.in);

    public static void main(String[] args)
    {
        try
        {
        getAFile("./somestuff.txt");
        }
        catch (IOException e)
        {
            System.out.println("An IO Error Occured");
        }
        /*
        System.out.println("How old are you?");
        int age = checkValidAge();
        
        if (age != 0)
        {
            System.out.println("You are " + age + " years old!");
        }
        divideByZero(2);
        */
    }
    
    public static void getAFile(String filename) trows IOException, FileNotFoundException {
        
        FileInputStream file = new FileInputStream(fileName);
        
        /*
        try
        {
            FileInputStream file = new FileInputStream(fileName);
        }
        
        catch (FileNotFoundException e)
        {
            System.out.println("Sorry I couldn't find that file");
        }
        catch (IOException)
        {
            System.out.println("Unknown IO Error");
        }
        catch (Exception e)
        {
            System.out.println("Some error occurred");
        }
        */

        // finnaly é sempre chamado seja apanhado algum erro ou não
        
        finally {
            System.out.println("");
        }
    }
    
    public static int checkValidAge() {
        try
        {
            return userInput.nextInt();
        }
        catch (InputMismatchException e)
        {
            userInput.next();
            System.out.println("That isn't a whole number");
            return 0;
        }
    }
    
    public static void divideByZero(int a) {
        try
        {
            System.out.println(a/0);
        }
        
        catch(ArithmeticException e)
        {
            //Imprimir erros de excepção
            System.out.println("You can't do that");
            
            System.out.println(e.getMessage());
            
            System.out.println(e.toString());
            
            e.printStackTrace();
        }
    }
}
