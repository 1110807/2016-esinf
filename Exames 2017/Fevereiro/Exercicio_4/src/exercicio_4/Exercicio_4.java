package exercicio_4;


import static java.lang.Integer.MAX_VALUE;
import java.util.*;

public class Exercicio_4 {

    
    /*
    public static double smaller_connection(No no1, No no2, LinkedList<String> lista) {
        return -1;
    }
    */
    public static void main(String[] args) {
        
        Graph<String,Integer> grafico = new Graph(false);
        LinkedList <String> resultado = new LinkedList<>();
        ArrayList <LinkedList<String>> listagem = new ArrayList<>();
        LinkedList <String> lista = new LinkedList<>();
      
            
        
        String a = "A";
        String b = "B";
        String c = "C";
        String d = "D";
        String e = "E";
        String f = "F";
        String g = "G";
        String h = "H";
        
        grafico.insertVertex(a);
        grafico.insertVertex(b);
        grafico.insertVertex(c);
        grafico.insertVertex(d);
        grafico.insertVertex(e);
        grafico.insertVertex(f);
        grafico.insertVertex(g);
        grafico.insertVertex(h);
        
        grafico.insertEdge(a, b, 4, 4);
        grafico.insertEdge(a, c, 4, 4);
        grafico.insertEdge(c, f, 3, 3);
        grafico.insertEdge(b, d, 6, 6);
        grafico.insertEdge(d, e, 7, 7);
        grafico.insertEdge(e, f, 5, 5);
        grafico.insertEdge(d, g, 2, 2);
        grafico.insertEdge(f, h, 5, 5);
        grafico.insertEdge(h, g, 6, 6);
        
        
        listagem = GraphAlgorithms.allPaths(grafico, a, d);
        
        int menor = Integer.MAX_VALUE;
        int indice;
        
        for ( LinkedList<String> z : listagem) {
            int tamanho = z.size();
            if (tamanho < menor) {
                resultado = z;
            }
            System.out.println(z);
        }
        resultado.forEach(s-> System.out.println(s));
        /*
        double soma = GraphAlgorithms.shortestPath(grafico, a, e, lista);
        
        System.out.println("Media = " + soma/resultado.size());
        */
        Iterable <String> Vertices = grafico.vertices();
        int soma = 0;
        for (int i=0; i<resultado.size() - 1;i++) {
            soma += grafico.getEdge(resultado.get(i), resultado.get(i+1)).getElement();
        }
        System.out.println(soma/(resultado.size()-1));
        
    }
    
}
