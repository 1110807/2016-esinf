package exercicio_1;

import java.util.*;

public class Exercicio_1 {

    public static Map <Integer, LinkedList <Integer>> roletaRussa(LinkedList <Integer> list) {
        
        Map <Integer, LinkedList <Integer>> resultado = new HashMap<>();
        
        ListIterator <Integer> iterador = list.listIterator();
        
        for (int i = 0; i < list.size(); i++) {
            while (list.size() > 1) {
                Random generator = new Random();
                int ind = generator.nextInt(list.size());
            
                System.out.println(ind);
            
                list.remove(ind);
            
                LinkedList <Integer> subLista = new LinkedList<>();
                subLista = (LinkedList) list.clone();
                
                //resultado.put(ind);
                resultado.put(ind, subLista);
            
                System.out.println(list);
                //System.out.println(resultado);
            
            }
        }
        
        return resultado;
    }
    
    
    public static void main(String[] args) {
        
        LinkedList <Integer> list = new LinkedList<>();
        Map <Integer, LinkedList <Integer>> resultadoFinal = new HashMap<>();
        
        list.add(2);
        list.add(9);
        list.add(7);
        list.add(10);
        list.add(15);
        list.add(6);
        list.add(12);
        list.add(3);
        
        resultadoFinal = roletaRussa (list);
        
        System.out.println(resultadoFinal);
        
    }
    
}
