package exercicio1;

import static java.lang.Integer.MAX_VALUE;
import java.util.*;

public class Exercicio1 {

    
    public static Map<Integer, LinkedList<Integer>> ksubLists(LinkedList<Integer> list, ArrayList<Integer> centers) {
        
        Map <Integer, LinkedList <Integer>> resultado = new HashMap<>();
        
        for (Integer center : centers) {
            
            LinkedList <Integer> subList = new LinkedList<>();
            subList.add(center);
            resultado.put(center, subList);
            
        }
        
        for(Integer l : list) {
            if (!centers.contains(l)) {
                Integer posicao = 0;
                Integer menor = MAX_VALUE;
            
                for (Integer center : centers) {
                
                    int calc = Math.abs(center- l);
                
                    if (calc < menor) {
                        menor = calc;
                        posicao = center;
                        System.out.println("posicao " + posicao);
                    }
                }
                resultado.get(posicao).add(l);
            }
        }
        
        return resultado;
    }
    
    
    public static void main(String[] args) {
        
        LinkedList <Integer> list = new LinkedList <>();
        ArrayList <Integer> centers = new ArrayList<>();
        Map <Integer, LinkedList <Integer>> resultadofinal = new HashMap<>();
        
        list.add(2);
        list.add(9);
        list.add(7);
        list.add(5);
        list.add(10);
        list.add(15);
        list.add(6);
        list.add(12);
        list.add(3);
        
        centers.add(3);
        centers.add(6);
        centers.add(10);
        
        resultadofinal = ksubLists(list, centers);
        
            
        System.out.println(resultadofinal);
        
    }
    
}
