/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl3_complexity;

import java.util.Arrays;
import java.util.Random;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author brunosantos
 */
public class PL3_ComplexityTest {

    Comparable[] dez = {1, 3, 2, 4, 7, 6, 9, 8, 1, 6};
    Comparable[] cem = new Comparable[100];
    Comparable[] mil = new Comparable[1000];
    Comparable[] dezMil = new Comparable[10000];
    Comparable[] cemMil = new Comparable[100000];
    Comparable[] milhao = new Comparable[1000000];

    public PL3_ComplexityTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws Exception {
        for (int i = 0; i < 10; i++) {
            dez[i] = (int) (Math.random() * 10);
        }
        for (int i = 0; i < 1000000; i++) {
            milhao[i] = (int) (Math.random() * 1000000);
        }

    }

    /**
     * Test of insertionSort method, of class PL3_Complexity.
     */
    @Test
    public void testInsertionSort() {
        System.out.print("insertionSort_10, ");

        int n = 10;
        long startTime = System.currentTimeMillis();

        PL3_Complexity.insertionSort(milhao, n);

        long endTime = System.currentTimeMillis();

        System.out.println("for an array of  " + n + " elements, takes " + (endTime - startTime) + " milliseconds");

    }

    /**
     * Test of insertionSort method, of class PL3_Complexity.
     */
    @Test
    public void testInsertionSort2() {
        System.out.print("insertionSort_100, ");

        int n = 100;

        long startTime = System.currentTimeMillis();

        PL3_Complexity.insertionSort(milhao, n);

        long endTime = System.currentTimeMillis();

        System.out.println("for an array of  " + n + " elements takes " + (endTime - startTime) + " milliseconds");

    }

    /**
     * Test of insertionSort method, of class PL3_Complexity.
     */
    @Test
    public void testInsertionSort3() {
        System.out.print("insertionSort_1000, ");

        int n = 1000;

        long startTime = System.currentTimeMillis();

        PL3_Complexity.insertionSort(milhao, n);

        long endTime = System.currentTimeMillis();

        System.out.println("for an array of  " + n + " elements takes " + (endTime - startTime) + " milliseconds");

    }

    /**
     * Test of insertionSort method, of class PL3_Complexity.
     */
    @Test
    public void testInsertionSort4() {
        System.out.print("insertionSort_10000, ");

        int n = 10000;

        long startTime = System.currentTimeMillis();

        PL3_Complexity.insertionSort(milhao, n);

        long endTime = System.currentTimeMillis();

        System.out.println("for an array of  " + n + " elements takes " + (endTime - startTime) + " milliseconds");

    }

    /**
     * Test of insertionSort method, of class PL3_Complexity.
     */
    @Test
    public void testInsertionSort5() {
        System.out.print("insertionSort_100000, ");

        int n = 100000;

        long startTime = System.currentTimeMillis();

        PL3_Complexity.insertionSort(milhao, n);

        long endTime = System.currentTimeMillis();

        System.out.println("for an array of  " + n + " elements takes " + (endTime - startTime) + " milliseconds");

    }

//    /**
//     * Test of insertionSort method, of class PL3_Complexity.
//     */
//    @Test
//    public void testInsertionSort6() {
//        System.out.print("insertionSort_1000000, ");
//
//        int n = 1000000;
//
//        long startTime = System.currentTimeMillis();
//
//        PL3_Complexity.insertionSort(milhao, n);
//
//        long endTime = System.currentTimeMillis();
//
//        System.out.println("for an array of  " + n + " elements takes " + (endTime - startTime) + " milliseconds");
//
//    }
    /**
     * Test of mergeSort method, of class PL3_Complexity.
     */
    @Test
    public void testMergeSort1() {
        System.out.print("mergeSort_10, ");

        int n = 10;
        long startTime = System.currentTimeMillis();

        PL3_Complexity.mergeSort(dez, n);

        long endTime = System.currentTimeMillis();

        System.out.println("for an array of  " + n + " elements takes " + (endTime - startTime) + " milliseconds");

    }

    /**
     * Test of mergeSort method, of class PL3_Complexity.
     */
    @Test
    public void testMergeSort2() {
        System.out.print("mergeSort_100, ");

        int n = 100;
        long startTime = System.currentTimeMillis();

        PL3_Complexity.mergeSort(milhao, n);

        long endTime = System.currentTimeMillis();

        System.out.println("for an array of  " + n + " elements takes " + (endTime - startTime) + " milliseconds");

    }

    /**
     * Test of mergeSort method, of class PL3_Complexity.
     */
    @Test
    public void testMergeSort3() {
        System.out.print("mergeSort_1000, ");

        int n = 1000;
        long startTime = System.currentTimeMillis();

        PL3_Complexity.mergeSort(milhao, n);

        long endTime = System.currentTimeMillis();

        System.out.println("for an array of  " + n + " elements takes " + (endTime - startTime) + " milliseconds");

    }

    /**
     * Test of mergeSort method, of class PL3_Complexity.
     */
    @Test
    public void testMergeSort4() {
        System.out.print("mergeSort_10000, ");

        int n = 10000;
        long startTime = System.currentTimeMillis();

        PL3_Complexity.mergeSort(milhao, n);

        long endTime = System.currentTimeMillis();

        System.out.println("for an array of  " + n + " elements takes " + (endTime - startTime) + " milliseconds");

    }

    /**
     * Test of mergeSort method, of class PL3_Complexity.
     */
    @Test
    public void testMergeSort5() {
        System.out.print("mergeSort_1000000, ");

        int n = 1000000;
        long startTime = System.currentTimeMillis();

        PL3_Complexity.mergeSort(milhao, n);

        long endTime = System.currentTimeMillis();

        System.out.println("for an array of  " + n + " elements takes " + (endTime - startTime) + " milliseconds");

    }

    /**
     * Test of quickSort method, of class PL3_Complexity.
     */
    @Test
    public void testQuickSort1() {
        System.out.print("quickSort_10 ");
        int left = 0;
        int n = dez.length-1;
        System.out.println(Arrays.toString(dez));
        long startTime = System.currentTimeMillis();
        PL3_Complexity.quickSort(dez, left, n);
        long endTime = System.currentTimeMillis();

        System.out.println("for an array of  " + (n+1) + " elements takes " + (endTime - startTime) + " milliseconds");
        System.out.println(Arrays.toString(dez));
    }
    
    /**
     * Test of quickSort method, of class PL3_Complexity.
     */
    @Test
    public void testQuickSort2() {
        System.out.print("quickSort_100 ");
        int left = 0;
        int n = 99;
        long startTime = System.currentTimeMillis();
        PL3_Complexity.quickSort(milhao, left, n);
        long endTime = System.currentTimeMillis();

        System.out.println("for an array of  " + (n+1) + " elements takes " + (endTime - startTime) + " milliseconds");
         System.out.println(Arrays.toString(milhao));
    }
    
    /**
     * Test of quickSort method, of class PL3_Complexity.
     */
    @Test
    public void testQuickSort3() {
        System.out.print("quickSort_1000 ");
        int left = 0;
        int n = 999;
        long startTime = System.currentTimeMillis();
        PL3_Complexity.quickSort(milhao, left, n);
        long endTime = System.currentTimeMillis();

        System.out.println("for an array of  " + (n+1) + " elements takes " + (endTime - startTime) + " milliseconds");
    }
    
    /**
     * Test of quickSort method, of class PL3_Complexity.
     */
    @Test
    public void testQuickSort4() {
        System.out.print("quickSort_100000 ");
        int left = 0;
        int n = 99999;
        long startTime = System.currentTimeMillis();
        PL3_Complexity.quickSort(milhao, left, n);
        long endTime = System.currentTimeMillis();

        System.out.println("for an array of  " + (n+1) + " elements takes " + (endTime - startTime) + " milliseconds");
    }
    
}
