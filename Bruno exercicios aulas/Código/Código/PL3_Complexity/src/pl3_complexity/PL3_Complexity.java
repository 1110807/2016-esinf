/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl3_complexity;

import java.util.Arrays;

/**
 *
 * @author brunosantos
 */
public class PL3_Complexity {

    public static <T extends Comparable<T>> void insertionSort(T v[], int n) {
        int j;
        T x;
        for (int i = 1; i < n; i++) {
            j = i;
            x = v[i];

            while (j > 0 && x.compareTo(v[j - 1]) < 0) {

                v[j] = v[j - 1];
                j = j - 1;
            }
            v[j] = x;
        }
    }

    public static <T extends Comparable<T>> void mergeSort(T S[], int n) {
        if (n >= 2) {
            int mid = n / 2; // index of midpoint
            T S1[] = Arrays.copyOfRange(S, 0, mid);
            T S2[] = Arrays.copyOfRange(S, mid, S.length);
            mergeSort(S1, S1.length);
            mergeSort(S2, S2.length);

            merge(S1, S2, S);
        }
    }

    public static <T extends Comparable<T>> void merge(T[] S1, T[] S2, T[] S) {

        int i = 0, j = 0, k = 0;

        while (i < S1.length && j < S2.length) {
            if (S1[i].compareTo(S2[j]) < 0) {
                S[k++] = S1[i++];
            } else {
                S[k++] = S2[j++];
            }
        }

        while (i < S1.length) {
            S[k++] = S1[i++];
        }

        while (j < S2.length) {
            S[k++] = S2[j++];
        }

    }

    public static <T extends Comparable<T>> void quickSort(T S[], int left, int right) {
        T pivot = S[(left + right) / 2], temp;
        int i = left;
        int j = right;
        while (i <= j) {
            while ( S[i].compareTo(pivot) < 0) {
                i++;
            }
            while ( S[j].compareTo(pivot) > 0) {
                j--;
            }
            if (i <= j) {
                temp = S[j];
                S[j] = S[i];
                S[i] = temp;
                i++;
                j--;
            }

        }
        if (left < j) {
            quickSort(S, left, j);
        }
        if (right > i) {
            quickSort(S, i, right);
        }
    }
}
