/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.time.LocalDate;
import java.util.*;

/**
 *
 * @author nunotmalheiro
 */
public class Supermarket {

    Map<Invoice, Set<Product>> m;

    Supermarket() {
        m = new HashMap<>();
    }

    // Reads invoices from a list of String
    void getInvoices(List<String> l) throws Exception {
        Set<Product> sp = null ;
        Invoice i = null;
        for (String line : l) {
            String[] x = line.split(",");

            switch (x[0]) {
                case "I":
                    i = new Invoice(x[1], x[2]);
                    sp = new TreeSet<>();
                    m.put(i, sp);
                    break;
                case "P":
                    sp.add(new Product(x[1], Integer.parseInt(x[2]), Long.parseLong(x[3])));
                    break;
                    default: throw new Exception("erro");
            }

        }
    }

    // returns a set in which each number is the number of products in the r
    // invoice 
    Map<Invoice, Integer> numberOfProductsPerInvoice() {
        Map<Invoice, Integer> res = new HashMap<>();
        for (Invoice inv : m.keySet()) {
            res.put(inv, m.get(inv).size());
        }

        return res;
    }

    // returns a Set of invoices in which each date is >d1 and <d2
    Set<Invoice> betweenDates(LocalDate d1, LocalDate d2
    ) {
        Set<Invoice> betweenDates = new HashSet<>();
        for(Invoice inv : m.keySet()){
            if( inv.getDate().isAfter(d1) && inv.getDate().isBefore(d2)){
                betweenDates.add(inv);
            }
        }
        return betweenDates;
    }

    // returns the sum of the price of the product in all the invoices
    long totalOfProduct(String productId
    ) {
        long total = 0;
        for(Invoice inv : m.keySet()){
            for(Product p : m.get(inv)){
                if(p.getIdentification().equalsIgnoreCase(productId))
                    total = total + p.getPrice()*p.getQuantity();
            }
        }
        return total;
    }

    // converts a map of invoices and troducts to a map which key is a product 
    // identification and the values are a set of the invoice references 
    // in which it appearss
    Map<String, Set<Invoice>> convertInvoices() {
        
        Map<String, Set<Invoice>> convertido = new HashMap<>();
        Set<Invoice> invconv = new TreeSet<>();
        
        
        for(Invoice inv : m.keySet()){
            for( Product p : m.get(inv)){
                
                    convertido.put(p.getIdentification(), invconv);
                    invconv.add(inv);
            }
        }
        
        return convertido;
    }
}
