/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author brunosantos
 */
public class PL1_1StringTest {
    
    public PL1_1StringTest() {
    }

    
    /**
     * Test of listAscending method, of class PL1_1String.
     */
    @Test
    public void testListAscending() {
        System.out.println("listAscending");
        String[] v = {"1","2","3"};
        int i = 0;
        String[] expResult = {"1","2","3"};
        String[] result = PL1_1String.listAscending(v, i);
        assertArrayEquals(expResult, result);
    }

  
    /**
     * Test of listDescending method, of class PL1_1String.
     */
    @Test
    public void testListDescending() {
        System.out.println("listDescending");
        String[] v = {"1","2","3"}; ;
        int i = 0;
        String[] expResult = {"3","2","1"};;
        String[] result = PL1_1String.listDescending(v, i);
        assertArrayEquals(expResult, result);
    }

  /**
     * Test of listaAscendente method, of class PL1_1String.
     */
    @Test
    public void testListaAscendente() {
        System.out.println("listaAscendente");
        String s = "123";
        String ascendente = "";
        String expResult = "123";
        String result = PL1_1String.listaAscendente(s, ascendente);
        assertEquals(expResult, result);

    }

    /**
     * Test of listaAscendente method, of class PL1_1String.
     */
    @Test
    public void testListaAscendente2() {
        System.out.println("listaAscendente2");
        String s = "asdeqwer";
        String ascendente = "";
        String expResult = "asdeqwer";
        String result = PL1_1String.listaAscendente(s, ascendente);
        assertEquals(expResult, result);

    }

   
    
    /**
     * Test of listadescendente method, of class PL1_1String.
     */
    @Test
    public void testListadescendente() {
        System.out.println("listadescendente");
        String s = "12345";
        PL1_1String instance = new PL1_1String();
        String expResult = "54321";
        String result = instance.listadescendente(s);
        assertEquals(expResult, result);
       
    }

   
    /**
     * Test of listaAscendente11 method, of class PL1_1String.
     */
    @Test
    public void testListaAscendente11() {
        System.out.println("listaAscendente11");
        String s = "123";
        String expResult = "123";
        String result = PL1_1String.listaAscendente11(s);
        assertEquals(expResult, result);

    }

   

  
   
  
   
}
