/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author brunosantos
 */
public class PL1_2Test {
    
    public PL1_2Test() {
    }

    /**
     * Test of palindromeTester method, of class PL1_2.
     */
    @Test
    public void testPalindromeTester() {
        System.out.println("palindromeTester1");
        int original = 23332;
        boolean expResult = true;
        boolean result = PL1_2.palindromeTester(original);
        assertEquals(expResult, result);
    }

     /**
     * Test of palindromeTester method, of class PL1_2.
     */
    @Test
    public void testPalindromeTester2() {
        System.out.println("palindromeTester2");
        int original = 233;
        boolean expResult = false;
        boolean result = PL1_2.palindromeTester(original);
        assertEquals(expResult, result);
    }

     /**
     * Test of palindromeTester method, of class PL1_2.
     */
    @Test
    public void testPalindromeTester3() {
        System.out.println("palindromeTester3");
        int original = 2;
        boolean expResult = true;
        boolean result = PL1_2.palindromeTester(original);
        assertEquals(expResult, result);
    }

    /**
     * Test of palindromeTester method, of class PL1_2.
     */
    @Test
    public void testPalindromeTester4() {
        System.out.println("palindromeTester4");
        int original=0;
        boolean expResult = true;
        boolean result = PL1_2.palindromeTester(original);
        assertEquals(expResult, result);
    }

    /**
     * Test of invertNumber method, of class PL1_2.
     */
    @Test
    public void testInvertNumber() {
        System.out.println("invertNumber");
        int original = 123;
        int reverse = 0;
        int expResult = 321;
        int result = PL1_2.invertNumber(original, reverse);
        assertEquals(expResult, result);
    }
    
}
