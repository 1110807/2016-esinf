/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author brunosantos
 */
public class PL1_3Test {
    
    public PL1_3Test() {
    }

    /**
     * Test of mainOfMDC method, of class PL1_3.
     */
    @Test
    public void testMainOfMDC() {
        System.out.println("mainOfMDC1");
        int m = 30;
        int n = 48;
        int expResult = 6;
        int result = PL1_3.mainOfMDC(m, n);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of mainOfMDC method, of class PL1_3.
     */
    @Test
    public void testMainOfMDC2() {
        System.out.println("mainOfMDC2");
        int m = 12312;
        int n = 345;
        int expResult = 3;
        int result = PL1_3.mainOfMDC(m, n);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of mainOfMDC method, of class PL1_3.
     */
    @Test
    public void testMainOfMDC3() {
        System.out.println("mainOfMDC3");
        int m = 1212;
        int n = 3425;
        int expResult = 1;
        int result = PL1_3.mainOfMDC(m, n);
        assertEquals(expResult, result);
       
    }
}
