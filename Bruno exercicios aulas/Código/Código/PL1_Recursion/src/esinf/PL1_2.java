/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

/**
 *
 * @author brunosantos
 */
public class PL1_2 {

    public static boolean palindromeTester(int original) {
        int reverse = 0;
        if (original == 0 || original/10 == 0) {
            return true;
        }

        reverse = invertNumber(original, reverse);
        
        return reverse == original;
    }

    public static int invertNumber(int original, int reverse) {
        if (original < 10) {
            
            return reverse*10+original;
        }
        reverse = original%10 + reverse*10;
        
        return invertNumber(original/10,reverse);
    }

}
