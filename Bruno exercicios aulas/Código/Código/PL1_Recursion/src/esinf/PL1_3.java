/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

/**
 *
 * @author brunosantos
 */
public class PL1_3 {

    public static int mainOfMDC(int m, int n) {
        int mdc = 1, temp;
        if (m == n) {
            mdc = m;
        } else {
            if (m < n) {
                temp = m;
                m = n;
                n = temp;
            }
            temp = 1;
            mdc = greatestCommonDivisor(n, m, mdc, temp);
        }
        return mdc;
    }

    public static int greatestCommonDivisor(int m, int n, int mdc, int temp) {
        if (temp >= m/2 ) {
            return mdc;
        }
        if (m % temp == 0 && n % temp == 0) {
            mdc = temp;
        }
        temp++;
        return greatestCommonDivisor(m, n, mdc, temp);
    }

}
