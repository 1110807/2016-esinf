package esinf;

/**
 *
 * @author DEI-ESINF
 */
public class Labirinth {

    /**
     *
     * @param actual the labirinth in its actual (marked) form
     * @param y coordinate y in the labirinth
     * @param x coordinate x in the labirinth
     * @return the marked labirinth or null if there is no way
     */
    public static int[][] check(int[][] actual, int y, int x) {
        
        /**
        * verificação de que não sai para fora do labirinto
        */
        if (y < 0 || y >= actual.length) {
            System.out.println("saida do labirinto");
            return null;
        }

        /**
        * verificação de que não sai do labirinto
        */
        if (x < 0 || x >= actual[y].length) {
            System.out.println("saida do labirinto");
            return null;
        }

        /**
         * verificação de posição incorrecta
         */
        if (actual[y][x] != 1) {
            System.out.println("posição incorrecta");
            return null;
        }

        /**
         * marcação do caminho
         */
        System.out.println("marcação com 9");
        actual[y][x] = 9;
       
        /*
        neste puzzle a saida é na posição Y=6 e X = 12
        */
        if (y == 6 && x == 12) {
            System.out.println("\n\n#####fim#####\n\n");
            return actual;
        }


        
        if(check(actual, y - 1, x) !=null)
            return actual;

        if(check(actual, y, x + 1) != null){
            return actual;
        }

        if(check(actual, y + 1, x) != null){
            return actual;
        }

        if(check(actual, y, x - 1) != null){
            return actual;
        }

        /**
         * marcação da alteração de passagem por caminhos já passados
         */
        System.out.println("marcação 2");
        actual[y][x] = 2;

        return null;
    }

}
