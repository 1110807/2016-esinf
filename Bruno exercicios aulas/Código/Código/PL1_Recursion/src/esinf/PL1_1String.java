/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.util.Arrays;

/**
 *
 * @author brunosantos
 */
public class PL1_1String {

//  
    public static String[] listAscending(String v[], int i) {
        String[] r = new String[v.length];
        RListAscending(v, 0, r);
        return r;
    }

    public static void RListAscending(String v[], int i, String[] res) {
        if (i < v.length) {
            res[i] = v[i];
            RListAscending(v, i + 1, res);

//            Se for para o inverso passar, para aqui o res[i] = v[i];
        }
    }

    public static String[] listDescending(String v[], int i) {
        String[] r = new String[v.length];
        RListDescending(v, 0, r);
        return r;
    }

    public static void RListDescending(String v[], int i, String[] res) {
        if (i < v.length) {
            res[v.length - i -1] = v[i];
            RListDescending(v, i + 1, res);

        }

    }
    
    public static String listaAscendente(String s, String ascendente){
        if((s == null || (s.length() <= 0))){
            return ascendente;
        }
        ascendente = ascendente + s.charAt(0);
        return listaAscendente(s.substring(1), ascendente);
    }
    
     public static String listaAscendente11(String s){
        if((s == null || (s.length() <= 1))){
            return s;
        }

        return listaAscendente11(s.substring(0, s.length()-1)) + s.charAt(s.length()-1);
    }
    
    public static String listadescendente(String s){
        if((s == null || (s.length()<=1))){
            return s;
        }
        return listadescendente(s.substring(1)) + s.charAt(0);
    }

}
