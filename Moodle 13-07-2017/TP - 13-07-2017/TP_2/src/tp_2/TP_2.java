package tp_2;

public class TP_2 {

    public static int somadois(int num1, int num2) {
                
        if (num2 > 0) {
            
            num1 = num1 + 1;
            //num2 = num2 - 1;
            
            //System.out.println("A soma é " + num1);
                        
            return somadois(num1, num2-1);
            
        }
        return num1;
        
    }
    
    public static void main(String[] args) {
        
        int num1 = 10;
        int num2 = 5;
        int soma;
        
        System.out.println("Os valores das duas variaveis sao " + num1 + " e " +num2);
        
        soma = somadois(num1, num2);
        
        System.out.println("A soma é " + soma);
        
    }
    
}
