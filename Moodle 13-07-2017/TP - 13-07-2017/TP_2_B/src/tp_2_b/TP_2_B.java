package tp_2_b;

public class TP_2_B {

    public static void converteBinario(int num) {
        
        int resultado;
        
        if (num != 0) {
            
            resultado = (num % 2);
                        
            converteBinario(num/2);
            
            System.out.print(resultado);
            
        }
    }
    
    
    public static void main(String[] args) {
        
        int num = 10;
        
        System.out.println("O numero em decimal é " + num);
        
        System.out.print("O numero em binario é ");
        
        converteBinario(num);
        
        System.out.println("\n");
    }
    
}
