package tp_1;

import java.util.*;

public class TP_1 {

    public static void process (int a[], int liminf, int limsup) {
        int i=liminf;
        int j=limsup-1 ;
        
        while (i<j) {
            int temp=a[i];
            a[i]=a[j];
            a[j]=temp;
            i++;
            j--;
        }
    }
    public static void example (int[] a, int li, int ls) {
    
        if (li < ls) {
        process (a,li,ls);
        ls=ls/2;
        example (a,li,ls);
        }
    }
    
    public static void main(String[] args) {
        
        int vector [] = {6,1,4,2,7,3,1,5};
        int li = 0;
        int lf = 8;
        
        System.out.print("O vetor inicial é " + Arrays.toString(vector));
        
        example (vector, li, lf);
        
        System.out.print("O vetor inicial é " + Arrays.toString(vector));
        
    }
    
}
