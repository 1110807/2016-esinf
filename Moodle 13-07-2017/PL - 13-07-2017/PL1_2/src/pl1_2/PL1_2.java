package pl1_2;

public class PL1_2 {

    public static int verificaPalindrome (int num, int reverso){
    
        int resto = 0;
        
        while (num != 0) {
            resto = num % 10;
            reverso = reverso * 10 + resto;
            num = num / 10;
            
            return verificaPalindrome(num, reverso);
        }
        
        return reverso;
        
    }
    
    public static void main(String[] args) {
        
        int num = 101;
        int reverso = 0;
        
        reverso = verificaPalindrome (num, reverso);
        
        System.out.println(reverso);
        
        if (reverso == num) {
            System.out.println("O numero é palindrome!");
        }
        else {
            System.out.println("O numero NÃO é palindrome!");
        }
        
    }
    
}
