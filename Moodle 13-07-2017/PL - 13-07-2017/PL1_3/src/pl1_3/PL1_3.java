package pl1_3;

public class PL1_3 {

    public static int maiorDivisor (int num1, int num2, int divisor, int resultado) {
        
        int menor;
        
        if (num1 < num2) {
            menor = num2;
        }
        else {
            menor = num1;
        }
        
        if (menor >= divisor) {
            
            if (num1 % divisor == 0 && num2 % divisor == 0) {
                resultado = divisor;
                
                System.out.println(resultado);
            }
            
            return maiorDivisor (num1, num2, divisor+1, resultado);
        }
        return resultado;
    }
    
    public static void main(String[] args) {
        int num1 = 48;
        int num2 = 30;
        int divisor = 1;
        int resultado = 0;
        
        resultado = maiorDivisor (num1, num2, divisor, resultado);
        
        System.out.println(resultado);
    }
    
}
