package pl1_1b;

public class PL1_1B {

    public static String palavra (String original, String transformada, int posicao) {
        
        //String transformada;
                
        if (posicao > 0) {
            transformada += original.charAt(posicao-1);
            
            return palavra (original, transformada, posicao-1);
            
        }
        
        return transformada;
    }
    
    public static void main(String[] args) {
        
        String original = "Davide";
        int posicao = original.length();
        String transformada = "";
        
        System.out.println(original);
        
        transformada = palavra (original, transformada, posicao);
        
        System.out.println(transformada);
        
    }
    
}
