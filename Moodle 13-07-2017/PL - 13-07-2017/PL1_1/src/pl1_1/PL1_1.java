package pl1_1;

public class PL1_1 {

    public static String palavra (String original, String transformada, int posicao) {
        
        //String transformada;
        
        if (posicao != original.length()) {
            transformada += original.charAt(posicao);
            return palavra (original, transformada, posicao+1);
            
        }
        
        return transformada;
    }
    
    public static void main(String[] args) {
        
        String abc = new String("abcde");
        String abc1 = new String("abcde");
        int x = abc.compareTo(abc1);
        System.out.println("O valor da string 1 é mais pequeno que o valor da string 2");
        System.out.println("O valor é: " + x);
        
        String original = "Davide";
        int posicao = 0;
        String transformada = "";
        
        System.out.println(original);
        
        transformada = palavra (original, transformada, posicao);
        
        System.out.println(transformada);
        
    }
    
}
