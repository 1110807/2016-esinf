package maps;

import java.util.*;

public class Maps {

    public static void main(String[] args) {
        
        Map<Integer, String> mapHTTPErrors = new HashMap<>();
        
        mapHTTPErrors.put(200, "Ok");
        mapHTTPErrors.put(303, "See Other");
        mapHTTPErrors.put(404, "Not Found");
        mapHTTPErrors.put(500, "Internal Server Error");
        
        //System.out.println(mapHTTPErrors);
        
        Map<Integer, String> mapErrors = new HashMap<>(mapHTTPErrors);
        
        //System.out.println(mapErrors);
        
        Map<String, String> mapContacts = new LinkedHashMap<>();
        
        mapContacts.put("0169238175", "Tom");
        mapContacts.put("0904891321", "Peter");
        mapContacts.put("0945678912", "Mary");
        mapContacts.put("0981127421", "John");
        
        //System.out.println(mapContacts);
        
        Map<String, String> mapLang = new TreeMap<>();
        
        mapLang.put(".c","C");
        mapLang.put(".java","Java");
        mapLang.put(".pl","Perl");
        mapLang.put(".cs","C#");
        mapLang.put(".php","PHP");
        mapLang.put(".cpp","C++");
        mapLang.put(".xml","XML");
        
        System.out.println(mapLang);
        
        mapLang.remove(".c");
        
        mapHTTPErrors.put(301, "Moved Permanently");
        
        System.out.println(mapHTTPErrors);
        
        String status301 = mapHTTPErrors.get(301);
        
        System.out.println(status301);
        
        if (mapHTTPErrors.containsKey(200)) {
            //System.out.println("Http status 200");
        }
        
        if (mapHTTPErrors.containsValue("Ok")) {
            //System.out.println("Found status OK");
        }
        
        String removedValue = mapHTTPErrors.remove(500);
        
        if (removedValue != null) {
            System.out.println("Removed value: " + removedValue);
        }
        
        mapHTTPErrors.remove(301);
        
        //System.out.println("Map após remoção: " + mapHTTPErrors);
        
        //System.out.println("Map before: " + mapHTTPErrors);
        
        mapHTTPErrors.replace(303, "No changes");
        
        //System.out.println("Map after: " +mapHTTPErrors);
        
        int size = mapHTTPErrors.size();
        
        //System.out.println(size);
        
        Map<String, String> mapCountryCodes = new TreeMap<>();
        
        mapCountryCodes.put("1", "USA");
        mapCountryCodes.put("44", "United Kingdom");
        mapCountryCodes.put("33", "France");
        mapCountryCodes.put("81", "Japan");
        
        Set<String> setCodes = mapCountryCodes.keySet();
        Iterator<String> iterator = setCodes.iterator();
        
        while(iterator.hasNext()) {
            String code = iterator.next();
            String country = mapCountryCodes.get(code);
            
            //System.out.println(code + " => " + country);
        }
        
        Collection<String> countries = mapCountryCodes.values();
        
        for (String country : countries) {
            //System.out.println(country);
        }
        
        mapCountryCodes.forEach((code, country) -> System.out.println(code + " => " + country));
        
        mapCountryCodes.clear();
        
        System.out.println("Is map empty " + mapCountryCodes.isEmpty());
        
        Map<Integer, String> countryCodesEU = new HashMap<>();
        
        countryCodesEU.put(44, "United Kingdom");
        countryCodesEU.put(33, "France");
        countryCodesEU.put(49, "Germany");
        
        Map<Integer, String> countryCodesWorld = new LinkedHashMap<>();
        
        countryCodesWorld.put(1, "United States");
        countryCodesWorld.put(86, "China");
        countryCodesWorld.put(82, "South Korea");
        
        System.out.println("Before: " + countryCodesWorld);
        
        countryCodesWorld.putAll(countryCodesEU);
        
        System.out.println("After: " + countryCodesWorld);

    }
    
}
