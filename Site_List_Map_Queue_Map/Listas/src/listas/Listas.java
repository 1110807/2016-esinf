package listas;

import java.util.*;

public class Listas {

    public static void main(String[] args) {
        
        List<String> listStrings = new ArrayList<>();
        
        listStrings.add("One");
        listStrings.add("Two");
        listStrings.add("Three");
        listStrings.add("Four");
        
        //System.out.println("1 - Array list " + listStrings);
        
        List<String> linkedList = new LinkedList<>();
        
        linkedList.add("Five");
        linkedList.add("Six");
        linkedList.add("Seven");
        linkedList.add("Eight");
        
        //System.out.println("2 - Linked list " + linkedList);
        
        List<Number> variosTipos = new LinkedList<>();
        List<Number> variosTipos2 = new LinkedList<>();
        
        variosTipos.add(new Integer(123));
        variosTipos.add(new Float(3.23));
        variosTipos.add(new Double(299.988));
        
        variosTipos2.add(333);
        variosTipos2.add(444);
        
        variosTipos.add(2, 222);
        
        variosTipos.addAll(2, variosTipos2);
        
        variosTipos.set(1, 4.4);
        
        //System.out.println("2 - Linked list Varios tipos" + variosTipos);
        
        List<Integer> linkedListInteger = new LinkedList<>();
        
        linkedListInteger.add(1);
        linkedListInteger.add(2);
        linkedListInteger.add(3);
        
        System.out.println(variosTipos.get(1));
        
        variosTipos.remove(2);
        
        //System.out.println("3 - Varios tipos perdeu o 2º elemento: " + variosTipos);
        
        Iterator<Number> iterator = variosTipos.iterator();
        
        while(iterator.hasNext()) {
            System.out.println("4 - Iterador: " + iterator.next());
        }
        
        if (variosTipos.contains(4.4)) {
            System.out.println("Elemento encontrado");
        } else {
            System.out.println("Elemento não encontrado!");
        }
        
        List<String> listaLetras = new ArrayList<>();
        
        listaLetras.add("D");
        listaLetras.add("C");
        listaLetras.add("E");
        listaLetras.add("B");
        listaLetras.add("A");
        
        System.out.println("Lista antes de ser ordenada: " + listaLetras);
        
        Collections.sort(listaLetras);
        
        System.out.println("Lista depois de ser ordenada: " + listaLetras);
        
        List<String> sourceList = new ArrayList<>();
        
        sourceList.add("A");
        sourceList.add("B");
        sourceList.add("C");
        sourceList.add("D");
        
        List<String> destList = new ArrayList<>();
        
        destList.add("V");
        destList.add("W");
        destList.add("X");
        destList.add("Y");
        destList.add("Z");
        
        System.out.println("destination List before copy: " + destList);
        
        //Collections.copy(destList, sourceList);
        
        System.out.println("destination List after copy: " + destList);
        
        List<Integer> numbers = new ArrayList<>();
        
        for(int i = 0; i < 10; i++) {
            numbers.add(i);
        }
        
        //System.out.println("A lista normal: " + numbers);
        
        Collections.reverse(numbers);
        
        //System.out.println("A lista invertida: " + numbers);
        
        List<String> listnames = Arrays.asList("Tom", "John", "Mary", "Peter", "David", "Alice");
        
        System.out.println("A lista original: " + listnames);
        
        List<String> subList = listnames.subList(2, 5);
        
        System.out.println("A sublista: " + subList);
    }
    
}
