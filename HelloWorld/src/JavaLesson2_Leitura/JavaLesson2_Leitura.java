package JavaLesson2_Leitura;

import java.util.Scanner;

public class JavaLesson2_Leitura {
    
    static Scanner in = new Scanner(System.in);
    
    public static void main (String[] args) {
        System.out.println("Introduza o seu numero:");
        
        if (in.hasNextInt()) {
            
            int number = in.nextInt();
            
            System.out.println("O nuemro introduzido foi " + number);
            
        }
        else {
            System.out.println("Da proxima vez introduza um numero inteiro!");
        }
        
        int numeroRandom = (int)(Math.random() *10);
        
        System.out.println(Math.abs(numeroRandom));
    }
    
}
