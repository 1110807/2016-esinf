
package JavaLesson5_Methods;

import java.util.Scanner;

public class JavaLesson5_Methods {
    
    //static double myPI = 3.1415; //Variavel de classe
    
    static int randomNumber; //Variavel de classe
    
    static Scanner in = new Scanner(System.in);
    
    public static void main(String[] args) {
        
        //System.out.println(addThem(1,2));
        //System.out.println("Global " + myPI);
        
        System.out.println(getRandomNum());
        
        int guessResult = 1;
        int randomGuess = 0;
        
        while(guessResult != -1) {
            System.out.print("Adivinhe o numero entre 0 e 50: ");
            randomGuess = in.nextInt();
            guessResult = checkGuess(randomGuess);
        }
        
        System.out.println("Sim o numero random é " + randomGuess);
        
        //tryToChange(d);
        //System.out.println("main d = " +d);
        
    }
    
    public static int getRandomNum() {
        
        randomNumber = (int)(Math.random()* 51);   
        return randomNumber;
    }
    
    public static int checkGuess(int guess) {
        
        if (guess== randomNumber) {
            return -1;
        } else {
            return guess;
        }
    }
    /*
    public static void tryToChange (int d) {
        
        d = d + 1;
        
        System.out.println("tryToChange d = " + d);
    }
    
    public static int addThem(int a, int b) {
        
        //double smallPI = 3.140; //Local Variable
        
        //double myPI = 3.0; //atribui um valor a variavel local que desaparece
        //depois do metodo terminar
        
        //myPI = myPI +3.0; /soma o valor a variavel global
        
        //System.out.println("Local " + myPI);
        
        int c = a + b;
        
        return c;
        
    }
    */
}
