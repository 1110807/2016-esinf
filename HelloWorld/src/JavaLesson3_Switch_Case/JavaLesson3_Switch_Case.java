
package JavaLesson3_Switch_Case;

public class JavaLesson3_Switch_Case {
    
    public static void main (String [] args) {
        
        int randomNumber = (int)(Math.random()*49);
        
        if (randomNumber < 25) {
            
            System.out.println("O numero é menor que 25! ");
            
        }
        
        System.out.println("O numero random é " + randomNumber);
        
        char theGrade = 'b';
        
        switch(theGrade) {
            case 'A':
                System.out.println("Great Job");
                break;
            case 'B':
                System.out.println("Good job");
                break;
            case 'C':
                System.out.println("Ok");
                break;
            default:
                System.out.println("You Failed");
                break;
        }
        
    
        
    }
    
}
