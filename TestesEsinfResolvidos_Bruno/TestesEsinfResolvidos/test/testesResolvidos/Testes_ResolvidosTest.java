/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testesResolvidos;

import AdjacencyMap.Graph;
import doublyLinkedList.DoublyLinkedList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author brunosantos
 */
public class Testes_ResolvidosTest {

    String joao;
    String maria;
    String rita;
    String joana;
    String raul;
    String antonio;
    String miguel;
    String manuel;
    String tiago;

    public Testes_ResolvidosTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        joao = "João";
        maria = "Maria";
        rita = "Rita";
        joana = "Joana";
        raul = "Raul";
        antonio = "António";
        miguel = "Miguel";
        manuel = "Manuel";
        tiago = "Tiago";
    }

    @After
    public void tearDown() {
    }

    //<editor-fold defaultstate="collapsed" desc="Recurso de fevereiro 2017">
    /**
     * Test of roletaRussa method, of class Testes_Resolvidos.
     */
    @Test
    public void testRoletaRussa() {
        System.out.println("roletaRussa");
        LinkedList<Integer> lst = new LinkedList<>();
        Integer temp[] = {2, 9, 7, 5, 10, 15, 6, 12, 3};
        lst.addAll(Arrays.asList(temp));
        Testes_Resolvidos instance = new Testes_Resolvidos();
        Map<Integer, LinkedList<Integer>> result = instance.roletaRussa(lst);

        System.out.println(result);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="exercicio 1 Época Normal de Janeiro 2017">
    /**
     * Test of KsubList method, of class Main.
     */
    @Test
    public void testKsubList() {
        System.out.println("KsubList");
        Testes_Resolvidos instance = new Testes_Resolvidos();
        LinkedList<Integer> list = new LinkedList<>();
        Integer b[] = {2, 9, 7, 5, 10, 15, 6, 12, 3};
        list.addAll(Arrays.asList(b));
        Integer a[] = {3, 6, 10};
        ArrayList<Integer> centers = new ArrayList<>();
        centers.addAll(Arrays.asList(a));
        Map<Integer, LinkedList<Integer>> expResult = new HashMap<>();
        Integer c[] = {3, 2};
        LinkedList<Integer> cc = new LinkedList<>();
        cc.addAll(Arrays.asList(c));
        expResult.put(3, cc);
        Integer d[] = {6, 7, 5};
        LinkedList<Integer> dd = new LinkedList<>();
        dd.addAll(Arrays.asList(d));
        expResult.put(6, dd);
        Integer e[] = {10, 9, 15, 12};
        LinkedList<Integer> ff = new LinkedList<>();
        ff.addAll(Arrays.asList(e));
        expResult.put(10, ff);
        Map<Integer, LinkedList<Integer>> result = instance.KsubList(list, centers);
        assertEquals(expResult, result);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="exercicio 4 Época Normal de Janeiro 2017">
    /**
     * Test of calculaPromocoes method, of class Main.
     */
    @Test
    public void testCalculaPromocoes() {
        System.out.println("calculaPromocoes");
        Testes_Resolvidos instance = new Testes_Resolvidos();
        Graph<String, Integer> g = new Graph(true);
        g.insertVertex(maria);
        g.insertVertex(joao);
        g.insertVertex(rita);
        g.insertVertex(joana);
        g.insertVertex(raul);
        g.insertVertex(antonio);
        g.insertVertex(miguel);
        g.insertVertex(manuel);
        g.insertVertex(tiago);
        g.insertEdge(maria, rita, 0, 0);
        g.insertEdge(joao, rita, 0, 0);
        g.insertEdge(maria, manuel, 0, 0);
        g.insertEdge(manuel, miguel, 0, 0);
        g.insertEdge(rita, joana, 0, 0);
        g.insertEdge(rita, raul, 0, 0);
        g.insertEdge(rita, antonio, 0, 0);
        g.insertEdge(antonio, tiago, 0, 0);
        g.insertEdge(joana, miguel, 0, 0);
        g.insertEdge(raul, miguel, 0, 0);

        Integer n = 3; // número de promoções

        List<String> result = instance.calculaPromocoes(g, n);
        System.out.println(result);

    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="exercicio 1 época de recurso de fevereiro de 2016">
    /**
     * Test of checkErrors method, of class Main.
     */
    @Test
    public void testCheckErrors() {
        System.out.println("checkErrors");
        Testes_Resolvidos instance = new Testes_Resolvidos();
        DoublyLinkedList<String> str = new DoublyLinkedList<>();
        Set<String> dictionary = new HashSet<>();
        String frase[] = {"os", "campos", "são", "berdes", "os", "zolhos", "dela", "são", "berdes", "e", "os", "meus", "zolhos", "tamen"};
        String dicionario[] = {"os", "campos", "são", "verdes", "os", "olhos", "dela", "são", "verdes", "e", "os", "meus", "olhos", "também"};
        for (int i = 0; i < frase.length; i++) {
            str.addLast(frase[i]);
            dictionary.add(dicionario[i]);
        }

        String temp[] = {"os", "campos", "são", "ERRO_01", "os", "ERRO_02", "dela", "são", "ERRO_01", "e", "os", "meus", "ERRO_02", "ERRO_03"};
        DoublyLinkedList<String> expResult = new DoublyLinkedList<>();
        for (int i = 0; i < temp.length; i++) {
            expResult.addLast(temp[i]);
        }
        DoublyLinkedList<String> result = instance.checkErrors(str, dictionary);

        assertEquals(expResult, result);
    }
    //</editor-fold>

}
