/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Heap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author brunosantos
 */
public class HeapPriorityQueueTest {

    HeapPriorityQueue<Integer, String> instance;

    Integer[] keys = {20, 15, 10, 13, 8, 12, 40, 30, 5, 21};
    String[] values = {"vinte", "quinze", "dez", "treze", "oito", "doze", "quarenta", "trinta", "cinco", "vinteeum"};

    public HeapPriorityQueueTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @Before
    public void setUp() {
        instance = new HeapPriorityQueue(keys, values);
    }

    /**
     * Test of parent method, of class HeapPriorityQueue.
     */
    @Test
    public void testParent() {
        System.out.println("parent");
        assertEquals(instance.parent(8), 3);
        assertEquals(instance.parent(2), 0);
    }

    /**
     * Test of left method, of class HeapPriorityQueue.
     */
    @Test
    public void testLeft() {
        System.out.println("left");
        int j = 4;

        int expResult = 9;
        int result = instance.left(j);
        assertEquals(expResult, result);
    }

    /**
     * Test of right method, of class HeapPriorityQueue.
     */
    @Test
    public void testRight() {
        System.out.println("right");
        int j = 4;

        int expResult = 10;
        int result = instance.right(j);
        assertEquals(expResult, result);
    }

    /**
     * Test of hasLeft method, of class HeapPriorityQueue.
     */
    @Test
    public void testHasLeft() {
        System.out.println("hasLeft");

        assertTrue("Têm esquerda", instance.hasLeft(3));
        assertFalse("Não têm esquerda", instance.hasLeft(5));
    }

    /**
     * Test of hasRight method, of class HeapPriorityQueue.
     */
    @Test
    public void testHasRight() {
        System.out.println("hasRight");
        assertTrue("Têm direita", instance.hasRight(3));
        assertFalse("Não têm direita", instance.hasRight(5));
    }

    /**
     * Test of swap method, of class HeapPriorityQueue.
     */
    @Test
    public void testSwap() {
        System.out.println("swap");
        Entry<Integer, String> temp2 = instance.heap.get(2);
        Entry<Integer, String> temp4 = instance.heap.get(4);
        String string2 = temp2.getValue();
        String string4 = temp4.getValue();
        instance.swap(2, 4);
        assertEquals("string2 deve ser igual ao novo string4",
                string2, instance.heap.get(4).getValue());
        assertEquals("string4 deve ser igual ao novo string2",
                string4, instance.heap.get(2).getValue());
    }

    /**
     * Test of percolateUp method, of class HeapPriorityQueue.
     */
    @Test
    public void testPercolateUp() {
        System.out.println("percolateUp");

        HeapPriorityQueue<Integer, String> heapInicial = instance.clone();
        instance.swap(0, instance.heap.size() - 1);
        assertFalse("deve ser falso pois ainda não foi feito o percolate",
                heapInicial.heap.get(0).equals(instance.heap.get(0)));
        instance.percolateUp(instance.heap.size() - 1);
        assertTrue("deve ser verdadeiro pois ainda já foi feito o percolate",
                heapInicial.heap.get(0).getValue().equals(
                        instance.heap.get(0).getValue()));
    }

    /**
     * Test of percolateDown method, of class HeapPriorityQueue.
     */
    @Test
    public void testPercolateDown() {
        System.out.println("percolateDown");
        HeapPriorityQueue<Integer, String> heapInicial = instance.clone();
        instance.swap(0, 3);
        assertFalse("deve ser falso pois ainda não foi feito o percolate",
                heapInicial.heap.get(3).equals(instance.heap.get(3)));
        instance.percolateDown(0);
        assertTrue("deve ser verdadeiro pois ainda já foi feito o percolate",
                heapInicial.heap.get(3).getValue().equals(
                        instance.heap.get(3).getValue()));
    }

    /**
     * Test of buildHeap method, of class HeapPriorityQueue.
     */
    @Test
    public void testBuildHeap() {
        System.out.println("buildHeap");
        HeapPriorityQueue<Integer, String> heapInicial = new HeapPriorityQueue<>();
        String um = "Um";
        String dois = "Dois";
        String vinte = "Vinte";
        String vinteCinco = "VinteeCinco";
        String trinta = "Trinta";
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add(um);
        expResult.add(dois);
        expResult.add(vinte);
        expResult.add(vinteCinco);
        expResult.add(trinta);
        ArrayList<Entry<Integer, String>> heap = heapInicial.heap;
        Entry<Integer, String> entry1 = new AbstractPriorityQueue.PQEntry<>(30, trinta);
        Entry<Integer, String> entry2 = new AbstractPriorityQueue.PQEntry<>(2, dois);
        Entry<Integer, String> entry3 = new AbstractPriorityQueue.PQEntry<>(20, vinte);
        Entry<Integer, String> entry4 = new AbstractPriorityQueue.PQEntry<>(25, vinteCinco);
        Entry<Integer, String> entry5 = new AbstractPriorityQueue.PQEntry<>(1, um);
        heap.add(entry1);
        heap.add(entry2);
        heap.add(entry3);
        heap.add(entry4);
        heap.add(entry5);
        Iterator<Entry<Integer, String>> it = heapInicial.heap.iterator();
        ArrayList<String> result = new ArrayList<>();
        while (it.hasNext()) {
            result.add(it.next().getValue());
        }
        assertFalse("é falsa, pois a heap inicial não está ordenada",
                expResult.equals(result));
        heapInicial.buildHeap();
        it = heapInicial.heap.iterator();
        result.clear();
        while (it.hasNext()) {
            result.add(it.next().getValue());
        }
        assertTrue("é verdadeira, pois a heap inicial já está ordenada",
                expResult.equals(result));

    }

    /**
     * Test of size method, of class HeapPriorityQueue.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        int expResult = 10;
        int result = instance.size();
        assertEquals(expResult, result);
    }

    /**
     * Test of min method, of class HeapPriorityQueue.
     */
    @Test
    public void testMin() {
        System.out.println("min");
        Entry<Integer, String> pq = instance.min();
        Integer k = pq.getKey();
        String v = pq.getValue();

        assertEquals(k, Integer.valueOf(5));
        assertEquals(v, "cinco");
    }

    /**
     * Test of insert method, of class HeapPriorityQueue.
     */
    @Test
    public void testInsert() {
        System.out.println("insert");
        instance.insert(3, "tres");

        Entry<Integer, String> pq = instance.min();
        Integer k = pq.getKey();
        String v = pq.getValue();

        assertEquals(v, "tres");

        String s = instance.toString();
        System.out.println(s);
    }

    /**
     * Test of removeMin method, of class HeapPriorityQueue.
     */
    @Test
    public void testRemoveMin() {
        System.out.println("removeMin");
        Entry<Integer, String> pq1 = instance.removeMin();
        String v1 = pq1.getValue();

        assertEquals(v1, "cinco");

        Entry<Integer, String> pq2 = instance.min();
        String v2 = pq2.getValue();

        assertEquals(v2, "oito");

        String s = instance.toString();
        System.out.println(s);
    }

    /**
     * Test of toString method, of class HeapPriorityQueue.
     */
    @Test
    public void testToString() {
        System.out.println("toString");

        System.out.println(instance);
    }

    /**
     * Test of clone method, of class HeapPriorityQueue.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        HeapPriorityQueue<Integer, String> temp = instance.clone();
        ArrayList<Entry<Integer, String>> heap = temp.heap;
        ArrayList<Entry<Integer, String>> heap1 = instance.heap;
        for (int i = 0; i < heap.size(); i++) {
            assertTrue("" + heap.get(i).getValue() + " = " + heap1.get(i).getValue(),
                    heap.get(i).getValue().equalsIgnoreCase(heap1.get(i).getValue()));
        }
        System.out.println(temp.toString());
    }

}
