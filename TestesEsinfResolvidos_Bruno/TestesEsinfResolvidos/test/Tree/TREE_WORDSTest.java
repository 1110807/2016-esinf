/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tree;

import Tree.TextWord;
import Tree.TREE_WORDS;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author brunosantos
 */
public class TREE_WORDSTest {

    public TREE_WORDSTest() {
    }

    /**
     * Test of createTree method, of class TREE_WORDS.
     */
    @Test
    public void testCreateTree() throws Exception {
        System.out.println("createTree");
        TREE_WORDS instance = new TREE_WORDS();
        instance.createTree();
        System.out.println(instance);
    }

    /**
     * Test of insert method, of class TREE_WORDS.
     */
    @Test
    public void testInsert() {
        System.out.println("insert");
        TextWord element = new TextWord("olá", 0);
        TREE_WORDS instance = new TREE_WORDS();
        instance.insert(element);
        System.out.println(instance);

    }

    /**
     * Test of getWordsOccurrences method, of class TREE_WORDS.
     */
    @Test
    public void testGetWordsOccurrences() throws FileNotFoundException {
        System.out.println("getWordsOccurrences");
        int[] occurExpected = {1, 2, 3};
        String[][] wordsExpected = {{"casaco", "correu", "do", "estava", "fecho", "frio", "pois"}, //1
        {"Luis", "a", "disse", "o", "ola"}, //2
        {"Maria"}};                                                //3

        TREE_WORDS instance = new TREE_WORDS();
        instance.createTree();
        Map<Integer, List<String>> occur = instance.getWordsOccurrences();

        int idx = 0;
        for (Map.Entry<Integer, List<String>> e : occur.entrySet()) {
            assertEquals(occurExpected[idx], e.getKey().intValue());
            assertEquals(Arrays.asList(wordsExpected[idx++]), e.getValue());
        }
    }

}
