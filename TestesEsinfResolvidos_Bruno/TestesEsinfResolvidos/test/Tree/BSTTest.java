/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tree;

import Tree.BST;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Tree.BST.Node;

/**
 *
 * @author brunosantos
 */
public class BSTTest {

    Integer[] arr = {20, 15, 50, 13, 8, 17, 40, 10, 30, 7};
    int[] height = {0, 1, 1, 2, 3, 3, 3, 4, 4, 4};
    Integer[] inorderT = {7, 8, 10, 13, 15, 17, 20, 30, 40, 50};
    Integer[] preorderT = {20, 15, 13, 8, 7, 10, 17, 50, 40, 30};
    Integer[] posorderT = {7, 10, 8, 13, 17, 15, 30, 40, 50, 20};
    BST<Integer> instance;

    public BSTTest() {
        instance = new BST();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        for (Integer i : arr) {
            instance.insert(i);
        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of root method, of class BST.
     */
    @Test
    public void testRoot() {
        System.out.println("root");
        System.out.println(instance);
        Integer expResult = 20;
        Integer result = instance.root().getElement();
        assertEquals(expResult, result);

    }

    /**
     * Test of isEmpty method, of class BST.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty com elementos");
        boolean expResult = false;
        boolean result = instance.isEmpty();
        assertEquals(expResult, result);
    }

    /**
     * Test of isEmpty method, of class BST.
     */
    @Test
    public void testIsEmpty_vazio() {
        System.out.println("isEmpty _ sem elementos");
        BST<Integer> newBST = new BST();
        boolean expResult = true;
        boolean result = newBST.isEmpty();
        assertEquals(expResult, result);
    }

    /**
     * Test of size method, of class BST.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        BST<String> newBST = new BST();
        assertEquals("the size should be 0", 0, newBST.size());
        newBST.insert("A");
        assertEquals("the size should be 1", 1, newBST.size());
        newBST.insert("B");
        assertEquals("the size should be 2", 2, newBST.size());
        newBST.insert("C");
        assertEquals("the size should be 3", 3, newBST.size());
    }

    /**
     * Test of insert method, of class BST.
     */
    @Test
    public void testInsert() {
        System.out.println("insert");
        int arr[] = {20, 15, 10, 13, 8, 17, 40, 50, 30, 20, 15, 10};
        BST<Integer> newBST = new BST();
        for (int i = 0; i < 9; i++) {
            newBST.insert(arr[i]);
            assertEquals("the size should be = " + (i + 1), newBST.size(), i + 1);
        }
        for (int i = 9; i < arr.length; i++) {
            newBST.insert(arr[i]); // aqui vamos inserir repetidos, que não vão ser contablilizados
            assertEquals("the size should be = 9", newBST.size(), 9);
        }
    }

    /**
     * Test of remove method, of class BST.
     */
    @Test
    public void testRemove() {
        System.out.println("remove");
        int size = arr.length;
        instance.remove(999);
        assertEquals("size should be = " + size, instance.size(), size);
        for (Integer i : arr) {
            instance.remove(i);
            size--;
            assertEquals("size should be = " + size, instance.size(), size);
        }
    }

    /**
     * Test of smallestElement method, of class BST.
     */
    @Test
    public void testSmallestElement_0args() {
        System.out.println("smallestElement");
        assertEquals(new Integer(7), instance.smallestElement());
        instance.remove(7);
        assertEquals(new Integer(8), instance.smallestElement());
        instance.remove(8);
        assertEquals(new Integer(10), instance.smallestElement());

    }

    /**
     * Test of smallestElement method, of class BST.
     */
    @Test
    public void testSmallestElement_BSTNode() {
        System.out.println("smallestElement");
        Integer result = instance.smallestElement(instance.root);
        assertEquals(new Integer(7), result);
    }

    /**
     * Test of nodesByLevel method, of class BST.
     */
    @Test
    public void testNodesByLevel() {
        System.out.println("nodesByLevel");
        Map<Integer, List<Integer>> expResult = new HashMap();
        expResult.put(0, Arrays.asList(new Integer[]{20}));
        expResult.put(1, Arrays.asList(new Integer[]{15, 50}));
        expResult.put(2, Arrays.asList(new Integer[]{13, 17, 40}));
        expResult.put(3, Arrays.asList(new Integer[]{8, 30}));
        expResult.put(4, Arrays.asList(new Integer[]{7, 10}));
        Map<Integer, List<Integer>> result = instance.nodesByLevel();
        System.out.println(instance);
        for (Map.Entry<Integer, List<Integer>> e : result.entrySet()) {
            assertEquals(expResult.get(e.getKey()), e.getValue());
        }

    }

    /**
     * Test of height method, of class BST.
     */
    @Test
    public void testHeight_0args() {
        System.out.println("height");
        BST newBst = new BST();
        assertEquals("height should be = -1", newBst.height(), -1);
        for (int i = 0; i < arr.length; i++) {
            newBst.insert(arr[i]);
            assertEquals("height should be = " + height[i], newBst.height(), height[i]);
        }
        System.out.println(newBst);
    }

    /**
     * Test of height method, of class BST.
     */
    @Test
    public void testHeight_BSTNode() {
        System.out.println("height");
        int expResult = 4;
        int result = instance.height(instance.root);
        assertEquals(expResult, result);
    }

    /**
     * Test of find method, of class BST.
     */
    @Test
    public void testFind() {
        System.out.println("find");
        Integer expResult = 40;
        Integer result = instance.find(new Integer(40), instance.root).getElement();
        assertEquals(expResult, result);

    }

    /**
     * Test of find method, of class BST.
     */
    @Test
    public void testFind_naoExistente() {
        System.out.println("find");
        BST.Node<Integer> expResult = null;
        BST.Node result = instance.find(new Integer(999), instance.root);
        assertEquals(expResult, result);

    }

    /**
     * Test of inOrder method, of class BST.
     */
    @Test
    public void testInOrder() {
        System.out.println("inOrder");
        Iterable<Integer> result = instance.inOrder();
        assertEquals(Arrays.asList(this.inorderT), result);

    }

    /**
     * Test of preOrder method, of class BST.
     */
    @Test
    public void testPreOrder() {
        System.out.println("preOrder");
        Iterable result = instance.preOrder();
        System.out.println(instance);
        assertEquals(Arrays.asList(this.preorderT), result);
    }

    /**
     * Test of postOrder method, of class BST.
     */
    @Test
    public void testPostOrder() {
        System.out.println("postOrder");
        Iterable result = instance.postOrder();
        System.out.println(instance);
        assertEquals(Arrays.asList(this.posorderT), result);
    }

    /**
     * Test of toString method, of class BST.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        BST instance = new BST();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

}
