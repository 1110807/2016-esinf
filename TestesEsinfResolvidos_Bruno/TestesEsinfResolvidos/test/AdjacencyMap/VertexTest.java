/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdjacencyMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author brunosantos
 */
public class VertexTest {

    Vertex<String, String> vA;
    Vertex<String, String> vB;
    Vertex<String, String> vC;
    Vertex<String, String> vD;
    Edge<String, String> eA;
    Edge<String, String> eB;
    Edge<String, String> eC;

    public VertexTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @Before
    public void setUp() {
        vA = new Vertex<>(0, "Porto");
        vB = new Vertex<>(1, "Aveiro");
        vC = new Vertex<>(2, "Coimbra");
        vD = new Vertex<>(3, "Lisboa");
        eA = new Edge<>("Edge1", 75.0, vA, vB);
        eB = new Edge<>("Edge2", 60.0, vB, vC);
        eC = new Edge<>("Edge3", 200.0, vC, vD);
        vA.addAdjVert("Aveiro", eA);
        vB.addAdjVert("Coimbra", eB);
        vC.addAdjVert("Lisboa", eC);
    }

    /**
     * Test of getKey method, of class Vertex.
     */
    @Test
    public void testGetKey() {
        System.out.println("getKey");

        assertTrue("deve ser 0", vA.getKey() == 0);
        assertTrue("deve ser 1", vB.getKey() == 1);
        assertTrue("deve ser 2", vC.getKey() == 2);
        assertTrue("deve ser 3", vD.getKey() == 3);

    }

    /**
     * Test of setKey method, of class Vertex.
     */
    @Test
    public void testSetKey() {
        System.out.println("setKey");
        assertTrue("deve ser 0", vA.getKey() == 0);
        vA.setKey(5);
        assertTrue("deve ser 5", vA.getKey() == 5);

    }

    /**
     * Test of getElement method, of class Vertex.
     */
    @Test
    public void testGetElement() {
        System.out.println("getElement");
        assertTrue("deve ser Porto", vA.getElement().equalsIgnoreCase("Porto"));
        assertFalse("não deve ser Aveiro", vA.getElement().equalsIgnoreCase("Aveiro"));
        assertTrue("deve ser Aveiro", vB.getElement().equalsIgnoreCase("Aveiro"));
        assertTrue("deve ser Coimbra", vC.getElement().equalsIgnoreCase("Coimbra"));
        assertTrue("deve ser Lisboa", vD.getElement().equalsIgnoreCase("Lisboa"));
    }

    /**
     * Test of setElement method, of class Vertex.
     */
    @Test
    public void testSetElement() {
        System.out.println("setElement");
        assertTrue("deve ser Porto", vA.getElement().equalsIgnoreCase("Porto"));
        vA.setElement("Faro");
        assertFalse("não deve ser Porto", vA.getElement().equalsIgnoreCase("Porto"));
        assertTrue("deve ser Faro", vA.getElement().equalsIgnoreCase("Faro"));

    }

    /**
     * Test of addAdjVert method, of class Vertex.
     */
    @Test
    public void testAddAdjVert() {
        System.out.println("addAdjVert");
        Vertex<String, String> instance = new Vertex<>(10, "Vila Real");
        assertTrue("deve ser 0", instance.numAdjVerts() == 0);
        Edge<String, String> eF = new Edge<>("edge_test", 23, instance, vB);
        instance.addAdjVert("Aveiro", eF);
        assertTrue("deve ser 1", instance.numAdjVerts() == 1);
        instance.addAdjVert("Coimbra", eC);
        assertTrue("deve ser 2", instance.numAdjVerts() == 2);
        Iterable<String> allAdjVerts = instance.getAllAdjVerts();
        System.out.println(allAdjVerts);

    }

    /**
     * Test of getAdjVert method, of class Vertex.
     */
    @Test
    public void testGetAdjVert() {
        System.out.println("getAdjVert");
        assertTrue("deve ser Aveiro", vA.getAdjVert(eA).equalsIgnoreCase("Aveiro"));
        assertFalse("não deve ser Lisboa", vA.getAdjVert(eA).equalsIgnoreCase("Lisboa"));
        assertTrue("não tem o edge", vA.getAdjVert(eC) == null);
    }

    /**
     * Test of remAdjVert method, of class Vertex.
     */
    @Test
    public void testRemAdjVert() {
        System.out.println("remAdjVert");
        assertTrue("deve ser Aveiro", vA.getAdjVert(eA).equalsIgnoreCase("Aveiro"));
        vA.remAdjVert("Aveiro");
        assertTrue("Não deve ter Aveiro", vA.getAdjVert(eA) == null);
    }

    /**
     * Test of getEdge method, of class Vertex.
     */
    @Test
    public void testGetEdge() {
        System.out.println("getEdge");
        assertTrue("deve ser eA", vA.getEdge("Aveiro") == eA);
        assertTrue("deve ser eB", vB.getEdge("Coimbra") == eB);
        assertTrue("deve ser eA", vC.getEdge("Lisboa") == eC);
        assertTrue("deve ser null", vA.getEdge("Lisboa") == null);
    }

    /**
     * Test of numAdjVerts method, of class Vertex.
     */
    @Test
    public void testNumAdjVerts() {
        System.out.println("numAdjVerts");
        assertTrue("deve ser igual a 1", vA.numAdjVerts() == 1);
        vA.addAdjVert("Coimbra", eB);
        assertTrue("deve ser igual a 2", vA.numAdjVerts() == 2);
    }

    /**
     * Test of getAllAdjVerts method, of class Vertex.
     */
    @Test
    public void testGetAllAdjVerts() {
        System.out.println("getAllAdjVerts");
        vA.addAdjVert("Coimbra", eB);
        Iterable result = vA.getAllAdjVerts();
        Iterator<String> it = result.iterator();
        List<String> temp = Arrays.asList("Aveiro", "Coimbra");
        for (String i : temp) {
            String vert = it.next();
            assertTrue("deve ser igual a " + i, i.equalsIgnoreCase(vert));
        }
    }

    /**
     * Test of getAllOutEdges method, of class Vertex.
     */
    @Test
    public void testGetAllOutEdges() {
        System.out.println("getAllOutEdges");
        Iterable<Edge<String, String>> allOutEdges = vA.getAllOutEdges();
        for (Edge<String, String> allOutEdge : allOutEdges) {
            assertTrue("deve ser igual a eA", eA.equals(allOutEdge));

        }
        vA.addAdjVert("Coimbra", eB);
        allOutEdges = vA.getAllOutEdges();
        Iterator it = allOutEdges.iterator();
        List<Edge> temp = new ArrayList<>(Arrays.asList(eA, eB));
        for (Edge i : temp) {
            assertTrue("deve ser igual a " + i, i.equals(it.next()));
        }

    }

    /**
     * Test of clone method, of class Vertex.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        assertTrue("o clone deve ser igual ao original", vA.clone().equals(vA));
        Vertex copia = vA.clone();
        assertTrue("o clone deve ser igual ao original", copia.equals(vA));
        copia.setElement("Faro");
        assertFalse("o clone alterado não deve ser igual ao original", copia.equals(vA));
    }

    /**
     * Test of equals method, of class Vertex.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");

        assertFalse("Aveiro não é igual ao Porto", vA.equals(vB));
        assertTrue("Porto é igual ao Porto", vA.equals(vA));

    }

    /**
     * Test of toString method, of class Vertex.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Vertex instance = new Vertex();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);

    }

}
