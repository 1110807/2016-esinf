/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdjacencyMap;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author brunosantos
 */
public class EdgeTest {

    Vertex<String, String> vA;
    Vertex<String, String> vB;
    Vertex<String, String> vC;
    Vertex<String, String> vD;
    Edge<String, String> eA;
    Edge<String, String> eB;
    Edge<String, String> eC;

    public EdgeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @Before
    public void setUp() {
        vA = new Vertex<>(0, "Porto");
        vB = new Vertex<>(1, "Aveiro");
        vC = new Vertex<>(2, "Coimbra");
        vD = new Vertex<>(3, "Lisboa");
        eA = new Edge<>("Edge1", 75.0, vA, vB);
        eB = new Edge<>("Edge2", 60.0, vB, vC);
        eC = new Edge<>("Edge3", 200.0, vC, vD);
        vA.addAdjVert("Aveiro", eA);
        vB.addAdjVert("Coimbra", eB);
        vC.addAdjVert("Lisboa", eC);
    }

    /**
     * Test of getElement method, of class Edge.
     */
    @Test
    public void testGetElement() {
        System.out.println("getElement");
        assertTrue("Deve ser Edge1", eA.getElement().equalsIgnoreCase("Edge1"));
        assertFalse("Não deve ser Edge2", eA.getElement().equalsIgnoreCase("Edge2"));
        assertTrue("Deve ser Edge2", eB.getElement().equalsIgnoreCase("Edge2"));
        assertTrue("Deve ser Edge3", eC.getElement().equalsIgnoreCase("Edge3"));
    }

    /**
     * Test of setElement method, of class Edge.
     */
    @Test
    public void testSetElement() {
        System.out.println("setElement");
        assertTrue("Deve ser Edge1", eA.getElement().equalsIgnoreCase("Edge1"));
        eA.setElement("novo Edge");
        assertTrue("Deve ser novo Edge", eA.getElement().equalsIgnoreCase("novo Edge"));
    }

    /**
     * Test of getWeight method, of class Edge.
     */
    @Test
    public void testGetWeight() {
        System.out.println("getWeight");
        assertTrue("deve ser 75", eA.getWeight() - 75.00 == 0);
        assertTrue("deve ser 60", eB.getWeight() - 60.00 == 0);
        assertTrue("deve ser 200", eC.getWeight() - 200.00 == 0);
    }

    /**
     * Test of setWeight method, of class Edge.
     */
    @Test
    public void testSetWeight() {
        System.out.println("setWeight");
        assertTrue("deve ser 75", eA.getWeight() - 75.00 == 0);
        eA.setWeight(45);
        assertTrue("deve ser 45", eA.getWeight() - 45.00 == 0);
    }

    /**
     * Test of getVOrig method, of class Edge.
     */
    @Test
    public void testGetVOrig() {
        System.out.println("getVOrig");
        assertTrue("O vertice inicial deve ser Porto", eA.getVOrig().equalsIgnoreCase("Porto"));
        assertFalse("O vertice inicial não deve ser Aveiro", eA.getVOrig().equalsIgnoreCase("Aveiro"));
        assertTrue("O vertice inicial deve ser Aveiro", eB.getVOrig().equalsIgnoreCase("Aveiro"));
        assertTrue("O vertice inicial deve ser Coimbra", eC.getVOrig().equalsIgnoreCase("Coimbra"));
    }

    /**
     * Test of setvOrig method, of class Edge.
     */
    @Test
    public void testSetvOrig() {
        System.out.println("setvOrig");
        assertTrue("O vertice inicial deve ser Porto", eA.getVOrig().equalsIgnoreCase("Porto"));
        eA.setvOrig(vD);
        assertTrue("O vertice inicial deve ser Lisboa", eA.getVOrig().equalsIgnoreCase("Lisboa"));
    }

    /**
     * Test of getVDest method, of class Edge.
     */
    @Test
    public void testGetVDest() {
        System.out.println("getVDest");
        assertTrue("O vertice final deve ser Aveiro", eA.getVDest().equalsIgnoreCase("Aveiro"));
        assertFalse("O vertice final não deve ser Porto", eA.getVDest().equalsIgnoreCase("Porto"));
        assertTrue("O vertice final deve ser Coimbra", eB.getVDest().equalsIgnoreCase("Coimbra"));
        assertTrue("O vertice final deve ser Lisboa", eC.getVDest().equalsIgnoreCase("Lisboa"));
    }

    /**
     * Test of setvDest method, of class Edge.
     */
    @Test
    public void testSetvDest() {
        System.out.println("setvDest");
        assertTrue("O vertice final deve ser Aveiro", eA.getVDest().equalsIgnoreCase("Aveiro"));
        eA.setvDest(vD);
        assertTrue("O vertice final deve ser Lisboa", eA.getVDest().equalsIgnoreCase("Lisboa"));

    }

    /**
     * Test of getEndpoints method, of class Edge.
     */
    @Test
    public void testGetEndpoints() {
        System.out.println("getEndpoints");
        String[] expResult = {"Porto", "Aveiro"};
        String[] result = eA.getEndpoints();
        assertArrayEquals(expResult, result);

    }

    /**
     * Test of equals method, of class Edge.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        assertFalse("o Edge1 deve ser diferente de Edge2", eA.equals(eB));
        assertTrue("o Edge1 deve ser igual a Edge1", eA.equals(eA));
        Edge<String, String> eNovo = new Edge<>("Edge1", 75.0, vA, vB);
        assertTrue("o Edge1 deve ser igual a eNovo", eA.equals(eNovo));
        Edge<String, String> eNovo1 = new Edge<>("EdgeNovo", 75.0, vA, vB);
        assertFalse("o Edge1 não deve ser igual a eNovo1", eA.equals(eNovo1));
        Edge<String, String> eNovo2 = new Edge<>("Edge1", 70.0, vA, vB);
        assertFalse("o Edge1 não deve ser igual a eNovo2", eA.equals(eNovo2));
        Edge<String, String> eNovo3 = new Edge<>("Edge1", 75.0, vC, vB);
        assertFalse("o Edge1 não deve ser igual a eNovo3", eA.equals(eNovo3));

    }

    /**
     * Test of clone method, of class Edge.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        assertTrue("o Edge1 deve ser igual a Edge1", eA.equals(eA));
        Edge<String, String> eNovo = eA.clone();
        assertTrue("o Edge1 deve ser igual a Edge1", eA.equals(eNovo));
    }

    /**
     * Test of toString method, of class Edge.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "      (Edge1) - 75.0 - Aveiro\n";
        String result = eA.toString();
        assertEquals(expResult, result);

    }

    /**
     * Test of compareTo method, of class Edge.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        assertTrue("Edge1 deve ser igual a Edge1", eA.compareTo(eA) == 0);
        assertTrue("Edge1 deve ser maior a Edge2", eA.compareTo(eB) > 0);
        assertTrue("Edge1 deve ser menor a Edge3", eA.compareTo(eC) < 0);
    }

}
