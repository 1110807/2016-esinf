/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tree;

/**
 *
 * @author brunosantos
 */
class TextWord implements Comparable<TextWord> {

    private String word;
    private int ocorrences;

    public TextWord(String word, int ocorrences) {
        setWord(word, ocorrences);
    }

    /**
     * @param word the word to set
     */
    public void setWord(String word, int ocorrences) {
        this.word = word;
        this.ocorrences = ocorrences;
    }

    /**
     * increments ocorrences
     */
    public void inOcorrences() {
        this.ocorrences++;
    }

    /**
     * @return the word
     */
    public String getWord() {
        return word;
    }

    /**
     * @return the ocorrences
     */
    public int getOcorrences() {
        return ocorrences;
    }

    @Override
    public int compareTo(TextWord o) {
        return word.compareTo(o.getWord());
    }

    @Override
    public String toString() {
        return "<" + word + ">:" + ocorrences;
    }

}
