/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tree;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author brunosantos
 * @param <E>
 */
public class BST<E extends Comparable<E>> {

    //<editor-fold defaultstate="collapsed" desc="NESTEED_CLASS_NODE">
    /**
     * nested static class para um nó(node) de uma Binary Search Tree(BST).
     *
     * @param <E>
     */
    protected static class Node<E> {

        /**
         * Os elementos existentes num nó
         */
        private E element;          // o elemento do nó
        private Node<E> left;      // o elemento à esquerda do elemento (se existir)
        private Node<E> right;     // o elemento à direita do elemento (se existir)

        /**
         * Construtor da nested class
         *
         * @param element
         * @param leftChild
         * @param rightChild
         */
        public Node(E element, Node<E> leftChild, Node<E> rightChild) {
            this.element = element;
            this.left = leftChild;
            this.right = rightChild;
        }

        /**
         * get element
         *
         * @return o elemento do nó
         */
        public E getElement() {
            return element;
        }

        /**
         * get Nó filho à esquerda
         *
         * @return Nó filho à esquerda, mais pequeno que o nó
         */
        public Node<E> getLeft() {
            return left;
        }

        /**
         * Nó filho à direita
         *
         * @return Nó filho à direita, mais alto que o nó
         */
        public Node<E> getRight() {
            return right;
        }

        /**
         * @param element the element to set
         */
        public void setElement(E element) {
            this.element = element;
        }

        /**
         * @param left the left to set
         */
        public void setLeft(Node<E> left) {
            this.left = left;
        }

        /**
         * @param right the right to set
         */
        public void setRight(Node<E> right) {
            this.right = right;
        }

    }
    //</editor-fold>

    /**
     * a Raiz da árvore binária
     */
    protected Node<E> root = null; // raiz da árover binária

    /**
     * construtor vazio de uma árvore binária
     */
    public BST() {
        this.root = null;
    }

    /**
     * retorna o nó raiz da árvore ou null de a árvore estiver vazia
     *
     * @return o nó raiz da árvore ou null de a árvore estiver vazia
     */
    protected Node<E> root() {
        return this.root;
    }

    /**
     * retorna true se a árvore estiver vazia e false caso não esteja vazia
     *
     * @return true se a árvore estiver vazia e false caso não esteja vazia
     */
    public boolean isEmpty() {
        return this.root == null;
    }

    //<editor-fold defaultstate="collapsed" desc="MÉTODOS DA CLASSE">
    //###################################################################################
    /**
     * Método público que chama um método privado para devolver o número de nós
     * que tem a árvore binária
     *
     * @return o número de nós que tem a árvore binária
     */
    public int size() {
        return size(root);
    }

    /**
     * método recursivo para saber o numero de nós que tem a árvore binária a
     * partir do nó enviado como parâmentro inicial
     *
     * @param node
     * @return 0 se o nó for null
     */
    private int size(Node<E> node) {
        if (node == null) {
            return 0;
        }
        return (size(node.left) + 1 + size(node.right));
    }
    //###################################################################################

    /**
     * Método público para inserir um novo nó na árvore chama um método privado
     * para saber onde inserir o nó
     *
     * @param element
     */
    public void insert(E element) {
        root = insert(element, root);
    }

    /**
     * Método privado e recursivo para inserir um novo nó a partir de um
     * determinado nó
     *
     * @param element
     * @param node
     * @return
     */
    private Node<E> insert(E element, Node<E> node) {
        if (node == null) {
            return new Node(element, null, null);   // cria um novo nó sem nós à esquerda ou à direita
        }

        if (node.getElement().compareTo(element) == 0) {
            node.setElement(element);
        } else if (node.getElement().compareTo(element) > 0) {
            node.setLeft(insert(element, node.getLeft()));
        } else {
            node.setRight(insert(element, node.getRight()));
        }

        return node;
    }

    //###################################################################################
    /**
     * método público para remover o nó que tenha o elemento chama o método
     * recursivo para efectuar a remoção
     *
     * @param element
     */
    public void remove(E element) {
        root = remove(element, root());
    }

    /**
     * método privado para remover o nó com o elemento, inicia-se com o root da
     * árvore
     *
     * @param element
     * @param node
     * @return o novo root após a remoção do nó com o elemento
     */
    private Node<E> remove(E element, Node<E> node) {
        if (node == null) {
            return null;
        }

        if (node.getElement().compareTo(element) < 0) {
            Node<E> rNode = remove(element, node.getRight());
            node.setRight(rNode);
        } else if (node.getElement().compareTo(element) > 0) {
            Node<E> rNode = remove(element, node.getLeft());
            node.setLeft(rNode);
        } else {
            if (node.getLeft() == null) {
                return node.getRight();
            }
            if (node.getRight() == null) {
                return node.getLeft();
            }

            E min = smallestElement(node.getRight());
            node.setElement(min);
            node.setRight(remove(min, node.getRight()));
        }
        return node;
    }

    //###################################################################################
    /**
     * método público que devolve o elemento mais pequeno da árvore, chama o
     * método recursivo de procura
     *
     * @return o elemento mais pequeno da árvore
     */
    public E smallestElement() {
        return smallestElement(root);
    }

    /**
     * método protegido sendo recursivo, em que procura o elemento mais pequeno
     * na árvore
     *
     * @param node
     * @return o elemento mais pequeno existente na árvore
     */
    protected E smallestElement(Node<E> node) {
        if (node == null) {
            return null;
        }
        E leftElement = smallestElement(node.getLeft());
        if (leftElement != null) {
            return leftElement;
        }
        return node.getElement();
    }

    //###################################################################################
    public Map<Integer, List<E>> nodesByLevel() {
        Map<Integer, List<E>> nodesPerLevel = new LinkedHashMap<>();
        processBstByLevel(root, nodesPerLevel, 0);
        return nodesPerLevel;
    }

    private void processBstByLevel(Node<E> node, Map<Integer, List<E>> result, int level) {
        LinkedList<Node<E>> currentLevel = new LinkedList<>();
        LinkedList<Node<E>> nextLevel = new LinkedList<>();
        LinkedList<E> elements = new LinkedList<>();
        currentLevel.add(node);

        while (!currentLevel.isEmpty()) {
            Node<E> nParent = currentLevel.remove();
            elements.add(nParent.getElement());

            if (nParent.getLeft() != null) {
                nextLevel.add(nParent.getLeft());
            }
            if (nParent.getRight() != null) {
                nextLevel.add(nParent.getRight());
            }
            if (currentLevel.isEmpty()) {
                result.put(level, new LinkedList<>(elements));
                elements.clear();
                level++;
                currentLevel.addAll(nextLevel);
                nextLevel.clear();
            }
        }
    }

    //###################################################################################
    /**
     * método público que retorna a altura da árvore A altura começa da raiz
     * (cima) para baixo
     *
     * @return altura desde a raiz até abaixo
     */
    public int height() {
        return height(root);
    }

    /**
     * método protected, que retorna a altura da árvore a partir de um
     * determinado nó, este método é recursivo.
     *
     * @param node
     * @return altura da árvore desde um determinado nó
     */
    protected int height(Node<E> node) {
        if (node == null) {
            return -1;
        }
        int heightLeft = height(node.getLeft());
        int heightRight = height(node.getRight());
        if (heightLeft > heightRight) {
            return heightLeft + 1;
        }
        return heightRight + 1;
    }

    //###################################################################################
    /**
     * Método que devolve o nó da árvore que tem o elemento
     *
     * @param element
     * @param node
     * @return o nó que tem o elementos
     */
    protected Node<E> find(E element, Node<E> node) {
        Node<E> nodeFound = null;
        if (element != null && node != null) {
            if (node.getElement().compareTo(element) == 0) {
                nodeFound = node;
            } else if (node.getElement().compareTo(element) < 0) {
                nodeFound = find(element, node.getRight());
            } else if (node.getElement().compareTo(element) > 0) {
                nodeFound = find(element, node.getLeft());
            }
        }
        return nodeFound;
    }
    //###################################################################################
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ORDENAÇÕES DA ÁRVORE">
    /**
     * método público que devolve a árvore ordenada por ordem crescente do
     * valor, este método chama um método privado que é recursivo, enviando como
     * parâmetro o root da árvore.
     *
     * @return um iterable (linkedList) por ordem crescente do valor
     */
    public Iterable<E> inOrder() {
        List<E> treeInOrder = new LinkedList<>();
        inOrderSubTree(root, treeInOrder);
        return treeInOrder;
    }

    /**
     * método privado e recursivo que alimenta um Iterable com valores por ordem
     * crescente
     *
     * @param node
     * @param snapshot
     */
    private void inOrderSubTree(Node<E> node, List<E> snapshot) {
        if (node != null) {
            inOrderSubTree(node.getLeft(), snapshot);
            snapshot.add(node.getElement());
            inOrderSubTree(node.getRight(), snapshot);
        }
    }

    /**
     * método público que devolve a árvore ordenada por ordem préOrdenada
     * (primeiro o root, depois percorrendo os ramos à esquerda até não existir
     * mais nada aí passa passa para o seu ramo à direita) do valor, este método
     * chama um método privado que é recursivo, enviando o root da árvore.
     *
     * @return um iterable (linkedList) por preOrder
     */
    public Iterable<E> preOrder() {
        List<E> treePreOrder = new LinkedList<>();
        preOrderSubTree(root, treePreOrder);
        return treePreOrder;
    }

    /**
     * método privado e recursivo que alimenta um Iterable com valores ordenados
     * por preOrder
     *
     * @param node
     * @param snapshot
     */
    private void preOrderSubTree(Node<E> node, List<E> snapshot) {
        if (node != null) {
            snapshot.add(node.getElement());
            preOrderSubTree(node.getLeft(), snapshot);
            preOrderSubTree(node.getRight(), snapshot);
        }
    }

    /**
     * método público que devolve a árvore ordenada por ordem pósOrdenada começa
     * no nivel mais pequeno, com o nó à esquerda passa para a direita passa
     * para o nó pai desse e continua desse modo até ao fim do valor, este
     * método chama um método privado que é recursivo, enviando o root da
     * árvore.
     *
     * @return um iterable (linkedList) por postOrder
     */
    public Iterable<E> postOrder() {
        List<E> treePostOrder = new LinkedList<>();
        postOrderSubtree(root, treePostOrder);
        return treePostOrder;
    }

    /**
     * método privado e recursivo que alimenta um Iterable com valores ordenados
     * por preOrder
     *
     * @param node
     * @param snapshot
     */
    private void postOrderSubtree(Node<E> node, List<E> snapshot) {
        if (node != null) {
            postOrderSubtree(node.getLeft(), snapshot);
            postOrderSubtree(node.getRight(), snapshot);
            snapshot.add(node.getElement());
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="TOSTRING">
    /**
     * to String
     *
     * @return representação da árvore binária BST
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        toStringRec(root, 0, sb);
        return sb.toString();
    }

    /**
     * método recursivo para alimentar o toString da BST
     *
     * @param root
     * @param level
     * @param sb
     */
    private void toStringRec(Node<E> root, int level, StringBuilder sb) {
        if (root == null) {
            return;
        }
        toStringRec(root.getRight(), level + 1, sb);
        if (level != 0) {
            for (int i = 0; i < level - 1; i++) {
                sb.append("|\t");
            }
            sb.append("|-------" + root.getElement() + "\n");
        } else {
            sb.append(root.getElement() + "\n");
        }
        toStringRec(root.getLeft(), level + 1, sb);
    }
    //</editor-fold>

}
