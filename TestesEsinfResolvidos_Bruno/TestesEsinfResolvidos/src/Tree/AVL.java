/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tree;

/**
 *
 * @author brunosantos
 * @param <E>
 */
public class AVL<E extends Comparable<E>> extends BST<E> {

    //<editor-fold defaultstate="collapsed" desc="MÉTODOS DA CLASSE">
    /**
     * método privado que retorna o factor de balanço do nó na árvore
     * height(right subtree) - height(left subtree)
     *
     * @param node
     * @return balance factor of the tree
     */
    private int balanceFactor(Node<E> node) {
        return height(node.getRight()) - height(node.getLeft());
    }

    /**
     * método privado que retorna o nó ao realizar uma rotação à direita do nó
     * enviado como parâmetro
     *
     * @param node
     * @return o nó ao realizar uma rotação à direita
     */
    private Node<E> rightRotation(Node<E> node) {
        Node<E> leftSon = node.getLeft();
        node.setLeft(leftSon.getRight());
        leftSon.setRight(node);
        node = leftSon;
        return node;
    }

    /**
     * método privado que retorna o nó ao realizar uma rotação à direita do nó
     * enviado como parâmetro
     *
     * @param node
     * @return o nó ao realizar uma rotação à esquerda
     */
    private Node<E> leftRotation(Node<E> node) {

        Node<E> rightSon = node.getRight();
        node.setRight(rightSon.getLeft());
        rightSon.setLeft(node);
        node = rightSon;

        return node;
    }

    /**
     * método privado que retorna o nó ao realizar uma rotação dupla do
     * parâmetro , vai verificar em primeiro lugar se tem de fazer uma rotação à
     * direita ou à esquerda chamando o método balanceFactor(). Este método
     * serve para quando o factor de balanço da árvore tem sinal diferente do
     * factor de balanço do seu nó filho
     *
     * @param node
     * @return
     */
    private Node<E> twoRotations(Node<E> node) {
        if (balanceFactor(node) < 0) {
            node.setLeft(leftRotation(node.getLeft()));
            node = rightRotation(node);
        } else {
            node.setRight(rightRotation(node.getRight()));
            node = leftRotation(node);
        }
        return node;
    }

    /**
     * método privado que vai ser chamado ao ser inserido um novo nó na árvore e
     * vai verificar se é necessário uma rotação simples ou dupla
     *
     * @param node
     * @return node cpmpleto com os left's e right's
     */
    private Node<E> balanceNode(Node<E> node) {
        if (balanceFactor(node) < 0) {
            if (balanceFactor(node.getLeft()) < 0) {
                node = rightRotation(node);
            } else if (balanceFactor(node.getLeft()) > 0) {
                node = twoRotations(node);
            }
        } else if (balanceFactor(node) > 0) {
            if (balanceFactor(node.getRight()) > 0) {
                node = leftRotation(node);
            } else if (balanceFactor(node.getRight()) < 0) {
                node = twoRotations(node);
            }
        }
        return node;
    }

    /**
     * método que deriva do BST para inserir um novo elemento na árvore AVL
     *
     * @param element
     */
    @Override
    public void insert(E element) {
        root = insert(element, root);
    }

    /**
     * método privado para inserir um novo elemento na árvore AVL, é enviado
     * como parâmentro o root(caso exista)
     *
     * @param element
     * @param node
     * @return o nó onde estará inserido o elemento
     */
    private Node<E> insert(E element, Node<E> node) {
        if (node == null) {
            return new Node(element, null, null);
        }

        if (node.getElement().compareTo(element) == 0) {
            node.setElement(element);
        } else if (node.getElement().compareTo(element) > 0) {
            node.setLeft(insert(element, node.getLeft()));
            node = balanceNode(node);
        } else {
            node.setRight(insert(element, node.getRight()));
            node = balanceNode(node);
        }

        return node;
    }

    /**
     * método público que deriva do BST para remover um elemento da árvore AVL
     *
     * @param element
     */
    @Override
    public void remove(E element) {
        root = remove(element, root());
    }

    /**
     * método privado para remover um elemento da árvore AVL, é enviado como
     * parâmentro o root(caso exista)
     *
     * @param element
     * @param node
     * @return
     */
    private Node<E> remove(E element, Node<E> node) {
        if (node == null) {
            return null;
        }

        if (node.getElement().compareTo(element) > 0) {
            node.setLeft(remove(element, node.getLeft()));
            node = balanceNode(node);
        } else if (node.getElement().compareTo(element) < 0) {
            node.setRight(remove(element, node.getRight()));
            node = balanceNode(node);
        } else {
            if (node.getLeft() == null && node.getRight() == null) {
                return null;
            }

            if (node.getLeft() == null) {
                return node.getRight();
            }

            if (node.getRight() == null) {
                return node.getLeft();
            }

            E smallElem = smallestElement(node.getRight());
            node.setElement(smallElem);
            node.setRight(remove(smallElem, node.getRight()));
            node = balanceNode(node);
        }

        return node;
    }

    /**
     * verifica se uma AVL é igual a outra
     *
     * @param second
     * @return true se são iguais, false caso contrário
     */
    public boolean equals(AVL<E> second) {
        return equals(root, second.root);
    }

    /**
     * método que é chamado pelo equals para verificar se todos os nós das
     * árvores AVL coincidem Este é um método recursivo
     *
     * @param root1
     * @param root2
     * @return
     */
    public boolean equals(Node<E> root1, Node<E> root2) {
        if (root1 == null && root2 == null) {
            return true;
        } else if (root1 != null && root2 != null) {
            if (root1.getElement().compareTo(root2.getElement()) == 0) {
                return equals(root1.getLeft(), root2.getLeft())
                        && equals(root1.getRight(), root2.getRight());
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    //</editor-fold>
}
