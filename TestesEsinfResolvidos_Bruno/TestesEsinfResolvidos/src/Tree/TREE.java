/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tree;

import Tree.BST;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author brunosantos
 * @param <E>
 */
public class TREE<E extends Comparable<E>> extends BST<E> {

    //<editor-fold defaultstate="collapsed" desc="NESTEED_CLASS_BSTITERATOR">
    /**
     * Neested class BSTIterador
     */
    private class BSTIterator implements Iterator<E> {

        /**
         * atributo da classe
         */
        private final Stack<Node<E>> stack;
        E curElement;       // o presente elemento
        boolean canRemove;  // para poder utilizar o remove()

        /**
         * Construtor vazio da classe
         */
        public BSTIterator() {
            stack = new Stack<>();
            Node<E> cur = (Node<E>) root();
            while (cur != null) {
                stack.push(cur);
                cur = cur.getLeft();
            }
            this.curElement = null;
            this.canRemove = false;
        }

        /**
         *
         * @return true se houver algum elemento a seguir
         */
        @Override
        public boolean hasNext() {
            return !stack.isEmpty();
        }

        /**
         *
         * @return o próximo elemento
         */
        @Override
        public E next() {
            Node<E> curr = stack.pop();
            this.curElement = curr.getElement();
            curr = curr.getRight();
            while (curr != null) {
                stack.push(curr);
                curr = curr.getLeft();
            }
            this.canRemove = true;
            return curElement;
        }

        /**
         * Método para remover o elemento
         */
        @Override
        public void remove() {
            if (!canRemove) {
                throw new IllegalStateException("You can't remove the element.");
            }
            TREE.super.remove(curElement);
            this.curElement = null;
            this.canRemove = false;
            this.stack.clear();
        }

    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="MÉTODOS DA CLASSE">
    //###################################################################################
    /**
     * método público que devolve o tamanho da árvore a partir do elemento chama
     * um método privado que é chamado recursivamente, que vai percorrendo a
     * arvore e incrementando o contador
     *
     * @param element
     * @return devolve o tamanho da árvore a partir do elemento
     */
    public int depth(E element) {
        return (depth(element, root));
    }

    /**
     * Método privado que é chamado recursivamente para saber o tamanho da
     * árvore a partir do nó inicial
     *
     * @param element
     * @param node
     * @return devolve o tamanho da árvore a partir do elemento
     */
    private int depth(E element, Node<E> node) {
        if (node != null) {
            if (node.getElement().compareTo(element) == 0) {
                return 0;
            } else if (node.getElement().compareTo(element) < 0) {
                return depth(element, node.getRight()) + 1;
            } else if (node.getElement().compareTo(element) > 0) {
                return depth(element, node.getLeft()) + 1;
            }
        }
        throw new IllegalArgumentException("Element does not exist in tree.");
    }
    //###################################################################################

    /**
     * procura na árvore se um nó da árvore tem este elemento
     *
     * @param element
     * @return
     */
    public boolean contains(E element) {
        return find(element, root) != null;
    }

    //###################################################################################
    /**
     * verifica na árvore se o elemento é uma folha ou seja, está no nivel mais
     * baixo da árvore
     *
     * @param element
     * @return true se o elemento está no nivel mais baixo da árvore false caso
     * contrário
     */
    public boolean isLeaf(E element) {
        Node<E> node = find(element, root);
        return node != null && node.getLeft() == null && node.getRight() == null;
    }
    //###################################################################################

    /**
     * Returns the parent's Element of an Element (or null if Element is the
     * root or not belongs to the tree).
     *
     * @param element A valid element within the tree
     * @return Element of element's parent (or null if element is root)
     */
    public E parent(E element) {
        if (root.getElement().compareTo(element) == 0) {
            return null;
        }
        Node<E> node = node = this.find(element, root);
        return parent(node, root);
    }

    /**
     * método privado que é chamado recursivamente para descobrir o elemento pai
     * do elemento passado por parâmetro
     *
     * @param node
     * @param parentNode
     * @return Element of element's parent (or null if element is root)
     */
    private E parent(Node<E> node, Node<E> parentNode) {
        E element = node.getElement();
        if (parentNode != null) {
            if (parentNode.getLeft().getElement().compareTo(element) == 0
                    || parentNode.getRight().getElement().compareTo(element) == 0) {
                return parentNode.getElement();
            } else if (parentNode.getElement().compareTo(element) > 0) {
                return parent(node, parentNode.getLeft());
            } else {
                return parent(node, parentNode.getRight());
            }
        }
        return null;
    }
    //###################################################################################

    /**
     * @return Iterable com os elementos dos nós da árvore binária ordenado
     * inOrder (do mais pequeno para o maior)
     */
    public Iterator<E> iterator() {
        return new BSTIterator();
    }
    //###################################################################################

    /**
     * Return the tree without leaves, chama o método privado copyRec para
     * retornar a árvore binária
     *
     * @return tree without leaves
     */
    public BST<E> autumnTree() {
        BST<E> tree = new TREE();
        tree.root = copyRec(root);
        return tree;
    }

    /**
     * método privado para retornar a árvore sem as folhas, este método é
     * chamado recursivamente
     *
     * @param node
     * @return node
     */
    private Node<E> copyRec(Node<E> node) {
        Node<E> newNode = null;
        if (node != null && (node.getLeft() != null || node.getRight() != null)) {
            newNode = new Node(node.getElement(), copyRec(node.getLeft()),
                    copyRec(node.getRight()));
        }
        return newNode;
    }

    //###################################################################################
    /**
     * build a list with all elements of the tree. The elements in the left
     * subtree in ascending order and the elements in the right subtree in
     * descending order. Este método chama dois métodos privados que são
     * recursivos
     *
     * @return returns a list with the elements of the left subtree in ascending
     * order and the elements in the right subtree is descending order.
     */
    public Iterable<E> ascdes() {
        List<E> list = new LinkedList<>();
        ascSubtree(root.getLeft(), list);
        list.add(root.getElement());        // adiciona o root à lista
        desSubtree(root.getRight(), list);
        return list;
    }

    /**
     * este método recursivo trata a parte esquerda da árvore a partir da raiz
     *
     * @param node
     * @param snapshot
     */
    private void ascSubtree(Node<E> node, List<E> snapshot) {
        if (node != null) {
            ascSubtree(node.getLeft(), snapshot);
            snapshot.add(node.getElement());
            ascSubtree(node.getRight(), snapshot);
        }
    }

    /**
     * este método recursivo trata da parte direita da árvore
     *
     * @param node
     * @param snapshot
     */
    private void desSubtree(Node<E> node, List<E> snapshot) {
        if (node != null) {
            desSubtree(node.getRight(), snapshot);
            snapshot.add(node.getElement());
            desSubtree(node.getLeft(), snapshot);
        }
    }
    //###################################################################################
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="MÉTODOS DE RESOLUÇÃO DOS TESTES">
    //##### PERGUNTA 3 - TESTE DE RECURSO DE 9 DE FEVEREIRO 2017 ########################
    //</editor-fold>
}
