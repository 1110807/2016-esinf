/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testesResolvidos;

import AdjacencyMap.Graph;
import doublyLinkedList.DoublyLinkedList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author brunosantos
 */
public class Testes_Resolvidos {

    //<editor-fold defaultstate="collapsed" desc="exercicio 1 Recurso de fevereiro 2017">
    Map<Integer, LinkedList<Integer>> roletaRussa(LinkedList<Integer> lst) {
        Map<Integer, LinkedList<Integer>> output = new HashMap<>();
        Random generator = new Random();

        int indexInicial = 0;
        int n = lst.size();

        for (int i = 1; i < n; i++) {
            int sizeOfList = lst.size();
            int ind = generator.nextInt(sizeOfList);
            indexInicial = (indexInicial + ind) % sizeOfList;
            LinkedList<Integer> temp = new LinkedList<>();
            lst.remove(lst.get(indexInicial));
            lst.forEach(x -> temp.add(x));
            output.put(i, temp);
        }
        return output;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="exercicio 1 Época Normal de Janeiro 2017">
    /**
     * resposta ao exercicio 1 da prova da época Normal 30 de janeiro de 2017
     *
     * @param list
     * @param centers
     * @return
     */
    public static Map<Integer, LinkedList<Integer>> KsubList(LinkedList<Integer> list, ArrayList<Integer> centers) {
        Map<Integer, LinkedList<Integer>> output = new HashMap<>();
        for (Integer center : centers) {
            LinkedList<Integer> temp = new LinkedList<>();
            temp.add(center);
            output.put(center, temp);
        }

        for (Integer l : list) {
            if (!centers.contains(l)) {
                Integer min = Integer.MAX_VALUE;
                Integer c = 0;
                for (Integer center : centers) {
                    int dif = Math.abs(center - l);
                    if (dif < min) {
                        min = dif;
                        c = center;
                    }
                }
                output.get(c).add(l);
            }
        }
        return output;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="exercicio 4 Época Normal de Janeiro 2017">
    /**
     * exercicio 4 da prova da época Normal 30 de Janeiro de 2017
     *
     * @param g
     * @param n
     * @return
     */
    public List<String> calculaPromocoes(Graph<String, Integer> g, Integer n) {
        Graph<String, Integer> gClone = g.clone();
        ArrayList<String> promos = new ArrayList<>();
        Iterator<String> allVerts = gClone.vertices().iterator();
        while (allVerts.hasNext()) {
            if (n == promos.size()) {
                return promos;
            }
            String v = allVerts.next();
            int inDegree = gClone.inDegree(v);
            if (inDegree == 0) {
                promos.add(v);
                gClone.removeVertex(v);
                allVerts = gClone.vertices().iterator();
            }
        }
        return promos;

    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="exercicio 1 época de recurso de fevereiro de 2016">
    /**
     * Exercicio 1 época de recurso de fevereiro de 2016
     *
     * @param str
     * @param dictionary
     * @return
     */
    public DoublyLinkedList<String> checkErrors(DoublyLinkedList<String> str, Set<String> dictionary) {
        DoublyLinkedList<String> output = new DoublyLinkedList<>();
        Map<String, String> errors = new HashMap<>();

        for (String palavra : str) {
            if (dictionary.contains(palavra)) {
                output.addLast(palavra);
            } else {
                if (!errors.containsKey(palavra)) {
                    int numero = errors.size() + 1;
                    StringBuilder erro = new StringBuilder();
                    if (numero > 9) {
                        erro.append("ERRO_").append(numero);
                    } else {
                        erro.append("ERRO_0").append(numero);
                    }
                    errors.put(palavra, erro.toString());
                }
                output.addLast(errors.get(palavra));
            }
        }
        return output;
    }

    //</editor-fold>
}
