/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdjacencyMap;

import java.lang.reflect.Array;

/**
 *
 * @author brunosantos
 * @param <V>
 * @param <E>
 */
public class Edge<V, E> implements Comparable {

    /**
     * variaveis de instancia da classe
     */
    private E element;           // Edge information
    private double weight;       // Edge weight
    private Vertex<V, E> vOrig;  // vertex origin
    private Vertex<V, E> vDest;  // vertex destination

    //<editor-fold defaultstate="collapsed" desc="CONSTRUTORES">
    /**
     * construtor vazio
     */
    public Edge() {
        element = null;
        weight = 0.0;
        vOrig = null;
        vDest = null;
    }

    /**
     * construtor completo
     *
     * @param element
     * @param weight
     * @param vOrig
     * @param vDest
     */
    public Edge(E element, double weight, Vertex<V, E> vOrig, Vertex<V, E> vDest) {
        this.element = element;
        this.weight = weight;
        this.vOrig = vOrig;
        this.vDest = vDest;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="GETTER'S E SETTER'S">
    /**
     * @return the element
     */
    public E getElement() {
        return element;
    }

    /**
     * @param element the element to set
     */
    public void setElement(E element) {
        this.element = element;
    }

    /**
     * @return the weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * @return the element of the vOrig
     */
    public V getVOrig() {
        return vOrig.getElement();
    }

    /**
     * @param vOrig the vOrig to set
     */
    public void setvOrig(Vertex<V, E> vOrig) {
        this.vOrig = vOrig;
    }

    /**
     * @return the element of the vDest
     */
    public V getVDest() {
        return vDest.getElement();
    }

    /**
     * @param vDest the vDest to set
     */
    public void setvDest(Vertex<V, E> vDest) {
        this.vDest = vDest;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="MÉTODOS">
    /**
     * método que devolve um array com os elementos dos vertices de origem e
     * destino
     *
     * @return um array com o elemento de origem no índice 0 e com o elemento de
     * destino no índice 1
     */
    public V[] getEndpoints() {

        V oElem = vOrig.getElement();   // To get the element of the vertex
        V dElem = vDest.getElement();   // To get the element of the vertex

        V[] endverts = (V[]) Array.newInstance(oElem.getClass(), 2);

        endverts[0] = oElem;
        endverts[1] = dElem;

        return endverts;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="EQUALS">
    @Override
    public boolean equals(Object otherObj) {

        if (this == otherObj) {
            return true;
        }

        if (otherObj == null || this.getClass() != otherObj.getClass()) {
            return false;
        }

        Edge<V, E> otherEdge = (Edge<V, E>) otherObj;

        // if endpoints vertices are not equal
        if (!this.vOrig.equals(otherEdge.vOrig) || !this.vDest.equals(otherEdge.vDest)) {
            return false;
        }

        if (this.weight != otherEdge.weight) {
            return false;
        }

        if (this.element != null && otherEdge.element != null) {
            return this.element.equals(otherEdge.element);
        }

        return true;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="CLONE">
    @Override
    public Edge<V, E> clone() {

        Edge<V, E> newEdge = new Edge<>();

        newEdge.element = element;
        newEdge.weight = weight;
        newEdge.vOrig = vOrig;
        newEdge.vDest = vDest;

        return newEdge;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="TOSTRING">
    @Override
    public String toString() {
        String st = "";
        if (element != null) {
            st = "      (" + element + ") - ";
        } else {
            st = "\t ";
        }

        if (weight != 0) {
            st += weight + " - " + vDest.getElement() + "\n";
        } else {
            st += vDest.getElement() + "\n";
        }

        return st;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="COMPARE_TO">
    @Override
    public int compareTo(Object otherObject) {
        Edge<V, E> other = (Edge<V, E>) otherObject;
        if (this.getWeight() < other.getWeight()) {
            return -1;
        }
        if (this.getWeight() == other.getWeight()) {
            return 0;
        }
        return 1;
    }
    //</editor-fold>

}
