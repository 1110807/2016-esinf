/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdjacencyMap;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author brunosantos
 * @param <V>
 * @param <E>
 */
public class Graph<V, E> implements GraphInterface<V, E> {

    /**
     * variáveis de instância da classe
     */
    private int numVert;
    private int numEdge;
    private boolean isDirected;
    private Map<V, Vertex<V, E>> vertices;  //all Vertices of the graph

    /**
     * construtor, somente com o conhecimento se é direcionado ou não
     *
     * @param isDirected
     */
    public Graph(boolean isDirected) {
        this.numVert = 0;
        this.numEdge = 0;
        this.isDirected = isDirected;
        vertices = new LinkedHashMap<>();
    }

    //<editor-fold defaultstate="collapsed" desc="METHODS">
    /**
     * método para verificar se existe o vertex
     *
     * @param vert
     * @return true se existir false caso contrário
     */
    public boolean validVertex(V vert) {
        return vertices.get(vert) != null;
    }

    /**
     * método para devolver o índice do vértice
     *
     * @param vert
     * @return o índice do vértice
     */
    public int getKey(V vert) {

        return vertices.get(vert).getKey();
    }

    /**
     * método que devolve num array todos os elementos dos vértices
     *
     * @return array com todos os elementos dos vértices
     */
    public V[] allkeyVerts() {

        V[] keyverts = (V[]) new Object[numVert];

        for (Vertex<V, E> vert : vertices.values()) {
            keyverts[vert.getKey()] = vert.getElement();
        }

        return keyverts;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="INTERFACE_METHODS">
    /**
     *
     * @return
     */
    @Override
    public int numVertices() {
        return numVert;
    }

    /**
     *
     * @return
     */
    @Override
    public Iterable<V> vertices() {
        return vertices.keySet();
    }

    /**
     *
     * @return
     */
    @Override
    public int numEdges() {
        return numEdge;
    }

    /**
     *
     * @return
     */
    @Override
    public Iterable<Edge<V, E>> edges() {
        List<Edge<V, E>> edges = new ArrayList<>();
        for (Vertex<V, E> value : vertices.values()) {
            for (Edge<V, E> edge : value.getAllOutEdges()) {
                edges.add(edge);
            }
        }
        return edges;
    }

    /**
     *
     * @param vOrig
     * @param vDest
     * @return
     */
    @Override
    public Edge<V, E> getEdge(V vOrig, V vDest) {
        if (!validVertex(vOrig) || !validVertex(vDest)) {
            return null;
        }
        Vertex<V, E> vorig = vertices.get(vOrig);
        return vorig.getEdge(vDest);
    }

    /**
     *
     * @param edge
     * @return
     */
    @Override
    public V[] endVertices(Edge<V, E> edge) {
        if (edge == null) {
            return null;
        }

        if (!validVertex(edge.getVOrig()) || !validVertex(edge.getVDest())) {
            return null;
        }

        Vertex<V, E> vorig = vertices.get(edge.getVOrig());

        if (!edge.equals(vorig.getEdge(edge.getVDest()))) {
            return null;
        }

        return edge.getEndpoints();
    }

    /**
     *
     * @param vert
     * @param edge
     * @return
     */
    @Override
    public V opposite(V vert, Edge<V, E> edge) {
        if (!validVertex(vert)) {
            return null;
        }

        Vertex<V, E> vertex = vertices.get(vert);

        return vertex.getAdjVert(edge);
    }

    @Override
    public int outDegree(V vert) {
        if (!validVertex(vert)) {
            return -1;
        }

        Vertex<V, E> vertex = vertices.get(vert);

        return vertex.numAdjVerts();
    }

    @Override
    public int inDegree(V vert) {
        if (!validVertex(vert)) {
            return -1;
        }

        int degree = 0;
        for (V otherVert : vertices.keySet()) {
            if (getEdge(otherVert, vert) != null) {
                degree++;
            }
        }

        return degree;
    }

    /**
     *
     * @param vert
     * @return
     */
    @Override
    public Iterable<Edge<V, E>> outgoingEdges(V vert) {
        if (!validVertex(vert)) {
            return null;
        }

        Vertex<V, E> vertex = vertices.get(vert);

        return vertex.getAllOutEdges();
    }

    /**
     *
     * @param vert
     * @return
     */
    @Override
    public Iterable<Edge<V, E>> incomingEdges(V vert) {
        List<Edge<V, E>> incoming_edges = new ArrayList<>();

        if (!validVertex(vert)) {
            return null;
        }

        Vertex<V, E> vertex = vertices.get(vert);

        Iterable<Edge<V, E>> list_all_edges = edges();

        for (Edge<V, E> e : list_all_edges) {
            if (e.getVDest() == vertex.getElement()) {
                incoming_edges.add(e);
            }
        }

        return incoming_edges;
    }

    /**
     *
     * @param newVert
     * @return
     */
    @Override
    public boolean insertVertex(V newVert) {
        if (validVertex(newVert)) {
            return false;
        }

        Vertex<V, E> vertex = new Vertex<>(numVert, newVert);
        vertices.put(newVert, vertex);
        numVert++;

        return true;
    }

    /**
     *
     * @param vOrig
     * @param vDest
     * @param edge
     * @param eWeight
     * @return
     */
    @Override
    public boolean insertEdge(V vOrig, V vDest, E edge, double eWeight) {
        if (getEdge(vOrig, vDest) != null) {
            return false;
        }

        if (!validVertex(vOrig)) {
            insertVertex(vOrig);
        }

        if (!validVertex(vDest)) {
            insertVertex(vDest);
        }

        Vertex<V, E> vorig = vertices.get(vOrig);
        Vertex<V, E> vdest = vertices.get(vDest);

        Edge<V, E> newEdge = new Edge<>(edge, eWeight, vorig, vdest);
        vorig.addAdjVert(vDest, newEdge);
        numEdge++;

        //if graph is not direct insert other edge in the opposite direction
        if (!isDirected) // if vDest different vOrig
        {
            if (getEdge(vDest, vOrig) == null) {
                Edge<V, E> otherEdge = new Edge<>(edge, eWeight, vdest, vorig);
                vdest.addAdjVert(vOrig, otherEdge);
                numEdge++;
            }
        }

        return true;
    }

    /**
     *
     * @param vert
     * @return
     */
    @Override
    public boolean removeVertex(V vert) {
        if (!validVertex(vert)) {
            return false;
        }

        for (V v : this.vertices.keySet()) {
            this.removeEdge(v, vert);
            this.removeEdge(vert, v);
        }

        this.vertices.remove(vert);
        this.numVert--;

        return false;
    }

    /**
     *
     * @param vOrig
     * @param vDest
     * @return
     */
    @Override
    public boolean removeEdge(V vOrig, V vDest) {
        if (!validVertex(vOrig) || !validVertex(vDest)) {
            return false;
        }

        Edge<V, E> edge = getEdge(vOrig, vDest);

        if (edge == null) {
            return false;
        }

        Vertex<V, E> vorig = vertices.get(vOrig);

        vorig.remAdjVert(vDest);
        numEdge--;

        //if graph is not direct
        if (!isDirected) {
            edge = getEdge(vDest, vOrig);
            if (edge != null) {
                Vertex<V, E> vdest = vertices.get(vDest);
                vdest.remAdjVert(vOrig);
                numEdge--;
            }
        }
        return true;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="CLONE">
    //Returns a clone of the graph
    @Override
    public Graph<V, E> clone() {

        Graph<V, E> newObject = new Graph<>(this.isDirected);

        //insert all vertices
        for (V vert : vertices.keySet()) {
            newObject.insertVertex(vert);
        }

        //insert all edges
        for (V vert1 : vertices.keySet()) {
            for (Edge<V, E> e : this.outgoingEdges(vert1)) {
                if (e != null) {
                    V vert2 = this.opposite(vert1, e);
                    newObject.insertEdge(vert1, vert2, e.getElement(), e.getWeight());
                }
            }
        }

        return newObject;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="EQUALS">
    /* equals implementation
     * @param the other graph to test for equality
     * @return true if both objects represent the same graph
     */
    @Override
    public boolean equals(Object otherObj) {

        if (otherObj == null) {
            return false;
        }

        if (this == otherObj) {
            return true;
        }

        if (!(otherObj instanceof Graph<?, ?>)) {
            return false;
        }

        Graph<V, E> otherGraph = (Graph<V, E>) otherObj;

        if (numVert != otherGraph.numVertices() || numEdge != otherGraph.numEdges()) {
            return false;
        }

        //graph must have same vertices
        boolean eqvertex;
        for (V v1 : this.vertices()) {
            eqvertex = false;
            for (V v2 : otherGraph.vertices()) {
                if (v1.equals(v2)) {
                    eqvertex = true;
                    break;
                }
            }

            if (!eqvertex) {
                return false;
            }
        }

        //graph must have same edges
        boolean eqedge;
        for (Edge<V, E> e1 : this.edges()) {
            eqedge = false;
            for (Edge<V, E> e2 : otherGraph.edges()) {
                if (e1.equals(e2)) {
                    eqedge = true;
                    break;
                }
            }

            if (!eqedge) {
                return false;
            }
        }

        return true;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="TOSTRING">
    //string representation
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (numVert == 0) {
            sb.append("\nGraph not defined!!");
        } else {
            sb.append("Graph: ").append(numVert).append(" vertices, ").
                    append(numEdge).append(" edges\n");
            for (Vertex<V, E> vert : vertices.values()) {
                sb.append(vert).append("\n");
            }
        }
        return sb.toString();
    }

    //</editor-fold>
}
