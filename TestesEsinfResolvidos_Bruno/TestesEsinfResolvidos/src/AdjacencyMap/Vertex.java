/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdjacencyMap;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author brunosantos
 * @param <V>
 * @param <E>
 */
public class Vertex<V, E> {

    /**
     * variaveis de instancia da classe
     */
    private int key;                        //Vertex key number
    private V element;                      //Vertex information
    private Map<V, Edge<V, E>> outVerts;    //adjacent vertices

    /**
     * construtor vazio
     */
    public Vertex() {
        this.key = -1;
        this.element = null;
        this.outVerts = new LinkedHashMap<>();
    }

    /**
     * construtor com a chave e o elemento
     *
     * @param key
     * @param vInf
     */
    public Vertex(int key, V vInf) {
        this.key = key;
        this.element = vInf;
        this.outVerts = new LinkedHashMap<>();
    }

    //<editor-fold defaultstate="collapsed" desc="GETTER'S SETTER'S">
    /**
     * @return the key
     */
    public int getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(int key) {
        this.key = key;
    }

    /**
     * @return the element
     */
    public V getElement() {
        return element;
    }

    /**
     * @param element the element to set
     */
    public void setElement(V element) {
        this.element = element;
    }
    //</editor-fold>

    /**
     * adiciona vértice adjacente
     *
     * @param vAdj
     * @param edge
     */
    public void addAdjVert(V vAdj, Edge<V, E> edge) {
        outVerts.put(vAdj, edge);
    }

    /**
     * método que devolve o vértice adjacente
     *
     * @param edge
     * @return o vértice adjacente ao vértice cujo o edge seja o enviado por
     * parâmetro
     */
    public V getAdjVert(Edge<V, E> edge) {

        for (V vert : outVerts.keySet()) {
            if (edge.equals(outVerts.get(vert))) {
                return vert;
            }
        }

        return null;
    }

    /**
     * método que remove um vértice adjacente
     *
     * @param vAdj
     */
    public void remAdjVert(V vAdj) {
        outVerts.remove(vAdj);
    }

    /**
     * método que devolve o edge de um vértice adjacente
     *
     * @param vAdj
     * @return devolve o edge de um vértice adjacente
     */
    public Edge<V, E> getEdge(V vAdj) {
        return outVerts.get(vAdj);
    }

    /**
     * método que devolve o número de vértices adjacentes
     *
     * @return devolve o número de vértice adjacentes
     */
    public int numAdjVerts() {
        return outVerts.size();
    }

    /**
     * método que devolve um iterable com todos os vértices adjacentes
     *
     * @return um iterable com todos os vértices adjacentes
     */
    public Iterable<V> getAllAdjVerts() {
        return outVerts.keySet();
    }

    /**
     * método que devolve um iterable com todos os edges que saem do vértice.
     *
     * @return um iterable com todos os edges que saem do vértice
     */
    public Iterable<Edge<V, E>> getAllOutEdges() {
        return outVerts.values();
    }

    //<editor-fold defaultstate="collapsed" desc="CLONE">
    @Override
    public Vertex<V, E> clone() {

        Vertex<V, E> newVertex = new Vertex<>();

        newVertex.key = key;
        newVertex.element = element;

        for (V vert : outVerts.keySet()) {
            newVertex.outVerts.put(vert, outVerts.get(vert));
        }

        return newVertex;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="EQUALS">
    @Override
    public boolean equals(Object otherObj) {
        if (this == otherObj) {
            return true;
        }
        if (otherObj == null || this.getClass() != otherObj.getClass()) {
            return false;
        }
        Vertex<V, E> otherVertex = (Vertex<V, E>) otherObj;

        return (this.key == otherVertex.key
                && this.element != null && otherVertex.element != null
                && this.element.equals(otherVertex.element));
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="TOSTRING">
    @Override
    public String toString() {
        String st = "";
        if (element != null) {
            st = element + " (" + key + "): \n";
        }
        if (!outVerts.isEmpty()) {
            for (V vert : outVerts.keySet()) {
                st += outVerts.get(vert);
            }
        }

        return st;
    }

    //</editor-fold>
}
