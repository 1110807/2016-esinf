/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AdjacencyMatriz;

import java.util.LinkedList;

/**
 *
 * @author brunosantos
 */
public class HighwayMap implements Cloneable {

    //<editor-fold defaultstate="collapsed" desc="INNER_CLASS_HIGHWAY">
    public class Highway {

        /**
         * atributos da inner class
         */
        private String name;
        private double distance;
        private double cost;

        /**
         * construtor completo
         *
         * @param n
         * @param dist
         * @param cst
         */
        public Highway(String n, double dist, double cst) {
            name = n;
            distance = dist;
            cost = cst;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the distance
         */
        public double getDistance() {
            return distance;
        }

        /**
         * @param distance the distance to set
         */
        public void setDistance(double distance) {
            this.distance = distance;
        }

        /**
         * @return the cost
         */
        public double getCost() {
            return cost;
        }

        /**
         * @param cost the cost to set
         */
        public void setCost(double cost) {
            this.cost = cost;
        }

    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="HIGHWAYS">
    /**
     * atributo da classe
     */
    AdjacencyMatrixGraph<String, Highway> map;

    /**
     * construtor vazio
     */
    public HighwayMap() {
        map = new AdjacencyMatrixGraph<>();
    }

    /**
     * return the number of cities
     *
     * @return
     */
    public int numCities() {
        return map.numVertices();
    }

    /**
     * insert a new city in the map, fails if the city already exists
     *
     * @param city
     * @return false if the city exists in the map
     */
    public boolean insertCity(String city) {
        return map.insertVertex(city);
    }

    /**
     * Inserts a new highway in the map fails if already exists
     *
     * @param from the source city
     * @param to the destination city
     * @param name of highway
     * @param distance distance in km
     * @param cost cost in euros
     * @return false if a city does not exist or highway already exists between
     * cities
     */
    public boolean insertHighway(String from, String to, String name, double distance, double cost) {
        return map.insertEdge(from, to, new Highway(name, distance, cost));
    }

    /**
     * Returns the highways that depart from a city
     *
     * @param city
     * @return List of Highways
     */
    public Iterable<Highway> departingHighways(String city) {
        return map.outgoingEdges(city);
    }

    /**
     * Returns the direct connections of a city
     *
     * @param city
     * @return list of cities
     */
    public Iterable<String> oneHopConnections(String city) {
        return map.directConnections(city);
    }

    /**
     * Returns if two cities are connected with a path of highways
     *
     * @param from the departing city
     * @param to the destination city
     * @return List of stops (null if cities do not exist, empty if no path)
     */
    public Iterable<String> existsConnection(String from, String to) {
        if (map.checkVertex(from) && map.checkVertex(to)) {
            LinkedList<LinkedList<String>> allPaths = new LinkedList<>();
            GraphAlgorithms.allPaths(map, from, to, allPaths);

            if (!allPaths.isEmpty() && !allPaths.get(0).isEmpty()) {
                return allPaths.get(0);
            }
        }

        return null;
    }

    /**
     * Returns the path between two cities with the minimum number of highways
     *
     * @param from the departing city
     * @param to the destination city
     * @return minimum number of connecting cities, null if cities do not exist
     * or are not connected
     */
    public Iterable<String> travelViaMinimumNumberOfHighways(String from, String to) {
        if (map.checkVertex(from) && map.checkVertex(to)) {

            LinkedList<LinkedList<String>> allPaths = new LinkedList<>();
            GraphAlgorithms.allPaths(map, to, to, allPaths);

            GraphAlgorithms.allPaths(map, from, to, allPaths);

            LinkedList<String> minPaths = allPaths.get(0);

            for (int i = 1; i < allPaths.size(); i++) {
                if (allPaths.get(i).size() < minPaths.size()) {
                    minPaths = allPaths.get(i);
                }
            }

            if (!minPaths.isEmpty()) {
                return minPaths;
            }
        }

        return null;
    }

    /**
     * Returns the path between two cities with the minimum distance
     *
     * @param from the departing city
     * @param to the destination city
     * @param path list of connecting cities
     * @return distance (-1 case of wrong cities)
     */
    public double minimumDistance(String from, String to, LinkedList<String> path) {
        if (map.checkVertex(from) && map.checkVertex(to)) {
            AdjacencyMatrixGraph<String, Double> newGraph = new AdjacencyMatrixGraph<>();

            for (String v : map.vertices()) {
                newGraph.insertVertex(v);
            }

            for (String h1 : map.vertices()) {
                for (String h2 : map.vertices()) {
                    if (map.getEdge(h1, h2) != null) {
                        newGraph.insertEdge(h1, h2, map.getEdge(h1, h2).distance);
                    }
                }
            }

            return EdgeAsDoubleGraphAlgorithms.shortestPath(newGraph, from, to, path);
        }

        return -1;
    }

    /**
     * Returns the path between two cities with the minimum cost
     *
     * @param from the departing city
     * @param to the destination city
     * @param path list of connecting cities
     * @return cost (-1 case of wrong cities)
     */
    public double minimumCost(String from, String to, LinkedList<String> path) {
        if (map.checkVertex(from) && map.checkVertex(to)) {
            AdjacencyMatrixGraph<String, Double> newGraph = new AdjacencyMatrixGraph<>();

            for (String v : map.vertices()) {
                newGraph.insertVertex(v);
            }

            for (String h1 : map.vertices()) {
                for (String h2 : map.vertices()) {
                    if (map.getEdge(h1, h2) != null) {
                        newGraph.insertEdge(h1, h2, map.getEdge(h1, h2).cost);
                    }
                }
            }

            return EdgeAsDoubleGraphAlgorithms.shortestPath(newGraph, from, to, path);
        }

        return -1;
    }

    /**
     * Returns a clone of the graph (a shallow copy).
     *
     * @return the new cloned map
     */
    @SuppressWarnings("unchecked")
    public HighwayMap clone() {
        HighwayMap newMap = new HighwayMap();
        newMap.map = (AdjacencyMatrixGraph<String, Highway>) map.clone();
        return newMap;
    }

    /**
     * Implementation of equals
     *
     * @return true if both objects represent the same map
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof HighwayMap)) {
            return false;
        }

        return map.equals(((HighwayMap) obj).map);
    }

    //</editor-fold>
}
