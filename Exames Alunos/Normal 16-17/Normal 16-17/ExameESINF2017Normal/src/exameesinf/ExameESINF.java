/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exameesinf;

import exameesinf.BST.Node;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author KAMMIKAZI
 */
public class ExameESINF {

    /**
     * @param args the command line arguments
     */
    
    public static  ArrayList<Integer> Heap = new ArrayList<>();
    public static void main(String[] args) {
        System.out.println("----------------------------------------------------EX01-----------------------------------------------------------------------------");
        LinkedList<Integer> list = new LinkedList<>();
        list.add(2);
        list.add(9);
        list.add(7);
        list.add(5);
        list.add(10);
        list.add(15);
        list.add(6);
        list.add(12);
        list.add(3);
        ArrayList<Integer> centers = new ArrayList<>();
        centers.add(10);
        centers.add(3);
        centers.add(6);
        Map<Integer, LinkedList<Integer>> kSubLists = new HashMap<>();
        kSubLists = KSubLists(list, centers);
        for (Integer i : kSubLists.keySet()) {
            System.out.println("");
            System.out.println("Centro :" + i);

            for (Integer e : kSubLists.get(i)) {
                System.out.print("Numero : " + e + " ");

            }
            System.out.println("");
        }
        System.out.println("----------------------------------------------------EX03-----------------------------------------------------------------------------");
        BST tree = new BST();
        tree.insert(65);
        tree.insert(54);
        tree.insert(85);
        tree.insert(19);
        tree.insert(60);
        tree.insert(80);
        tree.insert(89);
        tree.insert(8);
        tree.insert(35);
        tree.insert(70);
        tree.insert(83);

        String s = "101";
        
        Object o = getElementByString(tree, s);
        System.out.println("O elemento retornado por este metodo é " + o);
        
        System.out.println("----------------------------------------------------EX04-----------------------------------------------------------------------------");
        
        AdjacencyMatrixGraph<String,Integer> g = new AdjacencyMatrixGraph();
        String maria = "Maria";
        String joao = "João";
        String manuel = "Manuel";
        String rita  = "Rita";
        String joana = "Joana";
        String raul = "Raul";
        String antonio = "António";
        String tiago = "Tiago";
        String miguel = "Miguel";
        g.insertVertex(maria);
        g.insertVertex(joao);
        g.insertVertex(manuel);
        g.insertVertex(rita);
        g.insertVertex(joana);
        g.insertVertex(raul);
        g.insertVertex(antonio);
        g.insertVertex(tiago);
        g.insertVertex(miguel);
        g.insertEdge(maria, manuel, 1);
        g.insertEdge(maria, rita, 2);
        g.insertEdge(joao, rita, 3);
        g.insertEdge(rita, joana, 4);
        g.insertEdge(rita, raul, 5);
        g.insertEdge(rita, antonio, 6);
        g.insertEdge(manuel, miguel, 7);
        g.insertEdge(joana, miguel, 8);
        g.insertEdge(raul, miguel, 9);
        g.insertEdge(antonio, tiago, 10);
        
        System.out.println("");
        System.out.println("Exemplo com matriz de adjacencias e 3 promocoes");
        List<String> promos = new ArrayList<>();
        Integer n = 3;
        promos = calcularPromocoes(g, n);
        for(String s1 : promos){
            System.out.println(s1);
        }
        System.out.println("");
        
         System.out.println("----------------------------------------------------EX04/Map de Adjacencias/-----------------------------------------------------------------------------");
        
        Graph <String,Integer> gMap = new Graph(true);
        String maria1 = "Maria";
        String joao1 = "João";
        String manuel1 = "Manuel";
        String rita1  = "Rita";
        String joana1 = "Joana";
        String raul1 = "Raul";
        String antonio1 = "António";
        String tiago1 = "Tiago";
        String miguel1 = "Miguel";
        
        gMap.insertVertex(maria1);
        gMap.insertVertex(joao1);
        gMap.insertVertex(manuel1);
        gMap.insertVertex(rita1);
        gMap.insertVertex(joana1);
        gMap.insertVertex(raul1);
        gMap.insertVertex(antonio1);
        gMap.insertVertex(tiago1);
        gMap.insertVertex(miguel1);
        
        gMap.insertEdge(maria1, manuel1, 1 , 1);
        gMap.insertEdge(maria1, rita1, 2, 1);
        gMap.insertEdge(joao1, rita1, 3, 1);
        gMap.insertEdge(rita1, joana1, 4, 1);
        gMap.insertEdge(rita1, raul1, 5, 1);
        gMap.insertEdge(rita1, antonio1, 6, 1);
        gMap.insertEdge(manuel1, miguel1, 7, 1);
        gMap.insertEdge(joana1, miguel1, 8, 1);
        gMap.insertEdge(raul1, miguel1, 9, 1);
        gMap.insertEdge(antonio1, tiago1, 10, 1);
        System.out.println("");
        System.out.println("Exemplo com map de adjacencias e 2 promocoes");
        
        List<String> promos1 = new ArrayList<>();
        Integer n1 = 2;
        promos1 = calcularPromocoesMap(gMap, n1);
        for(String s1 : promos1){
            System.out.println(s1);
        }
        System.out.println("");
        
        System.out.println("----------------------------------------------------EX04/+Eficiente/-----------------------------------------------------------------------------");
        System.out.println("");
        System.out.println("Exemplo com 5 promocoes");
        System.out.println("");
        
        List<String> promos2 = new ArrayList<>();
        Integer n2 = 5;
        promos2 = calcularPromocoesMaisEficiente(gMap, n2);
        for(String s1 : promos2){
            System.out.println(s1);
        }
        System.out.println("");
        System.out.println("----------------------------------------------------EX05-----------------------------------------------------------------------------");
        
       
        
        Heap.add(10);
        Heap.add(9);
        Heap.add(5);
        Heap.add(8);
        Heap.add(7);
        Heap.add(4);
        Heap.add(3);
        Heap.add(1);
        Heap.add(2);
        Heap.add(6);
        int idx = 1;
        ArrayList<Integer> subHeap = new ArrayList<>();
        int cont = recursiveMethod(idx, subHeap);
        System.out.println("Tem " + cont + " elementos na subHeap");
        for(Integer i : subHeap){
            System.out.println(i);
        }
    }
    
    public static int recursiveMethod(int idx, ArrayList<Integer> vet) {
        int leftNode = idx*2+1;
        int rightNode = idx*2+2;
        
        if (!vet.contains(Heap.get(idx))) {
            vet.add(Heap.get(idx));
        }

            if(leftNode <Heap.size()){
            vet.add(Heap.get(leftNode));
             recursiveMethod(leftNode , vet);
            }
            if(rightNode <Heap.size()){
            vet.add(Heap.get(rightNode));
             recursiveMethod(rightNode, vet);
            }
        
       return vet.size();
    }

    public static Map<Integer, LinkedList<Integer>> KSubLists(LinkedList<Integer> list, ArrayList<Integer> centers) {
        Map<Integer, LinkedList<Integer>> kSubLists = new HashMap<>();

        Collections.sort(centers);                          // para ter a certeza que os centros vem por ordem crescente, caso esse nao seja o caso o algoritmo nao funciona direito

        for (int i = 0; i < centers.size(); i++) {

            int center = centers.get(i);

            LinkedList<Integer> L = new LinkedList<>();
            
            for (int j = 0; j < list.size(); j++) {

                int number = list.get(j);

                if (i == centers.size() - 1) {

                    L.add(number);
                } else if (Math.abs(number - center) < Math.abs(number - centers.get(i + 1))) {

                    list.remove(j);
                    j--;
                    L.add(number);

                }

            }
            kSubLists.put(center, L);

        }
        return kSubLists;
    }

    public static Object getElementByString(BST tree, String s) {
        Node n = tree.root();
        for (int i = 0; i < s.length(); i++) {
            String c = s.substring(i, i + 1);
            if (Integer.parseInt(c) == 0) {
                if (n.getLeft() == null) {
                    System.out.println("Node invalido");
                    return null;
                }
                n = n.getLeft();
            } else if (Integer.parseInt(c) == 1) {
                if (n.getRight() == null) {
                    System.out.println("Node invalido");
                    return null;
                }
                n = n.getRight();
            }
        }
        return n.getElement();
    }
    
    public static List<String> calcularPromocoes(AdjacencyMatrixGraph<String, Integer> g, Integer n) {
        Iterator<String> allVerts;
        ArrayList<String> promos = new ArrayList<>();
        ArrayList<String> aux = new ArrayList<>();
        Iterator<Integer> inEdges;
        Object[] endVertices = new Object[2];
        allVerts = g.vertices().iterator();
        while (allVerts.hasNext()) {
            String v = allVerts.next();

            promos.add(v);

            inEdges = g.incomingEdges(v).iterator();
            while (inEdges.hasNext()) {
                int e = inEdges.next();
                endVertices = g.endVertices(e);

                if (!promos.contains((String) endVertices[0])) {
                    promos.add((String) endVertices[0]);
                    aux.add((String) endVertices[0]);
                }
                if (!inEdges.hasNext() && aux.size()!=0) {
                    Object v2 = aux.get(0);
                    aux.remove(0);
                    inEdges = g.incomingEdges((String)v2).iterator();
                }
            }

                if (promos.size() == n) {

                    return promos;

                } else {

                    promos.clear();

                    aux.clear();
                }

            }
        
        return promos;
    }
    
    public static List<String> calcularPromocoesMap(Graph<String, Integer> g, Integer n) {
        Iterator<String> allVerts;
        ArrayList<String> promos = new ArrayList<>();
        ArrayList<String> aux = new ArrayList<>();
        Iterator<Edge<String,Integer>> inEdges;                                         //Muda este iterador
        String [] endVertices = new String[2];
        allVerts = g.vertices().iterator();
        while (allVerts.hasNext()) {
            String v = allVerts.next();

            promos.add(v);

            inEdges = g.incomingEdges(v).iterator();
            while (inEdges.hasNext()) {
                Edge<String,Integer> e = inEdges.next();                               // E consequentemente esta linha
                endVertices = g.endVertices(e);

                if (!promos.contains(endVertices[0])) {
                    promos.add((String) endVertices[0]);
                    aux.add((String) endVertices[0]);
                }
                if (!inEdges.hasNext() && aux.size()!=0) {
                    Object v2 = aux.get(0);
                    aux.remove(0);
                    inEdges = g.incomingEdges((String)v2).iterator();
                }
            }

                if (promos.size() == n) {

                    return promos;

                } else {

                    promos.clear();

                    aux.clear();
                }

            }
        
        return promos;
    }
    
    public static List<String> calcularPromocoesMaisEficiente(Graph <String, Integer> g, Integer n) {
        Graph<String, Integer> gClone = g.clone();
        ArrayList<String> promos = new ArrayList<>();
        aux(gClone, promos,n);
        return promos;

    }
    
    public static ArrayList<String> aux(Graph<String, Integer> gClone, ArrayList<String> promos, Integer n) {
        Iterator<String> allVerts = gClone.vertices().iterator();

        while (allVerts.hasNext()) {
            if(n == promos.size()){ 
                return promos;
            }
            String v = allVerts.next();
            int inDegree = gClone.inDegree(v);
            if (inDegree == 0) {
                promos.add(v);
                gClone.removeVertex(v);
                allVerts = gClone.vertices().iterator();
            
            }
        }
        return promos;

    }
    
       
}
