package exercicio_1_epocafevereiro;

import java.util.*;

public class Exercicio_1_EpocaFevereiro {

    
    public static DoublyLinkedList <String> checkErrors(DoublyLinkedList<String> str, Set<String> dictionary) {
        
        DoublyLinkedList <String> resultado = new DoublyLinkedList<>();
        HashMap <String, String> erros = new HashMap <>();
        
        Iterator <String> iterador = str.iterator();
        
        int i = 1;
        for (String c : str) {
            if (dictionary.contains(c)) {
                resultado.addLast(c);
            }
            else {
                if (!erros.containsKey(c)) {
                    erros.put(c, "ERRO_" + i);
                    i++;
                }
                resultado.addLast(erros.get(c));
                
            }
        }
        
        return resultado;
        
    }
    
    public static void main(String[] args) {
        
        DoublyLinkedList <String> str = new DoublyLinkedList<>();
        Set <String> dictionary = new HashSet<>();
        DoublyLinkedList <String> resultado = new DoublyLinkedList<>();
        
        
        str.addLast("os");
        str.addLast("campos");
        str.addLast("sao");
        str.addLast("berdes");
        str.addLast("os");
        str.addLast("zolhos");
        str.addLast("dela");
        str.addLast("sao");
        str.addLast("berdes");
        str.addLast("e");
        str.addLast("os");
        str.addLast("meus");
        str.addLast("zolhos");
        str.addLast("tamem");
        
        dictionary.add("os");
        dictionary.add("campos");
        dictionary.add("sao");
        dictionary.add("dela");
        dictionary.add("e");
        dictionary.add("meus");
        
        resultado = checkErrors (str, dictionary);
        
        resultado.forEach(a -> System.out.println(a));
    }
    
}
