package exercicio_2_epocafevereiro;

import java.util.*;

public class Exercicio_2_EpocaFevereiro {

    public static Integer checkSequence(Graph<Integer,Character> trie, Character[] sequence)	 {
        
        
        Iterable <Integer> vertices = trie.vertices();
                
        Integer noPai = null;
        
        for(Integer v : vertices) {
            if (v == 0) {
                noPai = v;
            }
        }
        
        for (Character s : sequence) {
            Iterable <Edge<Integer,Character>> caminhos = trie.outgoingEdges(noPai);
            for (Edge<Integer,Character> c : caminhos) {
                if (c.getElement().equals(s)) {
                    noPai = c.getVDest();
                }
            }
        }
        return noPai;
    }
    
    public static void main(String[] args) {
        
        Graph <Integer,Character> trie = new Graph(true);
        
        Character sequence []= {'i','n','n'};
        
        Integer resultado;
        
        trie.insertVertex(0);
        trie.insertVertex(101);
        trie.insertVertex(15);
        trie.insertVertex(11);
        trie.insertVertex(7);
        trie.insertVertex(102);
        trie.insertVertex(5);
        trie.insertVertex(3);
        trie.insertVertex(4);
        trie.insertVertex(12);
        trie.insertVertex(9);
        
        trie.insertEdge(0, 101, 't', 1);
        trie.insertEdge(0, 15, 'A', 1);
        trie.insertEdge(0, 11, 'i', 1);
        trie.insertEdge(101, 7, 'o', 1);
        trie.insertEdge(101, 102, 'e', 1);
        trie.insertEdge(11, 5, 'n', 1);
        trie.insertEdge(102, 3, 'a', 1);
        trie.insertEdge(102, 4, 'd', 1);
        trie.insertEdge(102, 12, 'n', 1);
        trie.insertEdge(5, 9, 'n', 1);
        
        System.out.println(trie);
        
        resultado = checkSequence(trie, sequence);
        
        System.out.println(resultado);
        
    }
    
}
