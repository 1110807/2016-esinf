package exercicio_1_epocanormal;

import java.util.*;

public class Exercicio_1_EpocaNormal {

    public static DoublyLinkedList <Integer> calcMMS(DoublyLinkedList <Integer> serie, Integer periodo) {
        
        DoublyLinkedList <Integer> resultado2 = new DoublyLinkedList<>();
        LinkedList <Integer> num = new LinkedList <>();
        
        int soma = 0;
        int contador = 0;
        
        serie.forEach(a-> num.add(a));
        
        Iterator <Integer> iterador = num.iterator();
        
        while (iterador.hasNext()) {
            soma += num.get(contador);
            if (contador < periodo-1) {
                resultado2.addLast(0);
                //System.out.println(soma);
            }
            else {
                resultado2.addLast(soma/periodo);
                soma-= num.get(contador -periodo +1);
            }
            contador++;
            iterador.next();
        }
        
        return resultado2;
    }	
    
    public static void main(String[] args) {
        
        DoublyLinkedList <Integer> serie = new DoublyLinkedList<>();
        LinkedList <Integer> num = new LinkedList <>();
        DoublyLinkedList <Integer> resultado = new DoublyLinkedList<>();
        
        int periodo = 3;
        int soma = 0;
        int contador = 0;
        
        serie.addLast(2);
        serie.addLast(4);
        serie.addLast(3);
        serie.addLast(7);
        serie.addLast(8);
        serie.addLast(10);
        
        resultado = calcMMS (serie, periodo);
        
        resultado.forEach(s -> System.out.println(s));
        
    }
    
}
