package Recurso_2016;

import ClassesEsinf.DoublyLinkedList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author andre
 */
public class ex01 {
    Set<String> dictionary;
    public static void main(String[] args) {
        Set<String> dictionary = new HashSet<>();
        dictionary.add("os");
        dictionary.add("campos");
        dictionary.add("são");
        dictionary.add("verdes");
        dictionary.add("olhos");
        dictionary.add("dela");
        dictionary.add("e");
        dictionary.add("meus");
        dictionary.add("também");
        
        DoublyLinkedList<String> lista = new DoublyLinkedList<>();
        lista.addLast("os");
        lista.addLast("campos");
        lista.addLast("são");
        lista.addLast("berdes");
        lista.addLast("os");
        lista.addLast("zolhos");
        lista.addLast("dela");
        lista.addLast("são");
        lista.addLast("berdes");
        lista.addLast("e");
        lista.addLast("os");
        lista.addLast("meus");
        lista.addLast("zolhos");
        lista.addLast("tamem");

        System.out.println("*****Original**********");
        for (Iterator iterator = lista.iterator(); iterator.hasNext();) {
            String next = (String) iterator.next();
            System.out.println(next);
        }
        System.out.println("*****************************");

        System.out.println("******Versao Aluno ************");
        DoublyLinkedList result = new DoublyLinkedList();
        result = checkErrors(lista, dictionary);
        for (Iterator iterator = result.iterator(); iterator.hasNext();) {
            String next = (String) iterator.next();
            System.out.println(next);
        }
        System.out.println("******************************");
    }

    public static DoublyLinkedList<String> checkErrors(DoublyLinkedList<String> str, Set<String> dictionary) {
        DoublyLinkedList<String> result = new DoublyLinkedList<>(); //Lista resultado
        Map<String, String> aux = new HashMap(); //Map auxiliar para guardar erros
        Iterator<String> it = str.iterator(); //iterar a str
        String element;
        int count = 1;
        while (it.hasNext()) {
            element = it.next();
            if (dictionary.contains(element)) {
                result.addLast(element); //nao existe erro logo guarda resultado
            } else {
                if (!aux.containsKey(element)) {
                    aux.put(element, "ERRO_0" + count); //erro novo guarda no map
                    count++;
                }
                result.addLast(aux.get(element)); //guarda o value no resultado
            }
        }
        return result;
    }
}
