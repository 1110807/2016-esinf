package Normal_2016;

import ClassesEsinf.DoublyLinkedList;
import java.util.Iterator;

/**
 *
 * @author andre
 */
public class ex01 {

    public static void main(String[] args) {
        DoublyLinkedList<Integer> lista = new DoublyLinkedList<>();
        lista.addLast(2);
        lista.addLast(4);
        lista.addLast(3);
        lista.addLast(7);
        lista.addLast(8);
        lista.addLast(10);
        int period = 3;
        
        System.out.println("*****Original**********");
        for (Iterator iterator = lista.iterator(); iterator.hasNext();) {
            Integer next = (Integer) iterator.next();
            System.out.println(next);
        }
        System.out.println("*****************************");
        
        System.out.println("******Versao Aluno ************");
        DoublyLinkedList result = new DoublyLinkedList();
        result = calcMMS_v1(lista, period);
        for (Iterator iterator = result.iterator(); iterator.hasNext();) {
            Integer next = (Integer) iterator.next();
            System.out.println(next);
        }
        System.out.println("******************************");
        System.out.println("******Versao Professora**************");
        
        result = calcMMS_vProf(lista, period);
        for (Iterator iterator = result.iterator(); iterator.hasNext();) {
            Integer next = (Integer) iterator.next();
            System.out.println(next);
        }
        System.out.println("*******************************");
    }

    public static DoublyLinkedList<Integer> calcMMS_v1(DoublyLinkedList<Integer> serie, Integer period) {

        DoublyLinkedList<Integer> result = new DoublyLinkedList<>();
        int count = 0, soma = 0;
        Iterator<Integer> it = serie.iterator();
        Iterator<Integer> it2 = serie.iterator();
        Integer element;
        while (it.hasNext()) {
            element = it.next();
            if (count >= period - 1) {
                soma += element;
                result.addLast(soma / period);
                soma -= it2.next();
                count++;
            } else {
                soma += element;
                result.addLast(0);
                count++;
            }
        }
        return result;
    }

    public static DoublyLinkedList<Integer> calcMMS_vProf(DoublyLinkedList<Integer> serie, Integer period) {

        DoublyLinkedList<Integer> result = new DoublyLinkedList<Integer>();
        int count = 0, soma = 0, k = 0;
        Iterator<Integer> itt1 = serie.iterator();
        Iterator<Integer> itt2 = serie.iterator();

        while (itt1.hasNext()) {
            count = k - period + 1;
            soma += itt1.next();
            if (count < 0) {
                result.addLast(0);
            } else if (count == 0) {
                result.addLast(soma / period);
            } else if (count > 0) {
                soma -= itt2.next();
                result.addLast(soma / period);
            }
            k = k + 1;
        }
        return result;
    }
}
