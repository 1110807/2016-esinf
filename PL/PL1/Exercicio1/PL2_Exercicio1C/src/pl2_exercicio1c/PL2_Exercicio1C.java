package pl2_exercicio1c;

public class PL2_Exercicio1C {

    public Set<String> getNumbers(String name) {
        if (!dir.containsValue(name))
            return null;
        Set<String> result = new HashSet<>();
        
        for (String keynum : dir.keySet()){
            String Name = dir.get(keynum);
            if (Name.equals(name))
                result.add(keynum);
        }
        return result;
    }
    public static void main(String[] args) {
        
    }
    
}
