package pl2_exercicio1;

public class PL2_Exercicio1 {

    public class PhoneDir{
        Map <String, String> dir;
        
        public PhoneDir(){
            dir = new HashMap<>();
        }
        
        public boolean insert (String number, String name) {
            if (number.equals == "" || name.equals == "")
                return false;
            if (number.length() != 9)
                return false;
            if (!number.matches("[0-9]+"))
                return false;
            if (!(number.charAt(0) == 9 || number.charAt(0) == 2))
                return false;
            dir.put(number, name);
        }
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
