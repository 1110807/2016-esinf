package exercicio_1;

import java.util.*;

public class Exercicio_1 {

    public static void main(String[] args) {
        
        DoublyLinkedList <String> str = new DoublyLinkedList<>();
        DoublyLinkedList <String> resultado = new DoublyLinkedList<>();
        List <String> temp = new ArrayList<>();
        Map <String,String> erros = new HashMap<>();
        Set <String> dictionary = new HashSet<>();
        
        str.addLast("os");
        str.addLast("campos");
        str.addLast("sao");
        str.addLast("berdes");
        str.addLast("os");
        str.addLast("zolhos");
        str.addLast("dela");
        str.addLast("sao");
        str.addLast("berdes");
        str.addLast("e");
        str.addLast("os");
        str.addLast("meus");
        str.addLast("zolhos");
        str.addLast("tamem");
        
        dictionary.add("os");
        dictionary.add("campos");
        dictionary.add("sao");
        dictionary.add("verdes");
        dictionary.add("os");
        dictionary.add("olhos");
        dictionary.add("dela");
        dictionary.add("e");
        dictionary.add("meus");
        dictionary.add("tambem");
        
        str.forEach(a-> temp.add(a));
        
        ListIterator <String> iteradortemp = temp.listIterator();
        Iterator <String> iteradordic = dictionary.iterator();
        
        int i = 1;
        
        for (String palavra : str) {
            if (dictionary.contains(palavra)) {
                resultado.addLast(palavra);
            }
            else {
                if (!erros.containsKey(palavra)){
                    erros.put(palavra, "Erro_0" + i);
                    i++;
                    
                }
                else {
                }
                resultado.addLast(erros.get(palavra));
            }
        }
        /*
        for (int i = 0; i < str.size(); i++) {
            for (int j = 0; j < dictionary.size(); j++) {
                if(str.equals(dictionary))
            }
        }
        
        while(iteradortemp.hasNext()) {
            for (int i = 0; i < dictionary.size(); i++) {
                if(dictionary.contains(temp.get(j))
                    resultado.addLast(temp.get(i));
                }
            }
        }
*/
        ListIterator <String> iteradorresultado = resultado.listIterator();
        
        while (iteradorresultado.hasNext()) {
            System.out.println(iteradorresultado.next());
        }
    }
    
}
