package exercicio2;

import static java.lang.Integer.MAX_VALUE;
import java.util.*;

public class Exercicio2 {

    public static void main(String[] args) {
        
        Graph <String, Integer> grafo = new Graph(true);
        ArrayList <LinkedList<String>> lista = new ArrayList<>();
        LinkedList<String> resultado = new LinkedList<>();
        
        String a = "A";
        String b = "B";
        String c = "C";
        String d = "D";
        String e = "E";
        String f = "F";
        String g = "G";
        String h = "H";
        
        grafo.insertVertex(a);
        grafo.insertVertex(b);
        grafo.insertVertex(c);
        grafo.insertVertex(d);
        grafo.insertVertex(e);
        grafo.insertVertex(f);
        grafo.insertVertex(g);
        grafo.insertVertex(h);
        
        grafo.insertEdge(a, b, 1, 1);
        grafo.insertEdge(b, d, 1, 1);
        grafo.insertEdge(d, e, 1, 1);
        grafo.insertEdge(e, g, 1, 1);
        grafo.insertEdge(b, c, 1, 1);
        grafo.insertEdge(c, f, 1, 1);
        grafo.insertEdge(e, h, 1, 1);
        grafo.insertEdge(h, g, 1, 1);
        grafo.insertEdge(e, h, 1, 1);
        
        String origem = "A";
        String destino = "G";
        
        Integer menor = (Integer)MAX_VALUE;
        
        //Iterable <String> vertices = grafo.vertices();
        
        lista = GraphAlgorithms.allPaths(grafo, origem, destino);
        
        System.out.println(lista);
        
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).size() < menor) {
                resultado = lista.get(i);
                menor = lista.get(i).size();
            }
        }
        
        System.out.println(resultado);
        
    }
    
}
