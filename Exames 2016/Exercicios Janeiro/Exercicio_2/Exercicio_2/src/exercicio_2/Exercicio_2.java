package exercicio_2;

import java.util.*;

public class Exercicio_2 {

    //Calcula qual a ligação que tem mais ligações no grafo
    
    public static int calculaTamanho(Graph <String, Double> g, String vo, LinkedList lista) {
        
        HashMap <String, Double> sizeBigger = new HashMap<>();
        
        double maior = -1d;
        double size = 0;
        
        for (String vd : g.vertices()) {
            
            size = GraphAlgorithms.shortestPath(g, vo, vd, lista);
            
            if (size > maior) {
                maior = size;
                sizeBigger.put(vo, size);
            }
        }
        
        System.out.println(sizeBigger);
        
        
        
        return 0;
    }
    
    
    public static void main(String[] args) {
        
        Graph <String, Double> grafico = new Graph(false);
        LinkedList <String> lista = new LinkedList<>();
        
        
        grafico.insertVertex("A");
        grafico.insertVertex("B");
        grafico.insertVertex("C");
        grafico.insertVertex("D");
        grafico.insertVertex("E");
        
        grafico.insertEdge("A", "B", 1.0, 1);
        grafico.insertEdge("A", "C", 1.0, 1);
        grafico.insertEdge("A", "D", 1.0, 1);
        grafico.insertEdge("A", "E", 1.0, 1);
        grafico.insertEdge("B", "C", 1.0, 1);
        grafico.insertEdge("C", "D", 1.0, 1);
        grafico.insertEdge("D", "E", 1.0, 1);
        
        double tamanhoFinal = 0d;
        
        for (String verticeO : grafico.vertices()) {
            tamanhoFinal = calculaTamanho (grafico, verticeO, lista);
        }
        /*
        for (Integer c : sizeBigger.keySet()) {
            System.out.println(c);
        }
        */
    }
    
}
