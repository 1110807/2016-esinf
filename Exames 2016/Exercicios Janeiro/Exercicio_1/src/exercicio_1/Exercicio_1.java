package exercicio_1;

import java.util.*;

public class Exercicio_1 {
/*
    DoublyLinkedList<Integer> calcMMS(DoublyLinkedList <Integer> serie, Integer period) {
        
        DoublyLinkedList <Integer> resultado = new DoublyLinkedList<>();
        
        return resultado;
    }
    */
    public static void main(String[] args) {
        
        DoublyLinkedList <Integer> serie = new DoublyLinkedList <>();
        DoublyLinkedList <Integer> resultado = new DoublyLinkedList<>();
        LinkedList <Integer> temporaria = new LinkedList<>();
        
        int periodo = 3;
        int soma = 0;
        
        serie.addLast(2);
        serie.addLast(4);
        serie.addLast(3);
        serie.addLast(7);
        serie.addLast(8);
        serie.addLast(10);
        
        serie.forEach(a -> temporaria.add(a));
        
        ListIterator <Integer> iterador1 = serie.listIterator();
        ListIterator <Integer> iterador2 = temporaria.listIterator();
        
        
        int i = 0;
        while (iterador2.hasNext()) {
            soma += temporaria.get(i);
            
            if (i < periodo - 1) {
                resultado.addLast(0);
            }
            else {
                resultado.addLast(soma / periodo);
                soma -= temporaria.get(i - periodo +1);
            }
            i++;
            iterador2.next();
            
        }
        
        
        
        resultado.forEach(s-> System.out.println(s));
       
    }
    
}
