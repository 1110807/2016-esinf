package exercicio_1;

import static java.lang.Integer.MAX_VALUE;
import java.util.*;

public class Exercicio_1 {

   public static DoublyLinkedList <Integer> Kneighbors (DoublyLinkedList <Integer> list, Integer elem, Integer k) {
       
       DoublyLinkedList < Integer> resultado = new DoublyLinkedList<>();
       LinkedList < Integer> clone = new LinkedList<>();
       
       ListIterator <Integer> iterador = list.listIterator();
       
       list.forEach(a -> clone.addLast(a));
              
       int calc;
       int x = 0;
                     
       while (resultado.size() < k) {
           
           int pos=0;
           int i = 0;
           Integer menor = MAX_VALUE;
           for (Integer c: clone) {
               
               calc = Math.abs(elem - c);
               
               if (calc < menor && (int) elem != (int) c) {
                   menor = calc;
                   pos = c;
                   x = i;
                   
               }
               i+=1;
               
           }
           
           resultado.addLast(pos);
           clone.remove(x);
           
           System.out.println(clone);
           
       }
       
       return resultado;
   }
    
    
    public static void main(String[] args) {
        
        DoublyLinkedList <Integer> list = new DoublyLinkedList<>();
        DoublyLinkedList <Integer> resultadoF = new DoublyLinkedList<>();
        
        Integer elem = 5;
        Integer k = 3;
        
        list.addLast(2);
        list.addLast(9);
        list.addLast(7);
        list.addLast(5);
        list.addLast(10);
        list.addLast(15);
        list.addLast(6);
        list.addLast(12);
        list.addLast(3);
        
        resultadoF = Kneighbors (list, elem, k);
        
        resultadoF.forEach(a -> System.out.println(a));
        
    }
    
}