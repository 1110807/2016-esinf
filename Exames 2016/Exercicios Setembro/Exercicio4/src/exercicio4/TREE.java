package tree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

/**
 *
 * @author DEI-ESINF
 * @param <E>
 */
/*
public interface:

public BST()
public boolean isEmpty()
public int size()
public void insert(E element)
public void remove(E element)
public String toString()

public int height()
public E smallestElement()
public Iterable<E> inOrder()
public Iterable<E> preOrder()
public Iterable<E> postOrder()
public Map<Integer,List<E>> nodesByLevel()
++++++++++++++++++++++++++++++
public int depth(E element)
public E parent(E element)
public boolean contains(E element)
public boolean isLeaf(E element)
public Iterator<E> iterator()

public List<E> ascdes()


 */
public class TREE<E extends Comparable<E>> extends BST<E> {
    //---------------------------------------------------------------

    /**
     * Returns the number of levels separating E element from the root.
     *
     * @param element A valid element within the tree
     * @return
     * @throws IllegalArgumentException if element is not a valid E for this
     * tree.
     */
    public int depth(E element) {
        return (depth(element, root));
    }

    private int depth(E element, Node<E> node) {

        if (node != null) {
            if (node.getElement().compareTo(element) == 0) {
                return 0;
            } else if (node.getElement().compareTo(element) < 0) {
                return depth(element, node.getRight()) + 1;
            } else if (node.getElement().compareTo(element) > 0) {
                return depth(element, node.getLeft()) + 1;
            }
        }

        throw new IllegalArgumentException("Element does not exist in the tree.");
    }
    //---------------------------------------------------------------

    public boolean contains(E element) {
        return find(element, root) != null;
    }

    //---------------------------------------------------------------
    public boolean isLeaf(E element) {
        Node<E> node = find(element, root);

        return node != null && node.getLeft() == null && node.getRight() == null;
    }

//---------------------------------------------------------------
    /**
     * Returns the parent's Element of an Element (or null if Element is the
     * root or not belongs to the tree).
     *
     * @param element A valid element within the tree
     * @return Element of element's parent (or null if element is root)
     */
    public E parent(E element) {
        if (root.getElement().compareTo(element) == 0) {
            return null;
        }

        Node<E> node = node = this.find(element, root);
        return parent(node, root);

    }

    private E parent(Node<E> node, Node<E> parentNode) {
        E element = node.getElement();
        if (parentNode != null) {
            if (parentNode.getLeft().getElement().compareTo(element) == 0
                    || parentNode.getRight().getElement().compareTo(element) == 0) {
                return parentNode.getElement();
            } else if (parentNode.getElement().compareTo(element) > 0) {
                return parent(node, parentNode.getLeft());
            } else {
                return parent(node, parentNode.getRight());
            }
        }

        return null;
    }

//****************************************************************
    public Iterator<E> iterator() {
        return new BSTIterator();
    }
//#########################################################################
//#########################################################################
//---------------------------------------------------------------

    /**
     * Returns the tree without leaves.
     *
     * @return tree without leaves
     */
    public BST<E> autumnTree() {
        BST<E> tree = new TREE();

        tree.root = copyRec(root);

        return tree;
    }

    private Node<E> copyRec(Node<E> node) {
        Node<E> newNode = null;

        if (node != null && (node.getLeft() != null || node.getRight() != null)) {
            newNode = new Node(node.getElement(), copyRec(node.getLeft()), copyRec(node.getRight()));
        }

        return newNode;
    }

//#########################################################################
//#########################################################################
    private class BSTIterator implements Iterator<E> {

        private final Stack<Node<E>> stack;
        E curElement;       //current element
        boolean canRemove;  //to enable remove()

        public BSTIterator() {
            stack = new Stack<>();
            Node<E> cur = (Node<E>) root();
            while (cur != null) {
                stack.push(cur);
                cur = cur.getLeft();
            }
            curElement = null;
            canRemove = false;
        }

        /**
         * @return whether we have a next smallest element
         */
        @Override
        public boolean hasNext() {
            return !stack.isEmpty();
        }

        /**
         * @return the next smallest element
         */
        @Override
        public E next() {
            Node<E> curr = stack.pop();
            curElement = curr.getElement();

            curr = curr.getRight();

            while (curr != null) {
                stack.push(curr);
                curr = curr.getLeft();
            }

            canRemove = true;
            return curElement;
        }

        /**
         * remove the current element
         */
        @Override
        public void remove() {
            if (!canRemove) {
                throw new IllegalStateException("You can't remove the element.");
            }

            TREE.super.remove(curElement);

            curElement = null;
            canRemove = false;
            stack.clear();
        }
    }
//#########################################################################
//#########################################################################

    /**
     * build a list with all elements of the tree. The elements in the left
     * subtree in ascending order and the elements in the right subtree in
     * descending order.
     *
     * @return returns a list with the elements of the left subtree in ascending
     * order and the elements in the right subtree is descending order.
     */
    public Iterable<E> ascdes() {
        List<E> list = new LinkedList<>();

        ascSubtree(root.getLeft(), list);
        list.add(root.getElement());
        desSubtree(root.getRight(), list);

        return list;
    }

    private void ascSubtree(Node<E> node, List<E> snapshot) {
        if (node != null) {
            ascSubtree(node.getLeft(), snapshot);
            snapshot.add(node.getElement());
            ascSubtree(node.getRight(), snapshot);
        }
    }

    private void desSubtree(Node<E> node, List<E> snapshot) {
        if (node != null) {
            desSubtree(node.getRight(), snapshot);
            snapshot.add(node.getElement());
            desSubtree(node.getLeft(), snapshot);
        }
    }

    public List<E> topoSuperior() {
        Map<Integer, List<E>> nodeByLevel = new LinkedHashMap<>();
        List<E> result = new ArrayList<>();
        Set<E> sortElement = new TreeSet<>();
        List<E> lvlNodes = new ArrayList<>();

        int topHalf = this.height() / 2;

        nodeByLevel = nodesByLevel();

        for (int i = 0; i <= topHalf; i++) {
            lvlNodes = nodeByLevel.get(i);
            sortElement.addAll(lvlNodes);
        }

        result.addAll(sortElement);
        Collections.reverse(result);
        return result;

    }

    public List<E> getBaseNodes() {
        List<E> output = new ArrayList<>();
        int alturaTree = this.height();
        int alturaActual = 0;
        getNodes(this.root, alturaActual, alturaTree, output);
        output.sort((a, b) -> b.compareTo(a));
        return output;
    }

    public void getNodes(Node<E> n, int alturaA, int alturaT, List<E> list) {
//        if (alturaA <= alturaT / 2) { //vai buscar a metade superior
        if (alturaA >= alturaT / 2) { // vai buscar a metade inferior
//            if (n.getLeft() != null) {
            list.add(n.getElement());
        }

//            if (n.getRight() != null) {
//        list.add(n.getRight().getElement());
//            }
//        } else {
        if (n.getLeft() != null) {
            getNodes(n.getLeft(), alturaA + 1, alturaT, list);
        }
        if (n.getRight() != null) {
            getNodes(n.getRight(), alturaA + 1, alturaT, list);
        }
//        }
    }

    public boolean istheTreeAvl() {
        if (root == null) {
            return true;
        }
        return isBalanced(root);
    }

    public boolean isBalanced(Node node) {
        int alturaEsquerda;
        int alturaDireita;
        if (node == null) {
            return true;
        }
        alturaEsquerda = height(node.getLeft());
        alturaDireita = height(node.getRight());
        if (Math.abs(alturaDireita - alturaEsquerda) <= 1
                && isBalanced(node.getLeft()) && isBalanced(node.getRight())) {
            return true;
        }
        return false;

    }

    /**
     * resposta ao exercicio 3 da Prova da época normal 30 de Janeiro 2017
     *
     * @param string
     * @return
     */
    public Node getNodeByOriented(String string) {
        Node node = root;
        int i = 0;
        while (i < string.length()) {
            if (node != null) {
                char x = string.charAt(i);
                if (x == '0') {
                    node = node.getLeft();
                } else if (x == '1') {
                    node = node.getRight();
                }
            }
            if (node == null) {
                return null;
            }
            i++;
        }
        return node;
    }
    
    public static void verificaAVL(TREE arv, Node noAtual, ArrayList diferenca) {
        
        int esquerda = arv.height(noAtual.getLeft());
        int direita = arv.height(noAtual.getRight());
        
        int verifica = Math.abs(esquerda - direita);
        
        diferenca.add(verifica);
        
        //System.out.println("Verifica " + verifica);
        
        if (noAtual.getLeft() != null) {
            verificaAVL(arv, noAtual.getLeft(), diferenca);
            
        }
        
        if (noAtual.getRight() != null) {
            verificaAVL(arv, noAtual.getRight(), diferenca);
        }

    }
    
    public static void main(String[] args) {
        
        TREE<Integer> arvore = new TREE();
        ArrayList <Integer> resultado = new ArrayList<>();
        
        arvore.insert(65);
        arvore.insert(54);
        arvore.insert(85);
        arvore.insert(19);
        arvore.insert(60);
        arvore.insert(80);
        arvore.insert(89);
        arvore.insert(8);
        arvore.insert(35);
        arvore.insert(70);
        arvore.insert(83);
        arvore.insert(9);
        
        Node noPai = arvore.root;
        
        int avl = 0;
        
        verificaAVL (arvore, noPai, resultado);
        
        System.out.println("Difererenca entre os ramos " + resultado);
        
        for (int i = 0; i < resultado.size(); i++) {
            
            if (resultado.get(i) >= 2) {
                avl = 1;
            }
        }
        
        if (avl == 0) {
            System.out.println("A arvore é AVL");
        }
        else
            System.out.println("A arvore NAO é AVL");
        
        
    }
}
