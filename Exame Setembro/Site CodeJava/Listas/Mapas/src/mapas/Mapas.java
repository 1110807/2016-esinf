package mapas;

import java.util.*;

public class Mapas {

    public static void main(String[] args) {
        Map<Integer, String> mapa = new HashMap<>();
        
        mapa.put(200, "Ok");
        mapa.put(303, "See Other");
        mapa.put(404, "Not Found");
        mapa.put(500, "Internal Server error");
        
        System.out.println(mapa);
        
        mapa.put(200, "No Ok");
        
        Map<Integer, String> mapaCopia = new HashMap(mapa);
        
        System.out.println(mapaCopia);
        
        Map<String, String> mapaLigado = new LinkedHashMap<>();
        
        mapaLigado.put("2345678900", "Tom");
        mapaLigado.put("123456789", "Peter");
        mapaLigado.put("456789", "Mary");
        mapaLigado.put("98765432","John");
        
        System.out.println(mapaLigado);
        
        Map<String, String> mapaArvore = new TreeMap<>();
        
        mapaArvore.put(".c", "C");
        mapaArvore.put(".java", "Java");
        mapaArvore.put(".pl", "Pearl");
        mapaArvore.put(".cs", "C#");
        mapaArvore.put("php", "PHP");
        mapaArvore.put("cpp", "C++");
        
        System.out.println(mapaArvore);
        
        mapa.put(400, "Bad Request");
        mapa.put(304, "Not Modified");
        mapa.put(200, "Ok");
        mapa.put(301, "Moved Permanently");
        mapa.put(500, "Internal Server Error");
        
        String status301 = mapa.get(301);
        
        
    }
    
}
