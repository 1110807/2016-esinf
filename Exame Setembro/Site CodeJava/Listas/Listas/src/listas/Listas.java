package listas;

import java.util.*;

public class Listas {

    public static void main(String[] args) {
        
        List<String> listaArrays = new ArrayList<>();
        
        listaArrays.add("One");
        listaArrays.add("Two");
        listaArrays.add("Three");
        listaArrays.add("Four");
        
        System.out.println(listaArrays);
        
        List<String> listaLinked = new LinkedList<>();
        
        listaLinked.add("Five");
        listaLinked.add("Six");
        listaLinked.add("Seven");
        listaLinked.add("Eight");
        
        System.out.println(listaLinked);
        
        listaArrays.add(0, "Zero");
        
        listaLinked.addAll(0, listaArrays);
        
        System.out.println(listaLinked);
        
        listaLinked.set(2, "2");
        
        System.out.println(listaLinked);
        
        listaLinked.remove(2);
        
        if (listaLinked.remove("Two")) {
            System.out.println("Removido");
        } else {
            System.out.println("Elemento não encontrado");
        }
        
        //System.out.println(listaLinked);
        
        Iterator<String> iterador = listaLinked.iterator();
        
        while (iterador.hasNext()) {
            System.out.println(iterador.next());
        }
        
        //listaLinked.forEach(s -> System.out.println(s));
        
        if (listaLinked.contains("Three")) {
            System.out.println("Encontrado");
        } else {
            System.out.println("Elemento não encontrado");
        }
        
        List<String> listaNomes = Arrays.asList("Tom", "john", "Mary", "Peter", "Davide", "Alice");
        System.out.println("Lista original" + listaNomes);
        
        List<String> subLista = listaNomes.subList(0, listaNomes.size());
        
        System.out.println(subLista);
        
        
        
        
        }
    
}
