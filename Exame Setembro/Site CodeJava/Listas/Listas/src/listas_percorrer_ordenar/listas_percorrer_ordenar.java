package listas_percorrer_ordenar;

import java.util.*;

public class listas_percorrer_ordenar {
    
    public static void main(String[] args) {
        
        List<String> listaLinked = new LinkedList<>();
        
        listaLinked.add("D");
        listaLinked.add("C");
        listaLinked.add("E");
        listaLinked.add("A");
        listaLinked.add("B");
        
        listaLinked.forEach(s -> System.out.println(s));
        
        Collections.sort(listaLinked);
        /*
        Iterator<String> iterador = listaLinked.iterator();
        
        while (iterador.hasNext()) {
            System.out.println(iterador.next());
        }
        */
        List<String> listaOrigem = new ArrayList<>();
        listaOrigem.add("A");
        listaOrigem.add("B");
        listaOrigem.add("C");
        listaOrigem.add("D");
        
        List<String> listaDestino = new ArrayList<>();
        listaDestino.add("V");
        listaDestino.add("W");
        listaDestino.add("X");
        listaDestino.add("Y");
        listaDestino.add("Z");
        
        System.out.println(listaDestino);
        
        Collections.copy(listaDestino, listaOrigem);
        
        System.out.println("Lista de destino depois de copiado" + listaDestino);
        
       
    }
}
