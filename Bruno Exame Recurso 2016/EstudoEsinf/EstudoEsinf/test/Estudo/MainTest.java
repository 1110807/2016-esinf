/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estudo;

import Priority_queue.HeapPriorityQueue;
import esinf.DoublyLinkedList;
import graphbase.Graph;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author brunosantos
 */
public class MainTest {

    private DoublyLinkedList<String> str = new DoublyLinkedList<>();
    private Set<String> dictionary = new HashSet<>();
    private DoublyLinkedList<Integer> serie = new DoublyLinkedList<>();
    private Integer period;
    private Graph<Integer, Character> trie = new Graph(true);
    private ArrayList<Integer> v = new ArrayList<>();
    private Graph<String, Integer> g = new Graph(true);

    public MainTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        String frase[] = {"os", "campos", "são", "berdes", "os", "zolhos", "dela", "são", "berdes", "e", "os", "meus", "zolhos", "tamen"};
        String dicionario[] = {"os", "campos", "são", "verdes", "os", "olhos", "dela", "são", "verdes", "e", "os", "meus", "olhos", "também"};
        for (int i = 0; i < frase.length; i++) {
            str.addLast(frase[i]);
            dictionary.add(dicionario[i]);
        }
        Integer temp[] = {2, 4, 3, 7, 8, 10};
        for (int i = 0; i < temp.length; i++) {
            serie.addLast(temp[i]);
        }
        this.period = 3;

        Integer zero = 0;
        Integer cento1 = 101;
        Integer quinze = 15;
        Integer onze = 11;
        Integer sete = 7;
        Integer cento2 = 102;
        Integer cinco = 5;
        Integer tres = 3;
        Integer quatro = 4;
        Integer doze = 12;
        Integer nove = 9;
        trie.insertVertex(zero);
        trie.insertVertex(cento1);
        trie.insertVertex(quinze);
        trie.insertVertex(onze);
        trie.insertVertex(sete);
        trie.insertVertex(cento2);
        trie.insertVertex(cinco);
        trie.insertVertex(tres);
        trie.insertVertex(quatro);
        trie.insertVertex(doze);
        trie.insertVertex(nove);
        trie.insertEdge(zero, cento1, 't', 1);
        trie.insertEdge(cento1, sete, 'o', 1);
        trie.insertEdge(zero, quinze, 'A', 1);
        trie.insertEdge(zero, onze, 'i', 1);
        trie.insertEdge(cento1, cento2, 'e', 1);
        trie.insertEdge(cento2, tres, 'a', 0);
        trie.insertEdge(cento2, quatro, 'd', 0);
        trie.insertEdge(cento2, doze, 'n', 0);
        trie.insertEdge(onze, cinco, 'n', 0);
        trie.insertEdge(cinco, nove, 'n', 0);

        v.add(1);
        v.add(2);
        v.add(3);
        v.add(4);
        v.add(5);
        v.add(6);
        v.add(7);
        v.add(8);
        v.add(9);
        v.add(10);
        String joao = "João";
        String maria = "Maria";
        String rita = "Rita";
        String joana = "Joana";
        String raul = "Raul";
        String antonio = "António";
        String miguel = "Miguel";
        String manuel = "Manuel";
        String tiago = "Tiago";
        g.insertVertex(maria);
        g.insertVertex(joao);
        g.insertVertex(rita);
        g.insertVertex(joana);
        g.insertVertex(raul);
        g.insertVertex(antonio);
        g.insertVertex(miguel);
        g.insertVertex(manuel);
        g.insertVertex(tiago);
        g.insertEdge(maria, rita, 0, 0);
        g.insertEdge(joao, rita, 0, 0);
        g.insertEdge(maria, manuel, 0, 0);
        g.insertEdge(manuel, miguel, 0, 0);
        g.insertEdge(rita, joana, 0, 0);
        g.insertEdge(rita, raul, 0, 0);
        g.insertEdge(rita, antonio, 0, 0);
        g.insertEdge(antonio, tiago, 0, 0);
        g.insertEdge(joana, miguel, 0, 0);
        g.insertEdge(raul, miguel, 0, 0);

    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of checkErrors method, of class Main.
     */
    @Test
    public void testCheckErrors() {
        System.out.println("checkErrors");
        Main instance = new Main();
        String temp[] = {"os", "campos", "são", "ERRO_1", "os", "ERRO_2", "dela", "são", "ERRO_1", "e", "os", "meus", "ERRO_2", "ERRO_3"};
        DoublyLinkedList<String> expResult = new DoublyLinkedList<>();
        for (int i = 0; i < temp.length; i++) {
            expResult.addLast(temp[i]);
        }
        DoublyLinkedList<String> result = instance.checkErrors(str, dictionary);
        for (String string : result) {
            System.out.print(string + " ");
        }
        assertEquals(expResult, result);
    }

    /**
     * Test of calcMMS method, of class Main.
     */
    @Test
    public void testCalcMMS() {
        System.out.println("calcMMS");
        Main instance = new Main();
        DoublyLinkedList<Integer> expResult = new DoublyLinkedList<>();
        Integer temp[] = {0, 0, 3, 4, 6, 8};
        for (int i = 0; i < temp.length; i++) {
            expResult.addLast(temp[i]);
        }
        DoublyLinkedList<Integer> result = instance.calcMMS(serie, period);
        for (Integer integer : result) {
            System.out.println(integer.toString());
        }
        assertEquals(expResult, result);

    }

    /**
     * Test of checkSequence method, of class Main.
     */
    @Test
    public void testCheckSequence() {
        System.out.println("checkSequence");
        Character[] sequence = {'t', 'e', 'd'};
        Main instance = new Main();
        Integer expResult = new Integer(4);
        Integer result = instance.checkSequence(trie, sequence);
        System.out.println("result: " + result);
        assertEquals(expResult, result);

    }

    /**
     * Test of Kneighbors method, of class Main.
     */
    @Test
    public void testKneighbors() {
        System.out.println("Kneighbors");
        DoublyLinkedList<Integer> list = new DoublyLinkedList<>();
        list.addLast(2);
        list.addLast(9);
        list.addLast(7);
        list.addLast(5);
        list.addLast(10);
        list.addLast(15);
        list.addLast(6);
        list.addLast(12);
        list.addLast(3);
        Integer elem = 10;
        Integer k = 5;
        Main instance = new Main();
        DoublyLinkedList<Integer> expResult = new DoublyLinkedList<>();
        expResult.addLast(6);
        expResult.addLast(3);
        expResult.addLast(7);
        DoublyLinkedList<Integer> result = instance.Kneighbors(list, elem, k);
        result.forEach(x -> System.out.println(x.toString()));
        assertEquals(expResult, result);

    }

    /**
     * Test of menoresElementos method, of class Main.
     */
    @Test
    public void testMenoresElementos() {
        System.out.println("menoresElementos");
        Main instance = new Main();
        ArrayList expResult = new ArrayList();
        expResult.add(1);
        expResult.add(2);
        expResult.add(3);
        expResult.add(4);
        ArrayList result = instance.menoresElementos(v, 4);

        assertEquals(expResult, result);

    }

    /**
     * Test of KsubList method, of class Main.
     */
    @Test
    public void testKsubList() {
        System.out.println("KsubList");
        LinkedList<Integer> list = new LinkedList<>();
        Integer b[] = {2, 9, 7, 5, 10, 15, 6, 12, 3};
        list.addAll(Arrays.asList(b));
        Integer a[] = {3, 6, 10};
        ArrayList<Integer> centers = new ArrayList<>();
        centers.addAll(Arrays.asList(a));
        Map<Integer, LinkedList<Integer>> expResult = new HashMap<>();
        Integer c[] = {3, 2};
        LinkedList<Integer> cc = new LinkedList<>();
        cc.addAll(Arrays.asList(c));
        expResult.put(3, cc);
        Integer d[] = {6, 7, 5};
        LinkedList<Integer> dd = new LinkedList<>();
        dd.addAll(Arrays.asList(d));
        expResult.put(6, dd);
        Integer e[] = {10, 9, 15, 12};
        LinkedList<Integer> ff = new LinkedList<>();
        ff.addAll(Arrays.asList(e));
        expResult.put(10, ff);
        Map<Integer, LinkedList<Integer>> result = Main.KsubList(list, centers);
        assertEquals(expResult, result);
    }

    /**
     * Test of calculaPromocoes method, of class Main.
     */
    @Test
    public void testCalculaPromocoes() {
        System.out.println("calculaPromocoes");

        Integer n = 9;
        Main instance = new Main();
        List<String> expResult = null;
        List<String> result = instance.calculaPromocoes(g, n);
        assertEquals(expResult, result);

    }

}
