/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Priority_queue;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author brunosantos
 */
public class HeapPriorityQueueTest {

    HeapPriorityQueue<Integer, String> instance;

    Integer[] keys = {20, 15, 10, 13, 8, 12, 40, 30, 5, 21};
    String[] values = {"vinte", "quinze", "dez", "treze", "oito", "doze", "quarenta", "trinta", "cinco", "vinteeum"};

    public HeapPriorityQueueTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        instance = new HeapPriorityQueue(keys, values);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of mistério method, of class HeapPriorityQueue.
     */
    @Test
    public void testMisterio() {
        System.out.println("misterio");
        HeapPriorityQueue instance = new HeapPriorityQueue();

        instance.insert(2, 20);
        System.out.println(instance);
        instance.insert(3, 30);
        System.out.println(instance);
        instance.insert(4, 40);
        System.out.println(instance);
        instance.insert(5, 50);
        System.out.println(instance);
        instance.insert(6, 60);
        System.out.println(instance);
        instance.insert(7, 70);
        System.out.println(instance);
        instance.insert(8, 80);
        System.out.println(instance);
        instance.insert(9, 90);
        System.out.println(instance);
        instance.insert(10, 100);
        System.out.println(instance);
        instance.insert(1, 10);
        System.out.println(instance);
        HeapPriorityQueue expResult = null;
        HeapPriorityQueue result = instance.misterio();
        assertEquals(expResult, result);

    }

    /**
     * Test of parent method, of class HeapPriorityQueue.
     */
    @Test
    public void testParent() {
        System.out.println("parent");
        int j = 0;
        HeapPriorityQueue instance = new HeapPriorityQueue();
        int expResult = 0;
        int result = instance.parent(j);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of left method, of class HeapPriorityQueue.
     */
    @Test
    public void testLeft() {
        System.out.println("left");
        int j = 0;
        HeapPriorityQueue instance = new HeapPriorityQueue();
        int expResult = 0;
        int result = instance.left(j);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of right method, of class HeapPriorityQueue.
     */
    @Test
    public void testRight() {
        System.out.println("right");
        int j = 0;
        HeapPriorityQueue instance = new HeapPriorityQueue();
        int expResult = 0;
        int result = instance.right(j);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of hasLeft method, of class HeapPriorityQueue.
     */
    @Test
    public void testHasLeft() {
        System.out.println("hasLeft");
        int j = 0;
        HeapPriorityQueue instance = new HeapPriorityQueue();
        boolean expResult = false;
        boolean result = instance.hasLeft(j);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of hasRight method, of class HeapPriorityQueue.
     */
    @Test
    public void testHasRight() {
        System.out.println("hasRight");
        int j = 0;
        HeapPriorityQueue instance = new HeapPriorityQueue();
        boolean expResult = false;
        boolean result = instance.hasRight(j);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of swap method, of class HeapPriorityQueue.
     */
    @Test
    public void testSwap() {
        System.out.println("swap");
        int i = 0;
        int j = 0;
        HeapPriorityQueue instance = new HeapPriorityQueue();
        instance.swap(i, j);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of percolateUp method, of class HeapPriorityQueue.
     */
    @Test
    public void testPercolateUp() {
        System.out.println("percolateUp");
        int j = 0;
        HeapPriorityQueue instance = new HeapPriorityQueue();
        instance.percolateUp(j);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of percolateDown method, of class HeapPriorityQueue.
     */
    @Test
    public void testPercolateDown() {
        System.out.println("percolateDown");
        int j = 0;
        HeapPriorityQueue instance = new HeapPriorityQueue();
        instance.percolateDown(j);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildHeap method, of class HeapPriorityQueue.
     */
    @Test
    public void testBuildHeap() {
        System.out.println("buildHeap");
        HeapPriorityQueue instance = new HeapPriorityQueue();
        instance.buildHeap();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of size method, of class HeapPriorityQueue.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        HeapPriorityQueue instance = new HeapPriorityQueue();
        int expResult = 0;
        int result = instance.size();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of min method, of class HeapPriorityQueue.
     */
    @Test
    public void testMin() {
        System.out.println("min");
        HeapPriorityQueue instance = new HeapPriorityQueue();
        Entry expResult = null;
        Entry result = instance.min();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of insert method, of class HeapPriorityQueue.
     */
    @Test
    public void testInsert() {
        System.out.println("insert");
        Object key = null;
        Object value = null;
        HeapPriorityQueue instance = new HeapPriorityQueue();
        Entry expResult = null;
        Entry result = instance.insert(key, value);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of removeMin method, of class HeapPriorityQueue.
     */
    @Test
    public void testRemoveMin() {
        System.out.println("removeMin");
        HeapPriorityQueue instance = new HeapPriorityQueue();
        Entry expResult = null;
        Entry result = instance.removeMin();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class HeapPriorityQueue.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        HeapPriorityQueue instance = new HeapPriorityQueue();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of clone method, of class HeapPriorityQueue.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        HeapPriorityQueue instance = new HeapPriorityQueue();
        HeapPriorityQueue expResult = null;
        HeapPriorityQueue result = instance.clone();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSubHeap method, of class HeapPriorityQueue.
     */
    @Test
    public void testGetSubHeap() {
        System.out.println("getSubHeap");
        int index = 1;
        Object[] vet = null;
        HeapPriorityQueue instance = new HeapPriorityQueue();
        instance.insert(1, 10);
        instance.insert(2, 9);
        instance.insert(3, 5);
        instance.insert(4, 8);
        instance.insert(6, 4);
        instance.insert(7, 3);
        instance.insert(8, 1);
        instance.insert(9, 2);
        instance.insert(5, 7);
        instance.insert(10, 6);
        System.out.println(instance);
        int expResult = 6;
        int result = instance.getSubHeap(index, vet);
        assertEquals(expResult, result);

    }

    /**
     * Test of getHeapPor method, of class HeapPriorityQueue.
     */
    @Test
    public void testGetHeapPor() {
        System.out.println("getHeapPor");
        List list = null;
        int indexInf = 0;
        int indexSup = 0;
        HeapPriorityQueue instance = new HeapPriorityQueue();
        instance.getHeapPor(list, indexInf, indexSup);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMax method, of class HeapPriorityQueue.
     */
    @Test
    public void testGetMax() {
        System.out.println("getMax");
        HeapPriorityQueue instance = new HeapPriorityQueue();
        instance.insert(10, 6);
        instance.insert(7, 3);
        instance.insert(2, 9);
        instance.insert(4, 8);
        instance.insert(1, 10);
        instance.insert(5, 7);
        instance.insert(6, 4);
        instance.insert(8, 1);
        instance.insert(3, 5);
        instance.insert(9, 2);
        System.out.println(instance);
        List expResult = null;
        List result = instance.getMax();
        assertEquals(expResult, result);

    }

    /**
     * Test of getMaxPor method, of class HeapPriorityQueue.
     */
    @Test
    public void testGetMaxPor() {
        System.out.println("getMaxPor");
        List list = null;
        int indexInf = 0;
        int indexSup = 0;
        HeapPriorityQueue instance = new HeapPriorityQueue();
        instance.getMaxPor(list, indexInf, indexSup);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
