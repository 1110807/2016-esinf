/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL;

import PL.BST.Node;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author brunosantos
 */
public class TREETest {

    private TREE instance = new TREE();

    public TREETest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    @SuppressWarnings("unchecked")
    public void setUp() {

        instance.insert(65);
        instance.insert(54);
        instance.insert(85);
        instance.insert(19);
        instance.insert(60);
        instance.insert(80);
        instance.insert(89);
        instance.insert(8);
        instance.insert(35);
        instance.insert(70);
        instance.insert(83);
        instance.insert(67);
        instance.insert(75);
        instance.insert(88);
        instance.insert(90);
//        instance.insert(61);
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of topoSuperior method, of class TREE.
     */
    @Test
    public void testTopoSuperior() {
        System.out.println("topoSuperior");

        List expResult = new ArrayList<>();
        Integer temp[] = {89, 85, 80, 65, 60, 54, 19};
        for (Integer temp1 : temp) {
            expResult.add(temp1);
        }
        List result = instance.topoSuperior();

        result.stream().forEach((r) -> System.out.println(r));
        System.out.println(instance);
        System.out.println(result);
        assertEquals(expResult, result);

    }

    /**
     * Test of getBaseNodes method, of class TREE.
     */
    @Test
    public void testGetBaseNodes() {
        System.out.println("getBaseNodes");

        List expResult = new ArrayList<>();
        expResult.add(67);
        expResult.add(75);
        List result = instance.getBaseNodes();

//        Set temp = new TreeSet();
//        temp.addAll(result);
//        result.clear();
//        result.addAll(temp);
//        Collections.reverse(result);
        System.out.println(instance);
        System.out.println(Arrays.toString(result.toArray()));
        assertEquals(expResult, result);

    }

    /**
     * Test of istheTreeAvl method, of class TREE.
     */
    @Test
    public void testIstheTreeAvl() {
        System.out.println("istheTreeAvl");

        boolean expResult = false;
        boolean result = instance.istheTreeAvl();
        System.out.println(instance);
        assertEquals(expResult, result);

    }

    /**
     * Test of isBalanced method, of class TREE.
     */
    @Test
    public void testIsBalanced() {
        System.out.println("isBalanced");
        BST.Node node = instance.root();
        boolean expResult = true;
        boolean result = instance.isBalanced(node);
        assertEquals(expResult, result);

    }

    /**
     * Test of getNodeByOriented method, of class TREE.
     */
    @Test
    public void testGetNodeByOriented() {
        System.out.println("getNodeByOriented- 101");
        String string = "101";
        TREE instance = new TREE();
        instance.insert(65);
        instance.insert(54);
        instance.insert(85);
        instance.insert(19);
        instance.insert(60);
        instance.insert(80);
        instance.insert(89);
        instance.insert(8);
        instance.insert(35);
        instance.insert(70);
        instance.insert(83);
        System.out.println(instance);

        Node expResult = instance.find(83, instance.root);
        BST.Node result = instance.getNodeByOriented(string);
        assertEquals(expResult, result);

    }

    /**
     * Test of getNodeByOriented method, of class TREE.
     */
    @Test
    public void testGetNodeByOriented1() {
        System.out.println("getNodeByOriented - 110");
        String string = "110";
        TREE instance = new TREE();
        instance.insert(65);
        instance.insert(54);
        instance.insert(85);
        instance.insert(19);
        instance.insert(60);
        instance.insert(80);
        instance.insert(89);
        instance.insert(8);
        instance.insert(35);
        instance.insert(70);
        instance.insert(83);
        System.out.println(instance);

        Node expResult = null;
        BST.Node result = instance.getNodeByOriented(string);
        assertEquals(expResult, result);

    }

}
