/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estudo;

import Priority_queue.HeapPriorityQueue;
import esinf.DoublyLinkedList;
import graphbase.Edge;
import graphbase.Graph;
import graphbase.GraphAlgorithms;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author brunosantos
 */
public class Main {

    public static void main(String[] args) {
        String s = "";
        s = dec_2_bin(14, s);
        System.out.println("");
        System.out.println(s);
    }

    /**
     * pergunta 3 da época de recurso de Fevereiro de 2016 Vai buscar a folha ao
     * nivel mais baixo da árvore à esquerda
     *
     */
    /**
     * resposta ao exercicio 1 da prova da época Normal 30 de janeiro de 2017
     *
     * @param list
     * @param centers
     * @return
     */
    public static Map<Integer, LinkedList<Integer>> KsubList(LinkedList<Integer> list, ArrayList<Integer> centers) {
        Map<Integer, LinkedList<Integer>> output = new HashMap<>();
        for (Integer center : centers) {
            LinkedList<Integer> temp = new LinkedList<>();
            temp.add(center);
            output.put(center, temp);
        }
//        list.removeAll(centers);

        for (Integer l : list) {
            if (!centers.contains(l)) {
                Integer min = Integer.MAX_VALUE;
                Integer c = 0;
                for (Integer center : centers) {
                    int dif = Math.abs(center - l);
                    if (dif < min) {
                        min = dif;
                        c = center;
                    }
                }
                output.get(c).add(l);
            }
        }
        return output;
    }

    /**
     * exercicio 4 da prova da época Normal 30 de Janeiro de 2017
     *
     * @param g
     * @param n
     * @return
     */
    
    
    public List<String> calculaPromocoes(Graph<String, Integer> g, Integer n) {
        Graph<String, Integer> gClone = g.clone();
        ArrayList<String> promos = new ArrayList<>();
        Iterator<String> allVerts = gClone.vertices().iterator();
        while (allVerts.hasNext()) {
            if (n == promos.size()) {
                return promos;
            }
            String v = allVerts.next();
            int inDegree = gClone.inDegree(v);
            if (inDegree == 0) {
                promos.add(v);
                gClone.removeVertex(v);
                allVerts = gClone.vertices().iterator();
            }
        }
        return promos;

    }

    /**
     * Exercicio 1 época de recurso de fevereiro de 2016
     *
     * @param str
     * @param dictionary
     * @return
     */
    public DoublyLinkedList<String> checkErrors(DoublyLinkedList<String> str, Set<String> dictionary) {
        DoublyLinkedList<String> output = new DoublyLinkedList<>();
        Map<String, String> errors = new HashMap<>();

        for (String palavra : str) {
            if (dictionary.contains(palavra)) {
                output.addLast(palavra);
            } else {
                if (!errors.containsKey(palavra)) {
                    int numero = errors.size() + 1;
                    String erro = "ERRO_" + numero;
                    errors.put(palavra, erro);
                }
                output.addLast(errors.get(palavra));
            }
        }
        return output;
    }

    /**
     * Exercício 1 Época Normal Janeiro 2016
     *
     * @param serie
     * @param period
     * @return
     */
    public DoublyLinkedList<Integer> calcMMS(DoublyLinkedList<Integer> serie, Integer period) {
        DoublyLinkedList<Integer> output = new DoublyLinkedList<>();
        List<Integer> listaTemporaria = new ArrayList<>();
        serie.forEach(s -> listaTemporaria.add(s));
        int temp = 0;
        for (int i = 0; i < serie.size(); i++) {
            temp += listaTemporaria.get(i);
            if (i < period - 1) {
                output.addLast(0);
            } else {
                output.addLast(temp / period);
                temp -= listaTemporaria.get(i - period + 1);
            }
        }
        return output;

    }

    /**
     * Pergunta 2 Exame de Recurso 12 Fevereiro 2016
     *
     * @param trie
     * @param sequence
     * @return
     */
    public Integer checkSequence(Graph<Integer, Character> trie, Character[] sequence) {
        if (trie == null || sequence == null) {
            return -1;
        }
        Iterable<Integer> vertices = trie.vertices();
        Integer noPai = null;
        for (Integer vertice : vertices) {
            if (vertice.compareTo(0) == 0) {
                noPai = vertice;
            }
        }
        for (Character c : sequence) {
            noPai = checkChar(trie, noPai, c);
            if (noPai == null) {
                return -1;
            }

        }
        return noPai < 100 ? noPai : -1;
    }

    /**
     * Pergunta 2 Exame de Recurso 12 Fevereiro 2016
     *
     * @param trie
     * @param noPai
     * @param c
     * @return
     */
    private Integer checkChar(Graph<Integer, Character> trie, Integer noPai, Character c) {
        Iterable<Edge<Integer, Character>> edges = trie.outgoingEdges(noPai);
        for (Edge<Integer, Character> edge : edges) {
            Character currentChar = edge.getElement();
            if (currentChar.equals(c)) {
                return edge.getVDest();
            }

        }
        return null;
    }

    private static String dec_2_bin(int valor, String bin) {
        int quociente, resto;
        if (valor != 0) {
            quociente = valor / 2;
            resto = valor % 2;
            return dec_2_bin(quociente, bin) + String.valueOf(resto);
        }
        return bin;
    }

    /**
     * exercício 2 Época Normal
     *
     * @param <V>
     * @param g
     * @param lstCenters
     * @return
     */
    public <V> Double centersGraph(Graph<V, Double> g, ArrayList<V> lstCenters) {
        Double caminho, caminhoMaior = 0d, caminhoMenor = 0d;
        lstCenters.clear();
        LinkedList<V> shortPath = new LinkedList<>();
        for (V verticeInicial : g.vertices()) {
            for (V verticeFinal : g.vertices()) {
                caminho = GraphAlgorithms.shortestPath(g, verticeInicial, verticeFinal, shortPath);
                if (caminhoMaior < caminho) {
                    caminhoMaior = caminho;
                }
            }
            if (caminhoMenor == 0) {
                lstCenters.add(verticeInicial);
                caminhoMenor = caminhoMaior;
            }
            if (caminhoMenor > caminhoMaior) {
                caminhoMenor = caminhoMaior;
                lstCenters.clear();
                lstCenters.add(verticeInicial);
            }
            if (caminhoMenor == caminhoMaior) {
                lstCenters.add(verticeInicial);
            }
            caminhoMaior = 0d;
        }
        return caminhoMenor;
    }

    /**
     * exercicio 1 Época Especial 2016
     *
     * @param list
     * @param elem
     * @param k
     * @return
     */
    DoublyLinkedList<Integer> Kneighbors(DoublyLinkedList<Integer> list, Integer elem, Integer k) {
        DoublyLinkedList<Integer> output = new DoublyLinkedList<>();
        List<Integer> numbers = new ArrayList<>();
        list.forEach(x -> numbers.add(x));
        numbers.sort((a, b) -> a - b);
        int index = numbers.indexOf(elem);
        int count = 0, i = 1, j = 1;

        while (count < k) {
            Integer left = numbers.get(index - i);
            Integer right = numbers.get(index + j);
            int leftAmplitude = elem - left;
            int rightAmplitude = right - elem;
            if (leftAmplitude <= rightAmplitude) {
                output.addLast(left);
                count++;
                i++;
            } else {
                output.addLast(right);
                count++;
                j++;
            }
        }
        return output;
    }

    public <K> ArrayList<K> menoresElementos(ArrayList<K> v, int n) {
        HeapPriorityQueue heapPriorityQueue = new HeapPriorityQueue();

        ArrayList<K> menorN = new ArrayList<>();

        for (int i = 0; i < v.size(); i++) {
            heapPriorityQueue.insert(v.get(i), i);
        }
        for (int i = 0; i < n; i++) {
            menorN.add((K) heapPriorityQueue.removeMin().getKey());
        }
        return menorN;
    }

}
