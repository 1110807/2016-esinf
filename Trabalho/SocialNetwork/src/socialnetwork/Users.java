/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialnetwork;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author fmassuncao
 */
public class Users implements Comparable {

    private String nick;
    private String email;
    private List<Cities> cidades;
    private List<Users> amigos;

    Users(String nick, String email, List<Cities> cidades, List<Users> amigos) {
        this.nick = nick;
        this.email = email;
        this.cidades = cidades;
        this.amigos = amigos;
    }

    public String getNick() {
        return nick;
    }

    public List<Cities> getCidades() {
        return cidades;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Users other = (Users) obj;
        if (!Objects.equals(this.nick, other.nick)) {
            return false;
        }
        return true;
    }

//    @Override
//    public int hashCode() {
//        throw new UnsupportedOperationException("Not supported yet.");
//        retur nick;
//    }
//    @Override
//    public int compareTo(Users o) {
//        int nickDiff;
//        nickDiff = nick.compareToIgnoreCase(Users.nick);
//        if(nickDiff != 0){
//            return nickDiff;
//        }
//        return 1;
//    }
//  
//}
//    public int compareTo(Users o) {
//       // Users x = (Users) o;
//        return this.nick.compareTo(o.nick);
//    }
    @Override
    public int compareTo(Object o) {
        Users x = (Users) o;
        return this.nick.compareTo(x.nick);
    }

    public String toString() {
        List<String> nicks = new ArrayList<>();
        for (Users amigo : amigos) {
            nicks.add(amigo.getNick());
        }
        return nick + email + cidades + nicks;

    }
}
