/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialnetwork;

/**
 *
 * @author fmassuncao
 */
public class Cities implements Comparable{

    private String nome;
    private int pontos;
    private double latitude;
    private double longitude;

    Cities(String nome) {
        this.nome = nome;
    }

    Cities(String nome, int pontos, double latitude, double longitude) {
        this.nome = nome;
        this.pontos = pontos;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
//        Cities(int pontos, double latitude, double longitude) {
//        
//        this.pontos = pontos;
//        this.latitude = latitude;
//        this.longitude = longitude;
//    }
    
//        Cities(String nome, int pontos, double latitude, double longitude) {
//        this.nome = nome;
//        this.pontos = pontos;
//        this.latitude = latitude;
//        this.longitude = longitude;
//    }

//    public String toString() {
//        return nome;
//    }
    
    @Override
       public String toString() {
        return nome + pontos + latitude + longitude;

    }

    @Override
    public boolean equals(Object o) {
        Cities c = (Cities) o;
        if (this.nome.equals(c.nome)) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public int compareTo(Object o) {
        Cities x = (Cities) o;
        return this.nome.compareTo(x.nome);
    }

}
