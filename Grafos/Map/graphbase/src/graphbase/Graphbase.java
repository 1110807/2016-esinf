package graphbase;

import java.util.*;

public class Graphbase {

    public static Integer checkSequence(Graph<Integer,Character> g, Character[] s) {
        if (g == null || s == null) {
            return -1;
        }
        Integer noPai = null;
        
        Iterable <Integer> vertices = g.vertices();
        
        for ( Integer vertice : vertices) {
            if (vertice == 0) {
                noPai = vertice;
            }
        }
        for (Character c : s) {
            Iterable <Edge<Integer,Character>> ruas = g.outgoingEdges(noPai);
            for (Edge<Integer,Character> rua : ruas) {
                if (rua.getElement().equals(c)) {
                    noPai = rua.getVDest();
                }
            }
        }
        
        
        return noPai;
    }
    
    public static void main(String[] args) {
        
        Graph <Integer, Character> gMap = new Graph(true);
        
        
        Character [] sequence = { 't', 'e', 'd'};
        Integer resultado;
        
        int zero = 0;
        int centoeum = 101;
        int quinze = 15;
        int onze = 11;
        int sete = 7;
        int centoedois = 102;
        int cinco = 5;
        int tres = 3;
        int quatro = 4;
        int doze = 12;
        int nove = 9;
        
        gMap.insertVertex(zero);
        gMap.insertVertex(centoeum);
        gMap.insertVertex(quinze);
        gMap.insertVertex(onze);
        gMap.insertVertex(sete);
        gMap.insertVertex(centoedois);
        gMap.insertVertex(cinco);
        gMap.insertVertex(tres);
        gMap.insertVertex(quatro);
        gMap.insertVertex(doze);
        gMap.insertVertex(nove);
        
        gMap.insertEdge(zero, centoeum, 't', 1);
        gMap.insertEdge(zero, quinze, 'A', 1);
        gMap.insertEdge(zero, onze, 'i', 1);
        gMap.insertEdge(centoeum, sete, 'o', 1);
        gMap.insertEdge(centoeum, centoedois, 'e', 1);
        gMap.insertEdge(onze, cinco, 'n', 1);
        gMap.insertEdge(centoedois, tres, 'a', 1);
        gMap.insertEdge(centoedois, quatro, 'd', 1);
        gMap.insertEdge(centoedois, doze, 'n', 1);
        gMap.insertEdge(cinco, nove, 'n', 1);
        
        resultado = checkSequence(gMap, sequence);
        
        System.out.println(resultado);
    }
    
}
