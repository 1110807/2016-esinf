package graph;

import java.util.*;

public class Exemplo_1 {

    public static void main(String[] args) {
        
        AdjacencyMatrixGraph<String, Integer> g = new AdjacencyMatrixGraph();
        //AdjacencyMatrixGraph <String, Integer> g = new AdjacencyMatrixGraph();
        String maria = "maria";
        String joao = "joao";
        String davide = "davide";
        g.insertVertex(maria);
        g.insertVertex(joao);
        g.insertVertex(davide);
        g.insertEdge(maria, joao, 10);
        g.insertEdge(maria, davide, 20);
        /*
        System.out.println(g);
        
        System.out.println(g.incomingEdges(maria));
        System.out.println(g.inDegree(maria));
        System.out.println(g.outDegree(maria));
        
        System.out.println(g.incomingEdges(joao));
        System.out.println(g.inDegree(joao));
        */
        System.out.println(g.directConnections(maria));
        
        System.out.println(g.edges());
    }
    
}
