public static double power (double b, int e){

 if (e == 0)					// Se o expoente for 0 retorna 1
	return 1;
 if (e == 1)					// Se o expoente for 1 retorna a base
	return b;
 if (e % 2 == 0)				// Se o expoente for par
	return power (b * b, e/2);	// Chama o metodo com a divisão do expoente a meio
 else							// Se o expoente for impar
	return b*power(b*b, e/2) ;	// Chama o metodo com a divisão do expoente a meio
}

//	Complexidade O(log n), o vetor é dividido a meio
//	O algoritmo é NÃO deterministico, porque se o expoente for 1 ou 0 o aloritmo só corre uma vez, no pior caso o
//	algoritmo corre e/2 vezes