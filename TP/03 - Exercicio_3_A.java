public static int mystery3 (int [] A, int n){
 if (n==1)
	return A[0];
 else {
	int k=n/2;						//Divide o vetor a meio
	for (int i=0; i<k; i++){
		if (n%2==0)					//Se o numero de elementos for par
			A[i] = test(A[i], A[i+k]);
		else						//Se o numero de elementos for impar
			A[i] = test(A[i], A[i+k+1]);
	}
	if (n/2 != 0)					//Se o vetor for impar
	A[0] = test(A[0], A[(n/2)]);	//

	return mystery3(A, k);			//
 }
}

/*

a - O metodo retorna o maior elemento do vetor

b - O (n log n)
  A = {31, 1, 5, 6, 17, 11, 23, 38, 2}.
	9 elementos

31 , 17 <-- ultima linha
1, 11
5, 23
6, 38
17, 2

k = 5

*/